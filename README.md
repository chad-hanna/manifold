# Manifold 

## Computing a small bank and mass model suitable for online analysis

Start by copying the file `O4_projected_psds.xml.gz` from the `examples` directory.  Then make a file called `bank.yaml` with the following contents

```
m1: [8, 200]
m2: [8, 200]
M: [0, 1000]
mc: [0, 1000]
q: [0.99, 5]
chi: [-0.1, 0.1]
ns-mass: [0, 3.0]
ns-s1z: [-0.05, 0.05]
freq: [10, 512]
max-duration: 128
psd-xml: O4_projected_psds.xml.gz
instrument: L1
mm: 0.03
reuse-g-mm: 0.10
max-num-templates: 1
min-coord-vol: 0.0001
min-depth: 7
approximant: IMRPhenomD
trim: true
verbose: true
```

Then run

```bash
manifold_cbc_bank --output-h5 H1L1V1-GSTLAL_BBH_TEMPLATE_BANK_INIT-0-0.h5 --psd-xml O4_projected_psds.xml.gz --yaml bank.yaml 
```

You should see some verbose output that ends like this

```
...
1800 <Rectangle [(1.9515449934959719, 1.995230618766973), (1.995230618766973, 2.0389162440379742), (0.05, 0.1)]> 1.0
1900 <Rectangle [(2.25734437039298, 2.3010299956639813), (2.0826018693089754, 2.1262874945799766), (0.0, 0.05)]> 1.0
1977 templates before
1958 templates after
```

Next you should run the constrain program. This will add some metadata to describe the region of validity (e.g., to pad the boundaries). It will also assign template ids which are needed for the mass model

```bash
manifold_cbc_bank_constrain --output-h5 H1L1V1-GSTLAL_BBH_TEMPLATE_BANK-0-0.h5 --bank H1L1V1-GSTLAL_BBH_TEMPLATE_BANK_INIT-0-0.h5 --yaml constrain.yaml 
```
where ```constrain.yaml``` contains

```yaml
m1: [10., 190]
m2: [10.,190]
mc: [10, 165]
q: [1., 4]
M: [20., 380]
chi: [-0.05, 0.05]
ns-mass: [0, 3.0]
ns-s1z: [-0.04, 0.04]
assign-ids: true
```

Then you can generate a mass model like this:

```bash
manifold_cbc_bank_mass_model --bank H1L1V1-GSTLAL_BBH_TEMPLATE_BANK-0-0.h5 --output-h5 H1L1V1-GSTLAL_BBH_SALPETER_MASS_MODEL-0-0.h5 --yaml mass_model.yaml
```
where ```mass_model.yaml``` contains

```yaml
bank:
output-h5:
start-ix:
stop-ix:
mass_model:
    model: salpeter
    m_min: 0.8
```

Finally you can convert the bank to XML with this command

```bash
manifold_cbc_bank_to_xml H1L1V1-GSTLAL_BBH_TEMPLATE_BANK-0-0.h5 --output-xml H1L1V1-GSTLAL_BBH_TEMPLATE_BANK-0-0.xml.gz
```


