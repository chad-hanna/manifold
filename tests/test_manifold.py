"""Unit tests for manifold repo general testing. Most unit tests will be in module-specific
test files, like "test_metric.py". This file is only for repository-level tests, like importability.
"""

import inspect


class TestImports:
	"""Group for testing importability of modules in package"""

	def test_import_cbc(self):
		"""Test import of cbc metric"""
		from manifold.sources import cbc
		assert inspect.ismodule(cbc)

	def test_import_cover(self):
		"""Test import of cbc metric"""
		from manifold import cover
		assert inspect.ismodule(cover)

	def test_import_metric(self):
		"""Test import of cbc metric"""
		from manifold import metric
		assert inspect.ismodule(metric)

	def test_import_signal(self):
		"""Test import of cbc metric"""
		from manifold import signal
		assert inspect.ismodule(signal)

