"""Tests of numerical differentiation technique using numdifftools.Hessian

"""
import functools

import numdifftools
import numpy
import sympy
from scipy.stats import multivariate_normal


def mn_match(d, mean, cov_matrix):
	return multivariate_normal.pdf(d, mean=mean, cov=cov_matrix)


def sym_nd_gaussian(n: int, cov: numpy.ndarray):
	coords = sympy.symbols(['x_{{{:d}}}'.format(i) for i in range(1, n + 1)])
	icov = numpy.linalg.inv(cov)
	exponent = []
	for i in range(n):
		for j in range(n):
			exponent.append(coords[i] * icov[i][j] * coords[j])
	exponent = -sympy.Rational(1, 2) * sum(exponent)
	return (1 / (2 * numpy.pi)) ** (n / 2) * (1 / numpy.sqrt(numpy.linalg.det(cov))) * sympy.exp(exponent)


def expected_hessian(n, cov, p):
	coords = sympy.symbols(['x_{{{:d}}}'.format(i) for i in range(1, n + 1)])
	mn = sym_nd_gaussian(n, cov)
	h = numpy.zeros((n, n))
	for i in range(n):
		for j in range(n):
			h_ij = sympy.diff(sympy.diff(mn, coords[i]), coords[j]).subs(dict(zip(coords, p)))
			h[i][j] = sympy.simplify(sympy.expand(h_ij))
	return h


class TestNumDiff:
	"""Test num diff"""

	def test_mn_diff_3d_diag(self):
		"""Test MN 3D diff
		"""
		mean = numpy.array([0, 0, 0])
		cov_mat = numpy.array([
			[1, 0, 0],
			[0, 1, 0],
			[0, 0, 1],
		])
		f = functools.partial(mn_match, mean=mean, cov_matrix=cov_mat)
		h = numdifftools.Hessian(f)
		m = h(numpy.array([0, 0, 0]))
		expected = expected_hessian(3, cov_mat, [0,0,0])
		numpy.testing.assert_array_almost_equal(m, expected, decimal=4)

	def test_mn_diff_3d_non_diag(self):
		"""Test MN 3D diff

		"""
		mean = numpy.array([0, 0, 0])
		cov_mat = numpy.array([
			[3, 0, 1],
			[0, 2, 0],
			[1, 0, 1],
		])
		f = functools.partial(mn_match, mean=mean, cov_matrix=cov_mat)
		h = numdifftools.Hessian(f)
		m = h(numpy.array([0, 0, 0]))
		expected = expected_hessian(3, cov_mat, [0,0,0])
		numpy.testing.assert_array_almost_equal(m, expected, decimal=4)

	def test_mn_diff_3d_diag_oom(self):
		"""Test MN 3D diff
		"""
		mean = numpy.array([0, 0, 0])
		cov_mat = numpy.array([
			[1, 0, 0],
			[0, 1, 0],
			[0, 0, 1e9],
		])
		f = functools.partial(mn_match, mean=mean, cov_matrix=cov_mat)
		h = numdifftools.Hessian(f)
		m = h(numpy.array([0, 0, 0]))
		expected = expected_hessian(3, cov_mat, [0,0,0])
		numpy.testing.assert_array_almost_equal(m, expected, decimal=4)

	def test_mn_diff_3d_nondiag_oom(self):
		"""Test MN 3D diff
		"""
		mean = numpy.array([0, 0, 0])
		cov_mat = numpy.array([
			[  1, 1e9,   0],
			[1e9,   1,   0],
			[  0,   0,   1],
		])
		V, Q = numpy.linalg.eig(cov_mat)
		cov_mat = numpy.dot(Q, numpy.dot(numpy.diag(abs(V)), numpy.linalg.inv(Q)))
		f = functools.partial(mn_match, mean=mean, cov_matrix=cov_mat)
		h = numdifftools.Hessian(f)
		m = h(numpy.array([0, 0, 0]))
		expected = expected_hessian(3, cov_mat, [0,0,0])
		numpy.testing.assert_array_almost_equal(m, expected, decimal=4)
