"""Unit tests for the cbc_metric module


Remaining tests:


Ellipse axes:
- use M.T (or m inv if not ortho)
- use M.T on euclidean basis vectors to get principal vectors of ellipse
- columns of M.T will be principal vectors
- scale by mm and add/sub to center to get test point
- compute {x} = (metric - fft) / fft match as ellipse extreme points
- take max(abs(x)) < threshold as success

Update iteration:
- choose target_mm based on coords
- perform max diff check like in notebook
- if not below threshold, update target_mm by OOM (e.g. t=0.99995 -> t=0.9995)
- rerun


New Coordinates:

- mc m2 chi
- logmc logm2 chi
- logmtotal logmratio chi
- sathya coordinates


Test convexity of match function
m1 = 100, m2 = 100, chi = 0

Other options:
- model match function as nd gaussian and fit for covariance matrix
- deterministic fit of components, e.g. g_xx = (1-M)/dx^2, use solve deltas (fft match minimizer)
*- turn off time projections?

mm1 match vs fft match as function of manifold:
- visualize diffs in 2d
- compute statistics on diffs / attempt to model?
- differences - fft maximizes over time too, but tiny perturbations


Waveform differences:
- understand dependence on fmin
- fft and visualize
- waveforms constant above fmin = 12.0
- duration change to 16 for resolution when fft
- lal.COMPLEX16FreqTimeFFT(self.t_series, self.f_series, self.revplan) freq -> time
- also test lower m1 = m2, e.g m1 = m2 = 30
- test isco comp
	 def isco(total_mass):
         return 1/(3.14159 * 6**(1.5) * (5e-6 * total_mass))
- use fft match to compare the different waveforms at threshold +- epsilon

Sathya Coordinates:
- write m1 m2 to tau1 tau2 coordfunc
- check explicitly against owen
- owen uses f as minimum of noise curve f =argmin(psd)

"""
import pathlib
import tempfile
from unittest import mock

import numpy

import manifold.utilities.common
import manifold.utilities.data
from manifold.sources import cbc
from manifold.utilities import data

TEST_PSD = (manifold.utilities.data.DATA_ROOT / 'psd_for_treebank.xml.gz').as_posix()
# TEST_PSD = (util.DATA_ROOT / 'REF.xml.gz').as_posix()
TEST_REUSE_G_MM = 0.0
TEST_FREQ_LOW = 15
TEST_FREQ_HIGH = 512


def _sample_metric():
	"""Sample a metric for testing"""
	cf = cbc.Log10M1Log10M2Chi
	bounds = {
		'log10m1': (-2., 3.),
		'log10m2': (-2., 3.),
		'chi': (-0.98, 0.98)
	}
	g = cbc.CBCMetric(psd=data.read_psd(data.TREEBANK_PSD, 'H1', verbose=False),
					  coord_func=cf(**bounds),
					  flow=10.0, fhigh=512.0,
					  approximant="IMRPhenomD",
					  max_metric_diff=0.7)
	return g


def _sample_rectangle(mins, maxs):
	"""Create a simple rectangle for testing"""
	g = _sample_metric()
	return cbc.ManifoldRectangle(mins=mins,
								 maxes=maxs,
								 metric=g,
								 level=0,
								 left=False,
								 right=False,
								 g=None)


class TestCBCMetric:
	"""Group for tests using m1 m2 coords"""

	def test_create(self):
		"""Test creating a metric with simple inputs"""
		g = _sample_metric()
		assert isinstance(g, cbc.CBCMetric)

	def test_evaluate(self):
		"""Test evaluating a metric at a point"""
		g = _sample_metric()
		center = numpy.array([1.9, 1.9, 0.0])
		m = g.evaluate(center, method='deterministic')
		assert m.shape == (3, 3)


class TestCBCManifoldRectangle:
	"""Test group for CBCManifoldRectangle class tests"""

	def test_create(self):
		"""Test"""
		r = _sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])
		assert isinstance(r, cbc.ManifoldRectangle)


class TestBank:
	"""Test group for Bank class tests"""

	def test_create(self):
		"""Test creating a bank with simple inputs"""
		bank = cbc.Bank(rectangles=[_sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])])
		assert isinstance(bank, cbc.Bank)

	def test_assign_ids_reassign(self):
		"""Test assigning and reassigning ids"""
		bank = cbc.Bank(rectangles=[_sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])])
		orig_ids = bank.ids

		# By default check no changes
		bank.assign_ids()
		assert bank.ids == orig_ids

		# Check reassign works
		bank.assign_ids(reassign=True)
		assert bank.ids != orig_ids

	def test_assign_ids_reserved(self):
		"""Test assigning ids with reserved ids"""
		# Patch the maximum ID to ensure that the test is deterministic
		with mock.patch('manifold.sources.cbc.Bank.MAX_TEMPLATE_ID', 3):
			# The above limits the possible IDs to {0, 1, 2}

			sampled_ids = set()
			for i in range(10):
				bank = cbc.Bank(rectangles=[_sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1]),
											_sample_rectangle([1.1, 1.1, 1.1], [1.2, 1.2, 1.2]), ])
				sampled_ids = sampled_ids.union(set(bank.ids))
			assert sampled_ids == {0, 1, 2}

			# Now sample with a "reserved" ID to ensure it is not used
			sampled_ids = set()
			for i in range(10):
				bank = cbc.Bank(rectangles=[_sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1]),
											_sample_rectangle([1.1, 1.1, 1.1], [1.2, 1.2, 1.2]), ],
								reserved_ids=[2])
				sampled_ids = sampled_ids.union(set(bank.ids))
			assert sampled_ids == {0, 1}

	def test_merge(self):
		"""Test merging banks"""
		# Patch the maximum ID to ensure that the test is deterministic
		r1 = _sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])
		r2 = _sample_rectangle([1.1, 1.1, 1.1], [1.2, 1.2, 1.2])
		r3 = _sample_rectangle([1.2, 1.2, 1.2], [1.3, 1.3, 1.3])
		r4 = _sample_rectangle([0.9, 0.9, 0.9], [1.0, 1.0, 1.0])
		r5 = _sample_rectangle([1.4, 1.4, 1.4], [1.5, 1.5, 1.5])
		r6 = _sample_rectangle([1.5, 1.5, 1.5], [1.6, 1.6, 1.6])

		with mock.patch('manifold.sources.cbc.Bank.MAX_TEMPLATE_ID', 6):
			# The above limits the possible IDs to {0, 1, 2, 3, 4, 5}
			special_bank = cbc.Bank(rectangles=[r1, r2], ids=[0, 1], constraint_sets={'default': {'Coord1': [1, 5], 'Coord2': [2, 3]}})
			bank2 = cbc.Bank(rectangles=[r3, r4], ids=[2, 3], constraint_sets={'default': {'Coord1': [1, 6], 'Coord2': [2, 7]}})
			bank3 = cbc.Bank(rectangles=[r5, r6], ids=[4, 5], constraint_sets={'default': {'Coord1': [0, 5], 'Coord2': [1, 3]}})
			bank_all = special_bank.merge(bank2, bank3)

		# Check that all rectangles are present
		assert len(bank_all.rectangles) == 6
		for r in [r1, r2, r3, r4, r5, r6]:
			assert r in bank_all.rectangles

		# Check that original IDs of special bank are preserved
		assert bank_all.rectangles[bank_all.ids_map[0]] == r1
		assert bank_all.rectangles[bank_all.ids_map[1]] == r2

		# Check that original IDs of other banks are not preserved
		matches = []
		for r, oid in zip():
			if bank_all.rectangles[bank_all.ids_map[oid]] == r:
				matches.append(oid)
		assert len(matches) < 4

		# Check that constraints are reconciled
		assert bank_all.constraints()['Coord1'] == [0, 6]
		assert bank_all.constraints()['Coord2'] == [1, 7]

	def test_rectangle_in_list(self):
		"""Test checking if a rectangle is in a list"""
		r = _sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])
		r2 = _sample_rectangle([1.1, 1.1, 1.1], [1.2, 1.2, 1.2])
		bank = cbc.Bank(rectangles=[r, r2])
		assert r in bank.rectangles
		assert r2 in bank.rectangles
		assert r not in bank.rectangles[1:]

		r.divide(reuse_g=True)

		bank.rectangles.remove(r)
		assert r not in bank.rectangles

	def test_serialization(self):
		"""Test serializing and deserializing a bank"""
		r = _sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])
		r2 = _sample_rectangle([1.1, 1.1, 1.1], [1.2, 1.2, 1.2])
		r3 = _sample_rectangle([1.2, 1.2, 1.2], [1.3, 1.3, 1.3])
		r4 = _sample_rectangle([0.9, 0.9, 0.9], [1.0, 1.0, 1.0])

		r.update_meta(test=1.0, test2=10, test3=None)

		bank = cbc.Bank(rectangles=[r, r2],
						exclusions_branched=[r3],
						exclusions_trimmed=[r4])

		# Serialize and deserialize
		with tempfile.TemporaryDirectory() as tmp_dir:
			tmp_path = pathlib.Path(tmp_dir) / 'test_bank.h5'

			# Serialize
			bank.save(tmp_path.as_posix())

			# Deserialize
			bank2 = cbc.Bank.load(tmp_path.as_posix())

		# Check that the banks are the same
		assert bank.rectangles == bank2.rectangles
		assert bank.ids.tolist() == bank2.ids.tolist()
		assert bank2.rectangles[0].meta['test'] == 1.0

		assert isinstance(bank2.rectangles[0].meta['test2'], (int, numpy.int64))
		assert bank2.rectangles[0].meta['test2'] == 10
		assert bank2.rectangles[0].meta['test3'] is None

		assert bank.exclusions['branching'] == bank2.exclusions['branching']
		assert bank.exclusions['trimming'] == bank2.exclusions['trimming']


	def test_default_constraint_set_name(self):
		"""Test the default constraint set name"""
		r = _sample_rectangle([1.0, 1.0, 1.0], [1.1, 1.1, 1.1])
		r2 = _sample_rectangle([1.1, 1.1, 1.1], [1.2, 1.2, 1.2])
		bank = cbc.Bank(rectangles=[r, r2], )

		assert bank.default_constraint_set_name is None

		# Add a constraint set
		bank.add_constraint_set('sample', {'Coord1': [1, 5], 'Coord2': [2, 3]}, make_default=True)
		assert bank.default_constraint_set_name == 'sample'

		# Add another constraint set
		bank.add_constraint_set('sample2', {'Coord1': [1, 5], 'Coord2': [2, 3]})
		assert bank.default_constraint_set_name == 'sample'

		# Remove the default constraint set
		bank.remove_constraint_set('sample')
		assert bank.default_constraint_set_name is None

		# Check default on creation given 1 set
		bank = cbc.Bank(rectangles=[r, r2], constraint_sets={'sample3': {'Coord1': [1, 5], 'Coord2': [2, 3]}})
		assert bank.default_constraint_set_name == 'sample3'

