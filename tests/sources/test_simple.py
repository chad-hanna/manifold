"""Unit tests for the cbc_metric module

"""
import numpy

import manifold.utilities.common
import manifold.utilities.data
from manifold.sources import simple

TEST_PSD = (manifold.utilities.data.DATA_ROOT / 'psd_for_treebank.xml.gz').as_posix()
TEST_REUSE_G_MM = 0.0
TEST_FREQ_LOW = 15
TEST_FREQ_HIGH = 512


class TestDampedSinMetric:
    """Group for tests simple metric"""

    def test_num_coords(self):
        """Test density of coords over {m1,m2} \in [1, 400]^2"""
        g = simple.DampedSinMetric(manifold.utilities.data.read_psd(TEST_PSD, 'H1', verbose=False),
                                   simple.coord_funcs['omega_lam'](
                                       **manifold.utilities.common.bounds_from_lists(['omega', 'lam'],
                                                                                     [1, 1],
                                                                                     [2, 2])),
                                   TEST_FREQ_LOW, TEST_FREQ_HIGH)

        center = numpy.array([50, 1])
        matrix = g(center)
        exp = numpy.array([[0.112592, 0.000711],
                           [0.000711, 0.116608]])
        numpy.testing.assert_array_almost_equal(matrix, exp, )
