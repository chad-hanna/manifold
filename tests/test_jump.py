"""Unit tests for jump utility module
"""
import numpy

from manifold.utilities import jump


class TestJump:
	"""Test group for"""

	def test_random_unit_sphere(self):
		"""Test"""
		res = jump.random_unit_sphere(2, 10)
		assert res.shape == (10, 2)

	def test_random_radial_scalar(self):
		"""Test"""
		res = jump.random_radial_scalar(2, 10)
		assert res.shape == (10,)

	def test_unit_sphere_to_ellipsoid(self):
		"""Test"""
		ell_mat = numpy.array([[1, 0], [0, 1]])
		unit_vec = numpy.array([1, 0])
		res = jump.unit_sphere_to_ellipsoid(ell_mat, unit_vec)
		assert res.shape == (2,)

	def test_random_point_within_ellipsoid(self):
		"""Test"""
		ell_mat = numpy.array([[1, 0], [0, 1]])
		r_vec = numpy.array([1, 0])
		r_rad = 1
		res = jump.random_point_within_ellipsoid(r_vec, r_rad, ell_mat)
		assert res.shape == (2,)

