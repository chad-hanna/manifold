"""Tests for coordinate module"""
from typing import Union

import numpy

from manifold import coordinate


class SC1(coordinate.CoordinateType):
	key = 'sc1'


class LogSC1(coordinate.CoordinateType):
	key = 'logsc1'


class SCoordFunc(coordinate.CoordFunc):
	_STANDARD_COORD_TYPES = (SC1,)
	TO_TYPES = (SC1,)
	FROM_TYPES = (LogSC1,)

	def __call__(self, c: Union[coordinate.Coordinates, coordinate.CoordsDict], inv: bool = False):
		if inv:
			c = self._normalize_coorddict(c)
			logsc1 = numpy.log10(c[SC1])
			return numpy.array(logsc1)
		else:
			sc1 = 10 ** c[..., 0]
			return {SC1: float(sc1)}




class TestCoords:
	"""Test group"""

	def test_bounds(self):
		"""Test bounds units"""
		bounds_sc1 = {
			SC1.key: [10, 100]
		}
		bounds_logsc1 = {
			LogSC1.key: [1, 2]
		}

		valid_sc1 = {
			SC1: 50
		}
		valid_logsc1 = {
			LogSC1: 1.5
		}


		invalid_sc1 = {
			SC1: 1
		}
		invalid_logsc1 = {
			SC1: 0
		}

		f = SCoordFunc(**bounds_sc1)
		f_log = SCoordFunc(**bounds_logsc1)

		assert valid_sc1 in f
		assert valid_logsc1 in f
		assert invalid_sc1 not in f
		assert invalid_logsc1 not in f

		assert valid_sc1 in f_log
		assert valid_logsc1 in f_log
		assert invalid_sc1 not in f_log
		assert invalid_logsc1 not in f_log



