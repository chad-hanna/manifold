"""Unit tests for the util module

"""
import lal

import manifold.utilities.common
import manifold.utilities.data
import testtools


class TestUtil:
	"""Group for tests"""

	def test_bounds_from_lists(self):
		"""Test bounds from lists"""
		names = ['A', 'B', 'C']
		mins = [1.0, 2.0, 3.0]
		maxs = [4.0, 5.0, 6.0]
		res = manifold.utilities.common.bounds_from_lists(names, mins, maxs)
		assert res == {'A': (1.0, 4.0), 'B': (2.0, 5.0), 'C': (3.0, 6.0)}

	def test_read_psd(self):
		"""Test read PSD data"""
		res = manifold.utilities.data.read_psd(testtools.TEST_DATA_PSD_PATH.as_posix(), 'H1')
		assert isinstance(res, lal.REAL8FrequencySeries)

	def test_read_psd_path(self):
		"""Test read PSD data pathlib"""
		res = manifold.utilities.data.read_psd(testtools.TEST_DATA_PSD_PATH, 'H1')
		assert isinstance(res, lal.REAL8FrequencySeries)
