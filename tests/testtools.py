"""Testing utilities"""

import pathlib

TEST_ROOT = pathlib.Path(__file__).parent
TEST_DATA_ROOT = TEST_ROOT / 'data'
TEST_DATA_PSD_PATH = TEST_DATA_ROOT / 'psd_for_treebank.xml.gz'




