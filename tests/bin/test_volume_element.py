"""Integration test for volume_element.py script"""
import numpy
import pytest
from numpy import testing

import manifold.utilities.data
import manifold.utilities.testing


@pytest.mark.skip(reason="Script import is broken")
def test_script():
	"""Test that script executes and returns expected output"""
	with manifold.utilities.testing.PathManager(add_paths=[manifold.utilities.data.SCRIPT_ROOT.as_posix()]):
		import volume_element
		args = [
			'--min-m1', '10.0',
			'--min-m2', '10.0',
			'--max-m1', '100.0',
			'--max-m2', '100.0',
			'--psd-xml', manifold.utilities.data.TREEBANK_PSD.as_posix(),
			'--freq_low', '10',
			'--freq_high', '512',
		]
		res = volume_element.main(args=args, show_plot=False)
		exp = numpy.array([1.123033, -0.42372427, -0.09428057])
		testing.assert_allclose(res[:3], exp, rtol=1e-5)
