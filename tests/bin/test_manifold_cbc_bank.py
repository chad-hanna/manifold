"""Integration test for manifold_cbc_bank.py script"""
import numpy
import pytest
from numpy import testing

import manifold.utilities.data
import manifold.utilities.testing

DATA_PATH = manifold.utilities.data.DATA_ROOT / 'psd_for_treebank.xml.gz'

@pytest.mark.skip(reason="Script import is broken")
def test_script():
	"""Test that script executes and returns expected output"""
	with manifold.utilities.testing.PathManager(add_paths=[manifold.utilities.data.SCRIPT_ROOT.as_posix()]):
		import manifold_cbc_bank
		args = [
			'--name-x=m1', '--name-x=m2',
			'--min-x=10', '--min-x=10',
			'--max-x=100', '--max-x=100',
			'--mm=1.0'
		]
		res = manifold_cbc_bank.main(args=args, show_plot=False, verbose=False)
		# TODO fix this test fully after refactoring bank script
		if res:
			exp = numpy.array([[10.703125, 11.40625],
							   [12.109375, 11.40625]])
			testing.assert_allclose(res[:2], exp, rtol=1e-5)
