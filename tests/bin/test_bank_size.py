"""Integration test for estimate_bank_size.py script"""

import logging

import pytest

from manifold.utilities import data, testing

logging.basicConfig(filename='test_bank_size.log', level=logging.DEBUG)


@pytest.mark.skip(reason="Script import is broken")
def test_script(caplog):
	"""Test that script executes and returns expected output"""
	caplog.set_level(logging.INFO)

	with testing.PathManager(add_paths=[data.SCRIPT_ROOT.as_posix()]):
		import estimate_bank_size
		args = [
			# Grid sizes
			'--m1-size', '10',
			'--m2-size', '10',
			'--chi-size', '10',

			# Log masses
			'--min-m1', '0.1',
			'--max-m1', '3.0',
			'--min-m2', '0.1',
			'--max-m2', '3.0',

			# Chi
			'--max-chi', '0.05',
			'--min-chi', '-0.05',

			# PSD file
			'--psd-xml-gz', data.O3A_PSD.as_posix(),

			# Frequencies
			'--flow', '15.',
			'--fhigh', '512.',
		]
		estimate_bank_size.main(args=args)
