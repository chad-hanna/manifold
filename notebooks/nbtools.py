"""Utils for setting up notebook environments

"""

import sys
import pathlib

REPO_DIR = pathlib.Path(__file__).parent.parent
PACKAGE_DIR = REPO_DIR

def setup_nb():
	sys.path.append(PACKAGE_DIR.as_posix())


