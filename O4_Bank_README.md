## How to make an O4 CBC template bank and mass model (Updated Dec 11 2023)


### Make a singularity build

1. Make a build directory (e.g., ```singularity``` or ```build```). 

2. In the build directory, run 

```
singularity build --sandbox --fix-perms <build-name> docker://containers.ligo.org/lscsoft/gstlal:master
```

This will create a singularity build with the gstlal master branch. 
You can set ```<build-name>``` to what you like.
Including the analysis type (e.g., ```ssm```) and date or git hash of the recent commit can be helpful.

3. If you're running this on ICDS, run:

```
cd <build-name>
mkdir ligo
cd ..
```

4. Run the following to get into singularity:

On ICDS:

```
singularity run -B /ligo --writable <build-name>
```

On other clusters:

```
singularity run --writable <build-name>
```

```--writable``` lets you modify the build.
Hereafter, whenever there is a ```singularity run``` command, ```-B /ligo``` is needed if you're doing it on ICDS, if on other clusters, it is not needed.

5. After getting into singularity, run:

```
cd <build-name>
mkdir src
cd src
```

6. Clone the manifold git repo inside ```src``` with:

```
git clone https://git.ligo.org/chad-hanna/manifold.git
```

This is for the main branch of manifold. 
If using a different branch, add ```-b <branch name>``` in the command above. 

7. Install ```manifold```:

```
cd manifold/
python3 setup.py install --old-and-unmanageable
```

8. Install ```numdifftools```

```
pip install numdifftools
```


9. Get out of singularity with ```exit```. 


### Modifying a singularity build

- If modifying ```gstlal``` code in the singularity build, get into singularity with:

```
singularity run --writable <path to your build>
```

make changes, and inside ```gstlal``` where ```gstlal```, ```gstlal-inspiral```, ```gstlal-burst```, and ```gstlal-ugly```, run:

```
cd gstlal && echo | ./00init.sh && ./configure --prefix /usr && make && make install && cd .. && cd gstlal-burst && echo | ./00init.sh && ./configure --prefix /usr && make && make install && cd .. && cd gstlal-inspiral && echo | ./00init.sh && ./configure --prefix /usr && make && make install && cd .. && cd gstlal-ugly && echo | ./00init.sh && ./configure --prefix /usr && make && make install && cd ..
```

- If modifying ```manifold``` code in the singularity build, get into singularity with:

```
singularity run --writable <path to your build>
```

make changes, add necessary script names to ```setup.py```, run

```
rm -rf /usr/local/lib/python3.6/site-packages/manifold*
python3 setup.py install --old-and-unmanageable
```

- If installing ```numdifftools``` after the build is made, one can get into singularity with ```--writable``` and run ```pip install numdifftools```, or run 
```
run singularity exec <path to your build> python3 -m pip install --user numdifftools
```


### Generating a bank

1. Make a run directory. 

2. Copy over the PSD file and congif yaml in ```examples``` in your singularity build. 

3. Edit the config yaml with desired template bank designs. 

4. Genearate a bank generation dag with 

```
singularity exec <path to your build> manifold_cbc_bank_dag --singularity-image <path to your build> <path to the config yaml>
```

5. Submit the bank generation dag with 

```
condor_submit_dag manifold_cbc_bank.dag
```

6. Monitor the dag with 

```
tail -f manifold_cbc_bank.dag.dagman.out
```

7. To plot a bank, run:

```
singularity exec <path to your build> manifold_cbc_bank_plot --output <output file name.png> <path to template bank hdf5 file>
```

Note that this dag will run a bank sim as well, and produce bank sim plots. 


### Generating a mass model

1. Genearate a mass model dag with 

```
singularity exec <path to your build> manifold_cbc_bank_mass_model_dag --singularity-image <path to your build> --output-h5 <name of outputfile.h5> --bank-file <path to the template bank h5 file> 
```

2. Submit the mass model dag with 

```
condor_submit_dag manifold_cbc_bank_mass_model.dag
```

3. Monitor the dag with 

```
tail -f manifold_cbc_bank_mass_model.dag.dagman.out
```


### Generating a bank sim only dag

1. Genearate a bank sim only dag with 

```
singularity exec <path to your build> manifold_cbc_bank_test_dag --singularity-image <path to your build> <path to the config yaml>
```

2. Submit the bank sim only dag with 

```
condor_submit_dag manifold_cbc_bank_test.dag
```

3. Monitor the dag with 

```
tail -f manifold_cbc_bank_test.dag.dagman.out
```





