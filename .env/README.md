# Curating Environments

Locking environments provides a few key benefits:

- Reproducibility
- Security
- Speed (no solving of environment required, NP-hard problem)

## Structure and Steps

Files are stored in the `.env` directory. The following structure is used:

- Base environment files stored with `base` in name
- Locked environment files stored with `lock` in name

Steps to curate environments:

1. Specify base environment in `manifold-base.yml`
2. Lock environments for each platform
3. Render environments for each platform
4. Create environments from locks (optional)

## Requirements

- `conda-lock` installed into base environment

## Locking envs

```bash
conda-lock lock -f manifold-base.yml -p osx-arm64 -p linux-64 -p osx-64 -p linux-aarch64 --filename-template "manifold-{platform}.lock" --lockfile "conda-lock-dev.yml"
```

## Rendering envs

```bash
conda-lock render -p linux-64 -p osx-64 -p osx-arm64 -p linux-aarch64 --filename-template "manifold-dev-{platform}.lock" "conda-lock-dev.yml"
```

## Creating envs from locks

Note: you must deactivate your current env before creating a new one if you're in `manifold-dev` env, which you can do
with `conda deactivate`.

```bash
conda create -n manifold-dev --file conda-manifold-dev-osx-arm64.lock --force
```

