"""Data loading utilities

"""
import pathlib
from typing import Union

import lal
from lal import series
from ligo.lw import utils as ligolw_utils

PACKAGE_ROOT = pathlib.Path(__file__).parent.parent
DATA_ROOT = PACKAGE_ROOT.parent / 'tests' / 'data'
SCRIPT_ROOT = PACKAGE_ROOT.parent / 'bin'
TREEBANK_PSD = DATA_ROOT / 'psd_for_treebank.xml.gz'
O3A_PSD = DATA_ROOT / 'REF.xml.gz'


def read_psd(path: Union[str, pathlib.Path], detector: str, verbose: bool = False) -> lal.REAL8FrequencySeries:
	"""Load PSD (power spectral density) from a file for a particular detector

	Args:
		path:
			str or Path, the location of the data file
		detector:
			str, the name of the detector for which to extract data from path
		verbose:
			bool, default False, if True load verbosely using ligo.lw.utils.load_filename(..., verbose=True)

	Returns:
		REAL8FrequencySeries, the psd series
	"""
	path = path.as_posix() if isinstance(path, pathlib.Path) else path
	raw_data = ligolw_utils.load_filename(path, verbose=verbose, contenthandler=series.PSDContentHandler)
	return series.read_psd_xmldoc(raw_data)[detector]