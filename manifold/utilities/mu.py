"""This module contains an implementation of CBC metric using the {mu1, mu2, mu3} coordinates

References:
     [1] Morisaki, Soichiro, and Vivien Raymond. “Rapid Parameter Estimation of Gravitational Waves from Binary Neutron
         Star Coalescence Using Focused Reduced Order Quadrature.” Physical Review D 102, no. 10 (November 9, 2020):
         104020. https://doi.org/10.1103/PhysRevD.102.104020.


Future Work:
	Constraint functions:
		Currently silly architecture (define static method on class, reference in a script wrapped by a trivial
		inner function). Refactor to prevent duplicate definitions of common constraint functions, and pass by value
"""
import functools
from typing import Tuple, Union

import lal
import numpy
from scipy import optimize

from manifold import coordinate
from manifold.sources import cbc

PN_NORM_CONST = lal.G_SI * lal.MSUN_SI / lal.C_SI ** 3
INTERP_CT_M_SYM_FROM_PSI = None  # This will be duck punched by scripts and used in coordinate transformations


################################################################################
#                              MASS VARIABLES                                  #
################################################################################

def m_sym(m_1: float, m_2: float) -> float:
	"""Symmetric mass

	Args:
		m_1:
			float, first mass
		m_2:
			float, second mass

	References:
		[1] See module reference [1], equation 23

	Returns:
		float, eta intermediate variable

		Latex: \frac{m_1 m_2}{(m_1 + m_2)^2}
	"""
	return (m_1 * m_2) / ((m_1 + m_2) ** 2)


def m_chirp(m1, m2):
	return ((m1 * m2) ** (3 / 5)) / ((m1 + m2) ** (1 / 5))


def m_sym_tilde(m_sym):
	"""Intermediate symmetric mass transformation useful for simplifying (m_chirp, m_sym) -> (m1, m2) calc

	Args:
		m_sym:

	Notes:
		LaTeX:
			$\tilde{\eta} = \sqrt{1 - 4 \eta}$

	Returns:

	"""
	return (1 - 4 * m_sym) ** (1 / 2)


def m_tilde(m_chirp, m_sym):
	"""Intermediate mass variable useful for simplifying (m_chirp, m_sym) -> (m1, m2) calc

	Args:
		m_chirp:
		m_sym:

	Notes:
		LaTeX:
			$\tilde{m} = \mathcal{M} \sqrt[5]{\tilde{\eta} \left(\eta^{2} - 3 \eta + 1\right) + 5 \eta^{2} - 5 \eta + 1}$

	Returns:

	"""
	_m_sym_tilde = m_sym_tilde(m_sym)
	return m_chirp * (_m_sym_tilde * (m_sym ** 2 - 3 * m_sym + 1) + (5 * m_sym ** 2 - 5 * m_sym + 1)) ** (1 / 5)


def m1(m_chirp, m_sym):
	"""Smaller mass of binary in terms of chirp and symmetric masses

	Args:
		m_chirp:
		m_sym:

	Notes:
		LaTeX:
			$m_{1} = \frac{2^{\frac{4}{5}} \tilde{m} \left(- \tilde{\eta} - 2 \eta + 1\right)}{4 \eta^{\frac{8}{5}}}$

	Returns:

	"""
	t_eta = m_sym_tilde(m_sym)
	t_m = m_tilde(m_chirp, m_sym)
	return 2 ** (4 / 5) * t_m * (-t_eta - 2 * m_sym + 1) / (4 * m_sym ** (8 / 5))


def m2(m_chirp, m_sym):
	"""Larger mass of binary in terms of chirp and symmetric masses

	Args:
		m_chirp:
		m_sym:

	Notes:
		LaTeX:
			$m_{2} = \frac{2^{\frac{4}{5}} \tilde{m}}{2 \eta^{\frac{3}{5}}}$

	Returns:

	"""
	t_m = m_tilde(m_chirp, m_sym)
	return 2 ** (4 / 5) * t_m / (2 * m_sym ** (3 / 5))


################################################################################
#                              SPIN VARIABLES                                  #
################################################################################


def chi(m: float, s_x: float, s_y: float, s_z: float) -> float:
	"""Dimensionless spin projection

	Args:
		m:
			float, mass
		s_x:
			float, x-component of spin
		s_y:
			float, y-component of spin
		s_z:
			float, z-component of spin

	Returns:
		float, dimensionless spin

		LaTex: \chi_k = |\vec{S}_k| / m_k^2
	"""
	spin_vec = numpy.array([s_x, s_y, s_z])
	return numpy.linalg.norm(spin_vec) / (m ** 2)


def beta(m1, m2, chi):
	"""Intermediate variable beta

	Args:
		m_1:
			float, first mass
		m_2:
			float, second mass
		chi_1:
			float, first effective spin
		chi_2:
			float, second effective spin

	Notes:
		LaTeX:
			$\frac{1}{12} \sum_{k=1}^2 \left[113 (\frac{m_k}{M})^2 + 75 \eta \right] \chi_k$

	References:
		[1] See module reference [1], equation 24

	Returns:
		float, beta intermediate variable
	"""
	_m_sym = m_sym(m1, m2)
	return (chi / 12) * ((113 * (m1 / (m1 + m2)) ** 2 + 75 * _m_sym) + (113 * (m2 / (m1 + m2)) ** 2 + 75 * _m_sym))


def chi_from_beta(m_sym, beta):
	"""Chi from intermediate spin variable beta

	Args:
		m_sym:
		beta:

	Notes:
		LaTeX:
			$\chi = - \frac{12 \beta}{76 \eta - 113}$

	Returns:

	"""
	return -1 * (12 * beta) / (76 * m_sym - 113)


################################################################################
#                    POST-NEWTONIAN PHASE COEFFICIENTS                         #
################################################################################


def psi_0(f_ref: float, m_chirp: float) -> float:
	"""Psi^0 PN phase coefficient

	Args:
		f_ref:
			float, reference frequency
		m_chirp:
			float, chirp mass

	References:
		[1] See module reference [1], equation 19

	Returns:
		float, psi^0 phase coefficient
	"""
	return 0.75 * (8.0 * numpy.pi * m_chirp * f_ref * PN_NORM_CONST) ** (-5.0 / 3.0)


def psi_2(f_ref: float, m_chirp: float, m_sym: float) -> float:
	"""Psi^2 PN phase coefficient

	Args:
		f_ref:
			float, reference frequency
		m_chirp:
			float, chirp mass
		m_sym:
			float, intermediate variable eta

	References:
		[1] See module reference [1], equation 20

	Returns:
		float, psi^2 phase coefficient
	"""
	_psi_0 = psi_0(f_ref, m_chirp)
	return (20.0 / 9.0) * (743.0 / 336.0 + (11.0 / 4.0) * m_sym) * (m_sym ** -0.4) * \
		((numpy.pi * m_chirp * f_ref * PN_NORM_CONST) ** (2.0 / 3.0)) * _psi_0


def psi_3(f_ref: float, m_chirp: float, m_sym: float, beta: float) -> float:
	"""Psi^3 PN phase coefficient

	Args:
		f_ref:
			float, reference frequency
		m_chirp:
			float, chirp mass
		m_sym:
			float, intermediate variable eta
		beta:
			float, intermediate variable beta

	References:
		[1] See module reference [1], equation 21

	Returns:
		float, psi^3 phase coefficient
	"""
	_psi_0 = psi_0(f_ref, m_chirp)
	return (4 * beta - 16 * numpy.pi) * (m_sym ** (-0.6)) * (numpy.pi * m_chirp * f_ref * PN_NORM_CONST) * _psi_0


################################################################################
#                    FORWARD COORDINATE TRANSFORMATIONS                        #
################################################################################


_PHASE_TO_MU_DEFAULT_MATRIX = numpy.array([
	[0.974, 0.209, 0.0840],
	[-0.221, 0.823, 0.524],
	[0.1, 0.1, 0.1],  # TODO replace these with derived values omitted from paper
])

_PHASE_TO_MU_DEFAULT_MATRIX_INV = numpy.linalg.inv(_PHASE_TO_MU_DEFAULT_MATRIX)


def transform_standard_to_phase(m_1: float, m_2: float, chi: float, f_ref: float) -> Tuple[float, float, float]:
	"""Convert CBC standard coordinates to pn phase coordinates

	Args:
		m_1:
			float, first mass
		m_2:
			float, second mass
		s1_x:
			float, x-component of first spin
		s1_y:
			float, y-component of first spin
		s1_z:
			float, z-component of first spin
		s2_x:
			float, x-component of second spin
		s2_y:
			float, y-component of second spin
		s2_z:
			float, z-component of second spin

	References:
		[1] See module reference [1] equations 19-22

	Returns:
		Tuple[float, float, float, float], (psi_0, psi_2, psi_3, psi_4)
	"""
	# compute chirp and effective spins
	_m_chirp = m_chirp(m_1, m_2)

	# compute intermediate variables
	_m_sym = m_sym(m_1, m_2)
	_beta = beta(m_1, m_2, chi=chi)

	# compute pn phase coefficients
	_psi_0 = psi_0(f_ref, _m_chirp)
	_psi_2 = psi_2(f_ref, _m_chirp, _m_sym)
	_psi_3 = psi_3(f_ref, _m_chirp, _m_sym, _beta)

	return _psi_0, _psi_2, _psi_3


def transform_phase_to_mu(psi_0: float, psi_2: float, psi_3: float, u_matrix: numpy.ndarray = None) -> \
		Tuple[float, float, float]:
	"""Transform PN phase coefficient coordinates to mu coordinates by means of a linear transformation.

	Args:
		psi_0:
			float, the first phase coefficient
		psi_2:
			float, the second phase coefficient
		psi_3:
			float, the third phase coefficient
		u_matrix:
			ndarray, the 3x3 array representing the transformation from phase to mu. If none specified,
			the values from module reference [1] will be used.

	References:
		[1] see module reference [1] equation 30

	Returns:
		Tuple[float, float, float], (mu_1, mu_2, mu_3)
	"""
	if u_matrix is None:
		print('using default')
		u_matrix = _PHASE_TO_MU_DEFAULT_MATRIX
	return tuple(numpy.matmul(u_matrix, numpy.array([psi_0, psi_2, psi_3])))


def transform_standard_to_mu(m_1: float, m_2: float, chi: float, f_ref: float, u_matrix: numpy.ndarray = None) -> Tuple[float, float, float]:
	"""Convert CBC standard coordinates to mu coordinates. Helper function around the two intermediate transformations

	Args:
		m_1:
			float, first mass
		m_2:
			float, second mass
		s1_x:
			float, x-component of first spin
		s1_y:
			float, y-component of first spin
		s1_z:
			float, z-component of first spin
		s2_x:
			float, x-component of second spin
		s2_y:
			float, y-component of second spin
		s2_z:
			float, z-component of second spin
		u_matrix:
			ndarray, the 3x3 array representing the transformation from phase to mu. If none specified,
			the values from module reference [1] will be used.

	References:
		[1] See module reference [1] equations 19-22, 30

	Returns:
		Tuple[float, float, float], (mu_1, mu_2, mu_3)
	"""
	psi = transform_standard_to_phase(m_1, m_2, chi, f_ref)
	return transform_phase_to_mu(psi[0], psi[1], psi[2], u_matrix)


################################################################################
#                      INVERSE COORDINATE TRANSFORMATIONS                      #
################################################################################


def m_chirp_from_psi(psi0, f_ref):
	return (2 ** (4 / 5) * 3 ** (3 / 5)) / (32 * numpy.pi * psi0 ** (3 / 5) * f_ref * PN_NORM_CONST)


def _m_sym_psi_poly(m_sym, psi0, psi2):
	return (2104795360483200000 * psi0 ** 3) * m_sym ** 5 + \
		(8462461866012000000 * psi0 ** 3) * m_sym ** 4 + \
		(13609543650318000000 * psi0 ** 3) * m_sym ** 3 + \
		(10943604904963500000 * psi0 ** 3 - 449558700701515776 * psi2 ** 5) * m_sym ** 2 + \
		(4399945045664437500 * psi0 ** 3) * m_sym ** 1 + \
		(707610209724821875 * psi0 ** 3)  # n^0


def _m_sym_psi_expr(m_sym, psi0, psi2, f_ref=200):
	_mcsec = (4 / 3. * psi0) ** (-.6) / 8 / numpy.pi / f_ref
	_eta_expression = psi2 / psi0 / (numpy.pi * _mcsec * f_ref) ** (2. / 3)
	return _eta_expression - 14860 / 3024. * m_sym ** (-.4) - 220 / 36. * m_sym ** (.6)



def m_sym_from_psi_ct_interp(p0, p2, f_ref=200):
	try:
		return min(INTERP_CT_M_SYM_FROM_PSI([p0, p2])[0], 0.25)
	except TypeError as e:  # raised by NoneType not callable
		raise ValueError('Cannot compute m_sym interpolated when interpolator is None') from e


def m_sym_from_psi_chad(p0, p2, f_ref=200):
	_mcsec = (4 / 3. * p0) ** (-.6) / 8 / numpy.pi / f_ref
	_eta_expression = p2 / p0 / (numpy.pi * _mcsec * f_ref) ** (2. / 3)

	def eta_root(e, C=_eta_expression):
		return abs(C - 14860 / 3024. * e ** (-.4) - 220 / 36. * e ** (.6))

	_eta = optimize.minimize(eta_root, (0.20,), bounds=((1e-2, .25),), tol=1e-20).x.item()
	# print(f'Chad Minimize Result: psi0={p0}, psi2={p2} -> m_sym={_eta}, Poly: {_m_sym_psi_poly(_eta, p0, p2)}')
	return _eta


def m_sym_from_psi_minimize(psi0, psi2):
	def optimand(m_sym):
		raw = _m_sym_psi_poly(m_sym, psi0=psi0, psi2=psi2) / 1e19
		return abs(raw)  # want minimum at raw = 0

	res = optimize.minimize(optimand, x0=(0.2,), bounds=((0, .25),)).x.item()
	print(f'Minimize Result: psi0={psi0}, psi2={psi2} -> m_sym={res}, Poly: {_m_sym_psi_poly(res, psi0, psi2)}')
	return res


def m_sym_from_psi_rootsolve(psi0, psi2):
	roots = numpy.roots([
		2104795360483200000 * psi0 ** 3,  # n^5
		8462461866012000000 * psi0 ** 3,  # n^4
		13609543650318000000 * psi0 ** 3,  # n^3
		10943604904963500000 * psi0 ** 3 - 449558700701515776 * psi2 ** 5,  # n^2
		4399945045664437500 * psi0 ** 3,  # n^1
		707610209724821875 * psi0 ** 3  # n^0
	])
	# TODO set bounds on root solve
	#     print(roots)
	real_roots = roots[numpy.isreal(roots)]
	if real_roots.size == 0:
		res = numpy.nan
	elif real_roots.size > 1:
		#         print(real_roots)
		res = numpy.real(real_roots[1])
	else:
		res = numpy.real(real_roots[0])

	print(f'RootSolve Result: psi0={psi0}, psi2={psi2} -> m_sym={res}, Poly: {_m_sym_psi_poly(res, psi0, psi2)}')

	if res > 0.25:
		raise ValueError(f'm_sym numerical root soln out of bounds {res}, real roots were: {real_roots}')
	return res


# Assign alias
m_sym_from_psi = m_sym_from_psi_ct_interp
# m_sym_from_psi = m_sym_from_psi_chad
# m_sym_from_psi = m_sym_from_psi_rootsolve
# m_sym_from_psi = m_sym_from_psi_minimize


def beta_from_psi(psi0, psi3, m_sym):
	return 4 * numpy.pi + (4 * 2 ** (1 / 5) * 3 ** (2 / 5) * psi3 * m_sym ** (3 / 5)) / (3 * psi0 ** (2 / 5))


def transform_phase_to_standard(psi_0: float, psi_2: float, psi_3: float, f_ref: float) -> Tuple[float, float, float]:
	# TODO resolve negative coefficients
	# psi_0, psi_2, psi_3 = abs(psi_0), abs(psi_2), psi_3 # KEEP sign on psi3 for signed beta / chi

	_m_chirp = m_chirp_from_psi(psi_0, f_ref)
	_m_sym = m_sym_from_psi(psi_0, psi_2)
	_beta = beta_from_psi(psi_0, psi_3, _m_sym)
	_m1 = m1(_m_chirp, _m_sym)
	_m2 = m2(_m_chirp, _m_sym)
	_chi = chi_from_beta(_m_sym, _beta)
	return _m1, _m2, _chi


def transform_mu_to_phase(mu1: float, mu2: float, mu3: float, u_matrix_inv: numpy.ndarray = None) -> \
		Tuple[float, float, float]:
	"""Transform PN phase coefficient coordinates to mu coordinates by means of a linear transformation.

	Args:
		psi_0:
			float, the first phase coefficient
		psi_2:
			float, the second phase coefficient
		psi_3:
			float, the third phase coefficient
		u_matrix:
			ndarray, the 3x3 array representing the transformation from phase to mu. If none specified,
			the values from module reference [1] will be used.

	References:
		[1] see module reference [1] equation 30

	Returns:
		Tuple[float, float, float], (mu_1, mu_2, mu_3)
	"""
	if u_matrix_inv is None:
		u_matrix_inv = _PHASE_TO_MU_DEFAULT_MATRIX_INV

	return tuple(numpy.matmul(u_matrix_inv, numpy.array([mu1, mu2, mu3])))


def transform_mu_to_standard(mu1: float, mu2: float, mu3: float, f_ref: float, u_matrix_inv: numpy.ndarray = None):
	_psi_0, _psi_2, _psi_3 = transform_mu_to_phase(mu1, mu2, mu3, u_matrix_inv)
	return transform_phase_to_standard(_psi_0, _psi_2, _psi_3, f_ref)


################################################################################
#                          MANIFOLD COORDINATE API                             #
################################################################################

class Mu1(coordinate.CoordinateType):
	"""Mu 1"""
	key = 'mu1'


class Mu2(coordinate.CoordinateType):
	"""Mu 2"""
	key = 'mu2'


class Mu3(coordinate.CoordinateType):
	"""Mu 3"""
	key = 'mu3'


class Mu1Mu2Mu3(cbc.CBCCoordFunc):
	key = "mu1_mu2_mu3"
	# TODO make this attribute more clear, this is only for normalizing coordinate dicts with name lookups
	_STANDARD_COORD_TYPES = cbc.CBCCoordFunc._STANDARD_COORD_TYPES + (cbc.Chi,)

	# TODO generalize the below class attrs to be computed / passed when constructed?
	f_ref = 200
	u_matrix = _PHASE_TO_MU_DEFAULT_MATRIX
	u_matrix_inv = _PHASE_TO_MU_DEFAULT_MATRIX_INV

	TO_TYPES = (cbc.M1, cbc.M2, cbc.Chi)
	FROM_TYPES = (Mu1, Mu2, Mu3)

	SUFFICIENT_CONSTRAINT_KEYS = (cbc.M1, cbc.M1, cbc.Chi,
								  Mu1, Mu2, Mu3)

	def __init__(self, f_ref: float, u_matrix: numpy.ndarray, **kwargs):
		"""Create a Mu CoordFunc"""
		super().__init__(**kwargs)
		self.f_ref = f_ref
		self.u_matrix = u_matrix
		try:
			self.u_matrix_inv = numpy.linalg.inv(u_matrix)
		except Exception as e:
			raise ValueError('Unable to invert given U matrix for Mu CoordFunc') from e

		self._func_kwargs_['f_ref'] = self.f_ref
		self._func_kwargs_['u_matrix'] = self.u_matrix

	def __call__(self, c: Union[coordinate.Coordinates, coordinate.CoordsDict], inv=False):
		if inv:
			c = self._normalize_coorddict(c, inv=True)
			_m1, _m2, _chi = c[cbc.M1], c[cbc.M2], c[cbc.Chi]
			_mu1, _mu2, _mu3 = transform_standard_to_mu(m_1=_m1, m_2=_m2, chi=_chi, f_ref=self.f_ref, u_matrix=self.u_matrix)
			return numpy.array([_mu1, _mu2, _mu3])

		else:  # go from mu to standard
			if len(c.shape) > 1:
				_mu1 = c[..., 0]
				_mu2 = c[..., 1]
				_mu3 = c[..., 2]
				inv_coords = [transform_mu_to_standard(mu1=_mu1, mu2=_mu2, mu3=_mu3, f_ref=self.f_ref, u_matrix_inv=self.u_matrix_inv)
							  for (_mu1, _mu2, _mu3) in numpy.array([_mu1, _mu2, _mu3]).T]
				return [{cbc.M1: _m1, cbc.M2: _m2, cbc.Chi: _chi} for _m1, _m2, _chi in inv_coords]
			else:
				_mu1, _mu2, _mu3 = c
				_m1, _m2, _chi = transform_mu_to_standard(mu1=_mu1, mu2=_mu2, mu3=_mu3, f_ref=self.f_ref, u_matrix_inv=self.u_matrix_inv)
				return {cbc.M1: _m1, cbc.M2: _m2, cbc.Chi: _chi}

		# m1, m2, chi = c[cbc.M1], c[cbc.M2], c[cbc.Chi]
		# mu = transform_standard_to_mu(m_1=m1, m_2=m2, chi=chi, f_ref=self.f_ref)
		# return numpy.array([mu[0], mu[1]])

	@property
	def min_eig_val(self):
		return 0.0  # 25

	def base_step(self, coords):
		return .01

	def target_match(self, c):
		return 1. - 0.7 * numpy.finfo(numpy.float32).eps

	def sufficient_coord_dict(self, v: numpy.ndarray):
		if isinstance(v, dict):
			v = numpy.array([v[Mu1], v[Mu2], v[Mu3]])
		v = v if isinstance(v, numpy.ndarray) else numpy.array(v)
		coord_dict = self(v)
		coord_dict[Mu1] = v[0]
		coord_dict[Mu2] = v[1]
		coord_dict[Mu3] = v[2]
		return coord_dict

	def random(self):
		mu1 = numpy.random.uniform(self.from_bounds[Mu1][0], self.from_bounds[Mu1][1])
		mu2 = numpy.random.uniform(self.from_bounds[Mu2][0], self.from_bounds[Mu2][1])
		mu3 = numpy.random.uniform(self.from_bounds[Mu3][0], self.from_bounds[Mu3][1])
		rp = {Mu1: mu1,
			  Mu2: mu2,
			  Mu3: mu3}
		return rp

