"""Utilities for bounded stochastic sampling on manifolds
"""

import numpy


def random_unit_sphere(dim: int, n_points: int):
	vec = numpy.random.randn(dim, n_points)
	vec /= numpy.linalg.norm(vec, axis=0)
	return vec.T


def random_radial_scalar(dim: int, n_points: int):
	return numpy.random.power(dim, size=n_points)


def unit_sphere_to_ellipsoid(ell_mat: numpy.ndarray, unit_vec: numpy.ndarray) -> numpy.ndarray:
	"""Radially project a unit vector from the origin to the surface of an ellipsoid

	Args:
		ell_mat:
			Array[N, N], the matrix of coefficients that defines the ellipsoid. Must be symmetric and positive definite.
		unit_vec:
			Array[N], the unit vector to be projected.

	Returns:
		Array[N], the projected vector, a point on the surface of the ellipsoid.
	"""
	evals, evecs = numpy.linalg.eig(ell_mat)
	prin_scal = 1 / numpy.sqrt(evals)
	scal_mat = numpy.outer(numpy.ones(prin_scal.size), prin_scal)
	scaled_evecs = scal_mat * evecs
	return numpy.dot(scaled_evecs, unit_vec)


def random_point_within_ellipsoid(r_vec, r_rad, ell_mat, dist_scale: float = 1.0):
	"""

	Args:
		r_vec:
		r_rad:
		ell_mat:
		dist_scale:

	Returns:

	"""
	scaled_ell_mat = ell_mat / (dist_scale ** 2)
	ell_pt = unit_sphere_to_ellipsoid(scaled_ell_mat, r_vec)
	return r_rad * ell_pt


def random_ellipse_point_from_center(center: numpy.ndarray, ell_mat: numpy.ndarray, scale: float = 1.0):
	rand_vec = random_unit_sphere(ell_mat.shape[0], 1)[0]
	rand_scale = random_radial_scalar(ell_mat.shape[0], 1)[0]
	change_vec = random_point_within_ellipsoid(rand_vec, rand_scale, ell_mat=ell_mat, dist_scale=scale)
	return center + change_vec
