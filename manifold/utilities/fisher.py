"""Utilities for computing Fisher matrix from PSD with given PN powers

Notes:
    Generality:
        This code was borrowed from [1] with the express purpose of replicating
        results from [2]. It will be generalized in future versions.

References:
    [1] https://git.ligo.org/soichiro.morisaki/implement_mu1_mu2_into_bilby/-/blob
    /master/GetMu1Mu2.ipynb
    [2] Morisaki, Soichiro, and Vivien Raymond. “Rapid Parameter Estimation of
    Gravitational Waves from Binary Neutron
        Star Coalescence Using Focused Reduced Order Quadrature.” Physical Review D
        102, no. 10 (November 9, 2020):
        104020. https://doi.org/10.1103/PhysRevD.102.104020.
"""

import numpy
from glue.ligolw import utils as ligolw_utils
from lal import series as lalseries
from scipy.integrate import simps


def f_k_value(
        k: float,
        freq: int,
        psd: numpy.ndarray,
        flow: int,
        fhigh: int,
        fref: int=200,
) -> float:
    """Compute the f-k value, defined in the formula below

    Formula:

        int^fhigh_flow ((f / fref)^k / psd(f)) df: float

    Args:
        k: float
        freq: ndarray
            psd's frequency samples
        psd: ndarray
            psd values
        flow: float
        fhigh: float

    Returns:
        float, f-k value
    """
    assert len(freq) == len(psd)
    start = int(numpy.argwhere(freq >= flow)[0])
    end = int(numpy.argwhere(freq <= fhigh)[-1])
    f = freq[start:end]
    p = psd[start:end]
    return simps((f / fref) ** k / p, f)


def fisher_matrix(powers, freq, psd, flow, fhigh, fref=200):
    """Return the global Fisher matrix. Here it is assumed that the gravitational
    waves' phase is expressed as psi0 * (f / fref)^i0 + psi1 * (f / fref)^i1 +

    Args:
        powers: array
            powers of frequencies, i.e. [i0, i1, ... ]
        freq: ndarray
            psd's frequency samples
        psd: ndarray
            psd values
        flow: float
            low-frequency cutoff of integration
        fhigh: float
            high-frequency cutoff of integration

    Returns:
        ndarray, fisher_matrix
    """
    return numpy.array([
        [f_k_value(-7.0 / 3.0 + i + j, freq, psd, flow, fhigh, fref) for j in powers]
        for i in powers
    ]) / f_k_value(-7.0 / 3.0, freq, psd, flow, fhigh, fref)


def marginalize(G, marginalized_idxs):
    """Return the fisher matrix marginalized over specified components.

    Parameters
    ----------
    G: array
        Fisher matrix
    marginalized_idxs: array
        indexes of parameters marginalized over

    Returns
    -------
    marginalized_fisher_matrix: array
    """
    # indexes for remaining parameters after the marginalization
    remaining_idxs = list(range(len(G)))
    for idx in marginalized_idxs:
        remaining_idxs.remove(idx)

    # marginalization following (17) of arXiv:1304.7017
    g_ab = numpy.array(
        [[G[i, j] for j in marginalized_idxs] for i in marginalized_idxs])
    G_ia = numpy.array(
        [[G[i, j] for j in marginalized_idxs] for i in remaining_idxs])
    G_ij = numpy.array(
        [[G[i, j] for j in remaining_idxs] for i in remaining_idxs])
    return G_ij - G_ia.dot(numpy.linalg.inv(g_ab).dot(G_ia.transpose()))


def best_combi_coeff(flow, fhigh, fref, freq, psd):
    """Calculate the best measurable combinations with restricted 1.5PN waveform.

    Parameters
    ----------
    flow : float
        lower frequency limit
    fhigh : float
        higher frequency limit
    fref : float
        reference frequency
    freq : array
        frequency bins of PSD
    psd : array
        PSD

    Returns
    -------
    U : ndarray
        coefficients of \mu^1, \mu^2 and \mu^3
        i.e. \mu^1 = U[0, 0] * \psi^0 + U[0, 1] * \psi^2 + U[0, 2] * \psi^3
             \mu^2 = U[1, 0] * \psi^0 + U[1, 1] * \psi^2 + U[1, 2] * \psi^3
             \mu^3 = U[2, 0] * \psi^0 + U[2, 1] * \psi^2 + U[2, 2] * \psi^3
    """
    powers = [-5. / 3., -1., -2. / 3, 0., 1.]
    G = fisher_matrix(powers, freq, psd, flow, fhigh, fref)
    tildeG = marginalize(G, [3, 4])
    S, UT = numpy.linalg.eig(tildeG)
    U = UT.transpose()
    # Fix the signs of coefficients
    U[0, :] *= numpy.sign(U[0, 0])
    U[1, :] *= numpy.sign(U[1, 1])
    return U


def read_psd(path, ifo):
    psd_file_obj = open(path, "rb")
    psd_xml_obj = ligolw_utils.load_fileobj(
        psd_file_obj,
        contenthandler=lalseries.PSDContentHandler
    )[0]
    psd = lalseries.read_psd_xmldoc(psd_xml_obj)[ifo]
    return psd.f0 + numpy.arange(psd.data.length) * psd.deltaF, psd.data.data
