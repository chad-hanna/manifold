"""CBC Source


Notes from Bank Class:

	# These methods are left here as a breadcrumb for someone who wants to try
	# something clever one day, but it was never really working...
	#
	#	def split_cell(self, ix):
	#		assert (self.sorted)
	#		l,r = self.rectangles[ix].divide()
	#		self.rectangles.pop(ix)
	#		self.pns.pop(ix)
	#
	#		for c in (l, r):
	#			d = self.cfunc(c.center)
	#			pn = _pn_phase(d(c.center))
	#			ix = bisect.bisect(self.pns, pn)
	#			self.pns.insert(ix, pn)
	#			self.rectangles.insert(ix, c)
	#			self.ids.insert(ix, max(self.ids)+1)
	#		self.sorted = False
	#
	#
	#	def refine(self, mm, n = 20000, m = 20, to_check = None, verbose = False):
	#		self.sort()
	#		new_rectangles = []
	#		added = 0
	#
	#		best_matches_rectangles = {}
	#		removed_rectangles = set()
	#		added_rectangles = set()
	#		dim = len(self.rectangles[0].center)
	#		for ix, r in enumerate(self.rectangles):
	#			if verbose and not ix % 1000: print("processing template %d, added %d so far" % (ix, added))
	#			if to_check is not None and r not in to_check:
	#				new_rectangles.append(r)
	#				continue
	#			matches, ixs = self.neighborhood_overlaps(ix, n=n)
	#			# verify with FFT matches if requested
	#			if m:
	#				local_ixs = numpy.argsort(matches)[-m:]
	#				local_matches = r.fft_match([self.rectangles[ixs[local_ix]] for local_ix in local_ixs])
	#				for _n, local_ix in enumerate(local_ixs):
	#					matches[local_ix] = local_matches[_n]
	#			# find the best match "on the diagonal" ignore the best match which is with itself.
	#			best_match_local_ix = numpy.argsort(matches)[-2]
	#			best_match_global_ix = ixs[best_match_local_ix]
	#			match = matches[best_match_local_ix]
	#			best_matches_rectangles.setdefault(self.rectangles[best_match_global_ix], set()).add(r)
	#			if 1-match > 2 * mm:
	#				right, left = r.divide(reuse_g = True)
	#				inright = self.inside(right)
	#				inleft = self.inside(left)
	#				if inright:
	#					new_rectangles.append(right)
	#					added_rectangles.add(right)
	#					added += 1
	#				if inleft:
	#					new_rectangles.append(left)
	#					added_rectangles.add(left)
	#					added += 1
	#				if (not inright) and (not inleft):
	#					new_rectangles.append(r)
	#				else:
	#					added -=1
	#					removed_rectangles.add(r)
	#			else:
	#				new_rectangles.append(r)
	#
	#		if verbose: print("calculating which templates have changed")
	#		to_check = set()
	#		for r in removed_rectangles:
	#			if r in best_matches_rectangles and r not in removed_rectangles:
	#				for r2 in best_matches_rectangles[r]:
	#					to_check.add(r2)
	#		to_check |= added_rectangles
	#
	#		assert not to_check - set(new_rectangles)
	#		if verbose: print("Done")
	#
	#		return Bank(new_rectangles, constraints = self.constraints), to_check
"""
import bisect
# Copyright (C) 2020-2021 Chad Hanna
# Copyright (C) 2014 Miguel Fernandez, Chad Hanna
# Copyright (C) 2016,2017 Kipp Cannon, Miguel Fernandez, Chad Hanna, Stephen Privitera, Jonathan Wang
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
import pathlib
import sys
import types
from typing import Union, List, Dict, Tuple

import lal
import lalsimulation as lalsim
import numpy
from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils
from ligo.lw.utils import process as ligolw_process
from numpy import log10 as l10

from manifold.cover import SizeMethod

try:
	from scipy.special import logsumexp
except ImportError as e:
	from scipy.misc import logsumexp

from scipy.interpolate import splrep, PPoly

from manifold import metric, signal, cover, coordinate
from manifold.utilities import data, common, jump
from manifold import io


# Define CBC Coordinate Types

class M1(coordinate.CoordinateType):
	"""Mass 1"""
	key = 'm1'


class M2(coordinate.CoordinateType):
	"""Mass 2"""
	key = 'm2'


class LogM1(coordinate.CoordinateType):
	"""Log10 Mass 1"""
	key = 'log10m1'


class LogM2(coordinate.CoordinateType):
	"""Log10 Mass 2"""
	key = 'log10m2'


class S1X(coordinate.CoordinateType):
	"""Spin 1 x-component"""
	key = 'S1x'


class S1Y(coordinate.CoordinateType):
	"""Spin 1 y-component"""
	key = 'S1y'


class S1Z(coordinate.CoordinateType):
	"""Spin 1 z-component"""
	key = 'S1z'


class S2X(coordinate.CoordinateType):
	"""Spin 2 x-component"""
	key = 'S2x'


class S2Y(coordinate.CoordinateType):
	"""Spin 2 y-component"""
	key = 'S2y'


class S2Z(coordinate.CoordinateType):
	"""Spin 2 z-component"""
	key = 'S2z'


class Chi(coordinate.CoordinateType):
	key = 'chi'


class MC(coordinate.CoordinateType):
	key = 'mc'


#
# Utility functions to help with the coordinate system for evaluating the waveform metric
#

def expand(x):
	# map [-1,1] -> [-inf, inf]
	return numpy.tan(x * numpy.pi / 2)


def crush(x):
	# map [-inf,inf] -> [-1, 1]
	return (numpy.arctan(x) * 2 / numpy.pi)


def chirpmass(m1, m2):
	return (m1 * m2) ** .6 / (m1 + m2) ** .2


def mi_from_mc_mj(mc, m2):
	p = -mc ** 5 / m2 ** 3
	q = -mc ** 5 / m2 ** 2

	def t(k, p, q):
		return 2 * (-p / 3.) ** .5 * numpy.cos(1. / 3. * numpy.arccos(3 * q / (2 * p) * (-3. / p) ** .5) - 2 * numpy.pi * k / 3.)

	def t3(p, q):
		return -2 * numpy.abs(q) / q * (-p / 3.) ** .5 * numpy.cosh(1. / 3. * numpy.arccosh(-3. * abs(q) / 2. / p * (-3. / p) ** .5))

	out = numpy.array([[t(i, p, q) for i in range(3)] + [t3(p, q)]])
	try:
		return numpy.nanmax(out, axis=1)
	except numpy.core._internal.AxisError:
		return numpy.nanmax(out)


def z_from_m1_m2_s1_s2(m1, m2, s1, s2):
	m1 = float(m1)
	m2 = float(m2)
	s1 = float(s1)
	s2 = float(s2)
	M = m1 + m2
	return (s1 * m1 + s2 * m2) / M


def zn_from_m1_m2_s1_s2(m1, m2, s1, s2):
	m1 = float(m1)
	m2 = float(m2)
	s1 = float(s1)
	s2 = float(s2)
	M = m1 + m2
	return (s1 * m1 - s2 * m2) / M


def s1_from_m1_m2_z_zn(m1, m2, z, zn):
	M = m1 + m2
	return (z + zn) * M / m1 / 2.


def s2_from_m1_m2_z_zn(m1, m2, z, zn):
	M = m1 + m2
	return (z - zn) * M / m2 / 2.


CBC_COORD_TRANSFORMS = {
	# Mapping of (from types), (to types): transform function
	((M1,), (LogM1,)): l10,
	((M2,), (LogM2,)): l10,
	((LogM1,), (M1,)): lambda x: 10 ** x,
	((LogM2,), (M2,)): lambda x: 10 ** x,
	((M1, M2), (MC,)): chirpmass,
	((M1, M2, S1Z, S2Z), (Chi,)): z_from_m1_m2_s1_s2,
}


class CBCCoordFunc(coordinate.CoordFunc):
	_STANDARD_COORD_TYPES = (M1, M2, S1X, S1Y, S1Z, S2X, S2Y, S2Z,)

	def dx(self, c: coordinate.Coordinates):
		return metric.DELTA * numpy.ones(len(c))

	def random(self):
		# TODO find a smarter way to draw uniformly over chi
		# might be a bad idea if people have very different priors for the two components
		if S1Z in self.to_bounds:
			s = numpy.random.uniform(min(self.to_bounds[S1Z][0], self.to_bounds[S2Z][0]), max(
				self.to_bounds[S1Z][1], self.to_bounds[S2Z][1]))
		else:
			s = 0
		m1 = 10 ** numpy.random.uniform(numpy.log10(self.to_bounds[M1][0]), numpy.log10(self.to_bounds[M1][1]))
		m2 = 10 ** numpy.random.uniform(numpy.log10(self.to_bounds[M2][0]), numpy.log10(m1))
		rp = {M1: m1,
			  M2: m2,
			  S1Z: s,
			  S2Z: s,
			  S1X: 0.,
			  S1Y: 0.,
			  S2X: 0.,
			  S2Y: 0.}
		if rp[M1] < rp[M2]:
			m1, s1x, s1y, s1z = rp[M1], rp[S1X], rp[S1Y], rp[S1Z]
			rp[M1], rp[S1X], rp[S1Y], rp[S1Z] = rp[M2], rp[S2X], rp[S2Y], rp[S2Z]
			rp[M2], rp[S2X], rp[S2Y], rp[S2Z] = m1, s1x, s1y, s1z
		return rp

	@property
	def base_step(self):
		return .01


class Log10M1Log10M2(CBCCoordFunc):
	key = "log10m1_log10m2"

	TO_TYPES = (M1, M2)
	FROM_TYPES = (LogM1, LogM2)

	def __call__(self, c: Union[coordinate.Coordinates, coordinate.CoordsDict], inv: bool = False):
		if inv:
			c = self._normalize_coorddict(c, inv=True)
			m1, m2 = c[M1], c[M2]
			return numpy.array([l10(m1), l10(m2)])
		else:
			m1 = 10 ** c[..., 0]
			m2 = 10 ** c[..., 1]
			try:
				return {M1: float(m1), M2: float(m2)}
			except (ValueError, TypeError):  # must be array
				return [{M1: a, M2: b} for (a, b) in numpy.array([m1, m2]).T]

	@property
	def min_eig_val(self):
		return 0.0  # 25

	def base_step(self, coords):
		return .01

	def target_match(self, c):
		return 1. - 0.7 * numpy.finfo(numpy.float32).eps


class Log10M1Log10M2Chi(CBCCoordFunc):
	key = "log10m1_log10m2_chi"

	TO_TYPES = (M1, M2, S1Z, S2Z)
	FROM_TYPES = (LogM1, LogM2, Chi)

	def __call__(self, c: Union[coordinate.Coordinates, coordinate.CoordsDict], inv=False):
		if inv:
			c = self._normalize_coorddict(c, inv=True)
			m1, m2, s1, s2 = c[M1], c[M2], c[S1Z], c[S2Z]
			chi = z_from_m1_m2_s1_s2(m1, m2, s1, s2)
			return numpy.array([l10(m1), l10(m2), chi])
		else:
			m1 = 10 ** c[..., 0]
			m2 = 10 ** c[..., 1]
			s1z = c[..., 2]
			s2z = c[..., 2]
			try:
				return {M1: float(m1), M2: float(m2), S1Z: float(s1z), S2Z: float(s2z)}
			except (ValueError, TypeError):  # must be array
				return [{M1: a, M2: b, S1Z: c, S2Z: d} for (a, b, c, d) in numpy.array([m1, m2, s1z, s2z]).T]

	@property
	def min_eig_val(self):
		return 0.0  # 25

	def base_step(self, coords):
		return .01

	def target_match(self, c):
		return 1 - 0.7 * numpy.finfo(numpy.float32).eps

	def mass_model(self, r, model=None, **kwargs):
		if model == "salpeter":
			return self._salpeter(r, m_min=0.80 if "m_min" not in kwargs else kwargs["m_min"], cuts=None if "cuts" not in kwargs else kwargs['cuts'])
		if model == "power-law":
			return self._power_law(r, index=kwargs['index'], m_min=0.80 if "m_min" not in kwargs else kwargs["m_min"], cuts=None if "cuts" not in kwargs else kwargs['cuts'])
		raise ValueError("model not one of salpeter, power_law")

	def _salpeter(self, r, m_min=0.80, cuts=None):
		return self._power_law(r, index=-2.35, m_min=m_min, cuts=cuts)

	def _power_law(self, r, index=-2.35, m_min=0.80, cuts=None):
		c = self(r.center)
		_m1, _m2, _x1, _x2 = c[M1], c[M2], c[S1Z], c[S2Z]
		if _m2 > _m1:
			m1, m2, x1, x2 = _m2, _m1, _x2, _x1
		else:
			m1, m2, x1, x2 = _m1, _m2, _x1, _x2
		assert m2 > 1.05 * m_min  # NOTE THIS 5% tolerance is arbitrary but meant to prevent people from getting an infinite probability.
		volume = (10 ** r.maxes[0] - 10 ** r.mins[0]) * (10 ** r.maxes[1] - 10 ** r.mins[1]) * r.coordinate_size[2]
		chi = (m1 * x1 + m2 * x2) / (m1 + m2)
		if cuts is not None:
			prob = 0.
			minchi = (m1 * cuts['x1'][0] + m2 * cuts['x2'][0]) / (m1 + m2)
			maxchi = (m1 * cuts['x1'][1] + m2 * cuts['x2'][1]) / (m1 + m2)
			if (cuts['m1'][0] <= m1 <= cuts['m1'][1]) and (cuts['m2'][0] <= m2 <= cuts['m2'][1]) and (minchi <= chi <= maxchi):
				prob = m1 ** index * volume / (m1 - m_min)
		else:
			prob = m1 ** index * volume / (m1 - m_min)
		return (m1, m2, chi), prob

	def array_to_coord_dict(self, a):
		"""Assumes s1z = s2z = chi"""
		return {M1: a[0], M2: a[1], S1Z: a[2], S2Z: a[2]}


#
# Metric object that numerically evaluates the waveform metric
#

class CBCMetric(metric.Metric):
	"""
    Class to numerically evaluate the compact binary parameter space metric.

    Parameters
    ----------
    psd: REAL8FrequencySeries
        A LAL type holding a PSD
    coord_func: coord_func
        A coord_func or derived class that specifies the coordinate transformation.
    duration: int, default4
        The duration of waveform to produce
    flow: float, default=15.
        The minimum frequency of the integration
    fhigh: float, default=512
        The maximum frequency of the integration
    approximant: str, default="IMRPhenomD"
        Currently only TaylorF2 and IMRPhenomD are supported

    Examples
    --------
    >>> psd = io.h5read('test_data/H1L1-MASS_BBH-1185149070-800.h5', 'psd')['L1']
    >>> bounds = {"m1":[1., 100.], "m2":[1., 100.], "S1z":[-1., 1.], "S2z":[-1., 1.]}
    >>> cfunc = metric.m1_m2_s1z_s2z(bounds)
    >>> g = metric.CBCMetric(psd, cfunc)
    >>> g([1.4, 1.4, 0, 0])
    array([[ 4.34726584e+05,  4.34725714e+05, -8.52915457e+03,
            -8.52915457e+03],
           [ 4.34725714e+05,  4.34726584e+05, -8.52915457e+03,
            -8.52915457e+03],
           [-8.52915457e+03, -8.52915457e+03,  1.82025977e+02,
             1.81156190e+02],
           [-8.52915457e+03, -8.52915457e+03,  1.81156190e+02,
             1.82025977e+02]])
    """
	WAVEFORM_COORDS = (M1, M2, S1X, S1Y, S1Z, S2X, S2Y, S2Z)
	WAVEFORM_COORD_DEFAULTS = {S1X: 0., S1Y: 0., S1Z: 0., S2X: 0., S2Y: 0., S2Z: 0.}

	def __init__(self, psd=None, coord_func=None, flow=15.0, fhigh=512., approximant="IMRPhenomD", max_metric_diff: float = metric.DEFAULT_MAX_DIFF_THRESHOLD, max_duration: float = numpy.inf):
		self.appxstr = approximant
		self.approximant = lalsim.GetApproximantFromString(approximant)
		self.max_duration = max_duration
		metric.Metric.__init__(self, psd, coord_func, flow, fhigh, max_metric_diff=max_metric_diff)

	def _to_dict(self):
		return {"psd": self.psd, "coord_func": {self.coord_func.key: self.coord_func.to_dict()}, "flow": self.freq_low, "fhigh": self.freq_high, "approximant": self.appxstr, "max_metric_diff": self.max_diff_threshold,
				"max_duration": self.max_duration}

	@classmethod
	def _from_dict(cls, d):
		# FIXME this is silly - fix how we encode these objects generally and especially the coord functions
		k, v = tuple(d['coord_func'].items())[0]
		d['coord_func'] = coord_funcs[k](**v)

		# FIXME make sure approximate is a string not bytes
		if isinstance(d['approximant'], bytes):
			d['approximant'] = d['approximant'].decode('utf-8')
		return cls(**d)

	def waveform(self, c: coordinate.Coordinates):

		# FIXME replace with something more accurate
		def f_at_t(mc, t):
			M = mc * 4.93e-6  # convert solar mass to seconds
			return 1 / numpy.pi / M * (5. / 256 * M / t) ** (3. / 8)

		# Generalize to different waveform coords
		parameters = self.coord_func(c)

		# FIXME this could be a bad idea since we are taking derivatives.
		parameters[S1Z] = min(max(parameters[S1Z], -0.999), 0.999)
		parameters[S2Z] = min(max(parameters[S2Z], -0.999), 0.999)

		# Figure out if we need to adjust the low frequency to limit waveform duration
		if numpy.isfinite(self.max_duration):
			flow = max(f_at_t(chirpmass(parameters[M1], parameters[M2]), self.max_duration), self.freq_low)
		else:
			flow = self.freq_low

		try:
			parameters[M1] *= lal.MSUN_SI
			parameters[M2] *= lal.MSUN_SI

			parameters['distance'] = 1.e6 * lal.PC_SI
			parameters['inclination'] = 0.
			parameters['phiRef'] = 0.
			parameters['longAscNodes'] = 0.
			parameters['eccentricity'] = 0.
			parameters['meanPerAno'] = 0.
			parameters['deltaF'] = self.freq_interval
			parameters['f_min'] = flow
			parameters['f_max'] = self.freq_high
			parameters['f_ref'] = 0.
			parameters['LALparams'] = None
			parameters['approximant'] = self.approximant

			parameters = self._normalize_waveform_params(parameters)

			hplus, hcross = lalsim.SimInspiralFD(**parameters)

			fseries = hplus
			lal.WhitenCOMPLEX16FrequencySeries(fseries, self.psd)
			fseries = signal.add_quadrature_phase(fseries, self.working_length)

			# TODO why the manual del, is there some lalsim side effect we need to clear?
			del hplus
			del hcross
		except RuntimeError as p:
			# TODO fix this reference / switch to logging
			print(p)
			# raise
			return None
		return fseries


#
# register coord funcs
#

coord_funcs = {
	# Tested coord funcs
	Log10M1Log10M2.key: Log10M1Log10M2,
	Log10M1Log10M2Chi.key: Log10M1Log10M2Chi,
}


#
# Bank specific classes
#

class ManifoldRectangle(cover.ManifoldRectangle):
	@property
	def metric_center(self):
		# FIXME for now this just returns the actual center of the rectangle
		return self.center

	def match(self, other):
		if type(other) == type(self):
			return self.metric.metric_match(self.g, self.center, other.center)
		elif type(other) == numpy.ndarray:
			return self.metric.metric_match(self.g, self.center, other)
		else:
			raise ValueError("argument %s not supported" % type(other))

	def fft_match(self, others):
		w1 = self.metric.waveform(self.center)
		return numpy.array([self.metric.fft_match(w1, self.metric.waveform(other.center)) for other in others])

	@property
	def sngl_row(self):
		# FIXME also include other relevant paramters here that are known e.g., flow, approximant, instrument, etc.
		d = self.metric._normalize_waveform_params(self.metric.coord_func(self.center))
		rdict = {k: 0 for k in (
			"process:process_id", "ifo", "search", "channel", "end_time", "end_time_ns", "end_time_gmst", "impulse_time", "impulse_time_ns", "template_duration", "event_duration", "amplitude", "eff_distance", "coa_phase", "mass1", "mass2", "mchirp",
			"mtotal", "eta", "kappa", "chi", "tau0", "tau2", "tau3", "tau4", "tau5", "ttotal", "psi0", "psi3", "alpha", "alpha1", "alpha2", "alpha3", "alpha4", "alpha5", "alpha6", "beta", "f_final", "snr", "chisq", "chisq_dof", "bank_chisq",
			"bank_chisq_dof", "cont_chisq", "cont_chisq_dof", "sigmasq", "rsqveto_duration", "Gamma0", "Gamma1", "Gamma2", "Gamma3", "Gamma4", "Gamma5", "Gamma6", "Gamma7", "Gamma8", "Gamma9", "spin1x", "spin1y", "spin1z", "spin2x", "spin2y",
			"spin2z",
			"event_id")}
		rdict.update({"ifo": "L1", "process_id": 0, "mass1": d['m1'], "mass2": d['m2'], "spin1x": d['S1x'], "spin1y": d['S1y'], "spin1z": d['S1z'], "spin2x": d['S2x'], "spin2y": d['S2y'], "spin2z": d['S2z']})
		return lsctables.SnglInspiralTable.RowType(**rdict)


class Chart(cover.Chart):
	pass


def _pn_phase(d):
	# From https://arxiv.org/pdf/1107.1267.pdf
	m1, m2, s1, s2 = d[M1] * 5e-6, d[M2] * 5e-6, d[S1Z], d[S2Z]  # mass in seconds
	mc = (m1 * m2) ** .6 / (m1 + m2) ** .2
	return mc


#	m = m1 + m2
#	eta = m1 * m2 / m**2
#	chi = (m1*s1 + m2*s2) / m
#	delta = (m1 - m2) / 2
#	xs = (s1 + s2) / 2.
#	xa = (s1 - s2) / 2.
#	beta = 113 / 12. * (xs + delta * xa - 76 * eta / 113. * xs)
#	sigma = xa**2 * (81/16. - 20 * eta) + 81 * xa * xs / 8. + xs**2 * (81/16. - eta / 4.)
#	gamma = xa * delta * (140 * eta / 9. + 732985 / 2268.) + xs * ( 732985 / 2268. - 24260. * eta / 81 - 340. * eta**2 / 9)
#	def phase(f, m = m, eta = eta, beta = beta, sigma = sigma):
#		v = (numpy.pi * m * f)**(1./3.)
#		return 3. / (128 * eta) * (
#			v**-5 +
#			v**-3 * (55 * eta / 9. +
#				3715/756.) +
#			v**-2 * (4 * beta -
#				16 * numpy.pi) +
#			v**-1 * (3085 * eta**2 / 72. +
#				27145 * eta / 504 +
#				15293365 / 508032 - 10 * sigma) +
#			v**0 * (38645*numpy.pi / 756 -
#				65 * numpy.pi * eta / 9 -
#				gamma) * (3 * numpy.log(v) + 1) +
#			v**1 * (-6848 / 21. * numpy.euler_gamma -
#				127825 * eta**3 / 1296 +
#				76055 * eta**2 / 1728. +
#				(2255 * numpy.pi**2 / 12 - 15737765635 / 3048192.) -
#				640 * numpy.pi**2 / 3 +
#				11583231236531 / 4694215680. -
#				6848 * numpy.log(4 * v) / 21. ) +
#			v**2 * (-74045 * numpy.pi * eta**2 / 756. +
#				378515 * numpy.pi * eta / 1512. +
#				77096675 * numpy.pi / 254016)
#			)
#	# 1 looks pretty good
#	return phase(1024) - phase(1)


class Bank:
	MAX_TEMPLATE_ID = 9999999
	MAX_NUM_TEMPLATES = MAX_TEMPLATE_ID

	def __init__(self, rectangles: List[ManifoldRectangle], constraint_sets: Dict[str, Dict[str, Tuple[float, float]]] = None, ids: List[int] = None, pns=None, process=None, reserved_ids: List[int] = None,
				 default_constraint_set_name: str = None, exclusions_branched: List = None, exclusions_trimmed: List = None):
		self.__process = process
		self.rectangles = rectangles
		self.exclusions = {
			'branching': exclusions_branched or [],
			'trimming': exclusions_trimmed or [],
		}
		self.constraint_sets = constraint_sets or {}
		self.default_constraint_set_name = default_constraint_set_name or (None if len(self.constraint_sets) == 0 else list(self.constraint_sets.keys())[0])
		self.cfunc = None if len(rectangles) == 0 else self.rectangles[0].metric.coord_func
		self.metric = None if len(rectangles) == 0 else self.rectangles[0].metric
		self.ids = ids
		self.pns = pns
		if self.ids is None or self.pns is None:
			# implies now sorted and has pns
			self.assign_ids(reserved_ids=reserved_ids)
		self.centers = numpy.array([r.center for r in self.rectangles])
		self.ids_map = {i: n for n, i in enumerate(self.ids)}
		self.rectangle_meta = {}

	def __len__(self):
		return len(self.rectangles)

	def assign_ids(self, reserved_ids: List[int] = None, reassign: bool = False):
		"""Assign identifiers randomly to templates in the Bank.

		Args:
			reserved_ids:
				List[int], default None, if specified, these ids will not be used in the assignment. This is achieved by removing the reserved ids from the identifier sample space before assigning ids to the templates.
			reassign:
				bool, default False, if True, the method will reassign ids to the templates, even if the bank already has ids assigned.

		Returns:
			None
		"""
		if self.ids is None or reassign:
			# Check for too many templates
			if len(self.rectangles) > self.MAX_NUM_TEMPLATES:
				raise ValueError(f"Too many templates, increase size of draw space. Received {len(self.rectangles)} templates, maximum is {self.MAX_NUM_TEMPLATES}")

			sample_space = set(range(self.MAX_TEMPLATE_ID))
			if reserved_ids is not None:
				sample_space -= set(reserved_ids)

			# Verify remaining sample space is large enough
			if len(sample_space) < len(self.rectangles):
				raise ValueError("Not enough sample space to assign ids")

			self.ids = numpy.random.choice(list(sample_space), len(self.rectangles), replace=False)
		self.__sort()

	def __sort(self):
		rs = sorted([(_pn_phase(r.metric.coord_func(r.center)), n, r) for n, r in zip(self.ids, self.rectangles)])
		self.pns = [r[0] for r in rs]
		self.rectangles = [r[-1] for r in rs]
		self.centers = numpy.array([r.center for r in self.rectangles])
		self.ids = numpy.array([r[1] for r in rs])
		self.ids_map = {i: n for n, i in enumerate(self.ids)}

	def template_by_id(self, tid):
		return self.cfunc(self.centers[self.ids_map[tid]])

	def rectangle_by_id(self, tid):
		return self.rectangles[self.ids_map[tid]]

	def add_constraint_set(self, name: str, constraints: Dict[str, Tuple[float, float]], override: bool = False, make_default: bool = False):
		"""Add a new set of constraints to the bank

		Args:
			name:
				str, name of the constraint set
			constraints:
				Dict[str, Tuple[float, float]], constraints to add to the bank, e.g. {"m1": (1.0, 100.0), "m2": (1.0, 100.0), "S1z": (-1.0, 1.0), "S2z": (-1.0, 1.0)}
			override:
				bool, default False, if True, will overwrite any existing constraint set with the same name
			make_default:
				bool, default False, if True, will make this constraint set the default constraint set for the bank

		Returns:
			None
		"""
		if name in self.constraint_sets and not override:
			raise ValueError(f"Constraint set {name} already exists in bank. Use override=True to overwrite.")
		self.constraint_sets[name] = constraints

		if make_default:
			self.default_constraint_set_name = name

	def remove_constraint_set(self, name: str):
		"""Remove a constraint set from the bank

		Args:
			name:
				str, name of the constraint set to remove

		Returns:
			None
		"""
		if name in self.constraint_sets:
			del self.constraint_sets[name]

		# Make sure to unset the default if removed
		if self.default_constraint_set_name == name:
			self.default_constraint_set_name = None

	def constraints(self, name: str = None):
		"""Get the constraints for the bank

		Args:
			name:
				str, default None, if specified, will return the constraints for the specified constraint set, otherwise will return the constraints for the default constraint set

		Returns:
			Dict[str, Tuple[float, float]], the constraints for the bank
		"""
		if name is None:
			name = self.default_constraint_set_name
		if name is None:
			raise ValueError("No default constraint set specified")

		return self.constraint_sets[name]

	def templates(self):
		for c in self.centers:
			yield self.cfunc(c)

	def split(self):
		assert (self.pns is not None)
		left = [r for r in self.rectangles if r.left]
		idsleft = [ID for ID, r in zip(self.ids, self.rectangles) if r.left]
		pnsleft = [pn for pn, r in zip(self.pns, self.rectangles) if r.left]
		right = [r for r in self.rectangles if r.right]
		idsright = [ID for ID, r in zip(self.ids, self.rectangles) if r.right]
		pnsright = [pn for pn, r in zip(self.pns, self.rectangles) if r.right]
		return Bank(left, self.constraints(), ids=idsleft, pns=pnsleft), Bank(right, self.constraints(), ids=idsright, pns=pnsright)

	def group(self, num):
		assert (self.pns is not None)

		# https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length
		def __split(a, n):
			k, m = divmod(len(a), n)
			return ((i * k + min(i, m), (i + 1) * k + min(i + 1, m)) for i in range(n))

		for (six, eix) in __split(self.rectangles, num):
			yield Bank(self.rectangles[six:eix], self.constraints(), ids=self.ids[six:eix], pns=self.pns[six:eix])

	def inside(self, r, check_vertices=True):
		return Bank._inside(r, self.constraints(), self.cfunc, check_vertices)

	@staticmethod
	def _inside(r: ManifoldRectangle, constraints: List[types.FunctionType], cfunc: CBCCoordFunc, check_vertices: bool):
		out = True
		for func in [Bank.m1_constraint, Bank.m2_constraint, Bank.M_constraint, Bank.mc_constraint, Bank.q_constraint, Bank.chi_constraint]:
			if check_vertices:
				points = list(r.vertices) + [list(r.center)]
			else:
				points = [list(r.center)]
			out &= not all(func(constraints, cfunc(numpy.array(v))) for v in points)
		return out

	def trim(self, verbose=True):
		if verbose:
			print("%d templates before" % len(self.rectangles))

		data = [(r, ID, self.inside(r)) for r, ID in zip(self.rectangles, self.ids)]
		keep = [(x[0], x[1]) for x in data if x[2]]
		exclude = [x[0] for x in data if not x[2]]

		# Update kept templates
		self.rectangles = [x[0] for x in keep]
		self.ids = [x[1] for x in keep]
		self.ids_map = {i: n for n, i in enumerate(self.ids)}

		# Track excluded templates
		self.exclusions['trimming'].extend(exclude)

		if verbose:
			print("%d templates after" % len(self.rectangles))

	def hardcut(self, verbose=True):
		if verbose:
			print("%d templates before" % len(self.rectangles))
		data = [(r, ID) for r, ID in zip(self.rectangles, self.ids) if self.inside(r, check_vertices=False)]
		self.rectangles = [x[0] for x in data]
		self.ids = [x[1] for x in data]
		self.ids_map = {i: n for n, i in enumerate(self.ids)}
		if verbose:
			print("%d templates after" % len(self.rectangles))

	def neighborhood_overlaps(self, ix, n=100000):
		if self.pns is None:
			raise ValueError("indexing into this bank without calling sort() first will lead to nonsense. This is probably wrong")
		if ix < n / 2:
			ixL = 0
		else:
			ixL = ix - n // 2
		ixR = ixL + n
		if ixR > len(self.rectangles):
			ixR = len(self.rectangles)
		return self.rectangles[ix].match(self.centers[ixL:ixR]), numpy.arange(ixL, ixR)

	def adjacent(self, c, n=1000, use_metric=False, ixs_only=False):
		if use_metric:
			center = self.cfunc(c, inv=True)
			g = scale_g(self.metric(center), 1e-3)  # FIXME not sure if this is a good idea or not
			matches = self.metric.metric_match(g, center, self.centers)
			ixs = numpy.argsort(matches)
			ixs = ixs[-n:]
			if ixs_only:
				return ixs
			else:
				return [self.rectangles[ix] for ix in ixs]
		else:  # use whatever is in self.pns to sort
			assert (self.pns is not None)
			pn = _pn_phase(c)
			ix = bisect.bisect(self.pns, pn)
			if ix < n / 2:
				ixL = 0
			else:
				ixL = ix - n // 2
			ixR = ixL + n
			if ixs_only:
				return numpy.array(range(ixL, ixR))
			else:
				this_rs = self.rectangles[ixL:ixR]
				return this_rs

	def nearest(self, c, n=30000):
		# TODO expose 'n' param to yaml
		# TODO expose 'use_metric' to yaml
		this_rs = self.adjacent(c, n=n)
		cp = self.cfunc(c, inv=True)

		def _fft_match(x, cp=cp):
			return x[-1].metric.fft_match_at_coords(cp, x[-1].center)

		return max(map(_fft_match, enumerate(this_rs)))

	def prob_of_tk_given_rho(self, p_of_tj_given_alpha, num_rhos=30, n=2000000, start_ix=None, stop_ix=None, verbose=True):
		if start_ix is None:
			start_ix = 0
		if stop_ix is None:
			stop_ix = len(self)
		rhos = numpy.logspace(0.5, 1.5, num_rhos)
		log_p_of_tk_given_alpha_rho = numpy.zeros((len(rhos), len(p_of_tj_given_alpha)))
		# We do this so that someone doesn't mistake an unprocessed template for valid since the user can specify an index range.
		log_p_of_tk_given_alpha_rho.fill(numpy.nan)
		onebyroot2pi = 1. / numpy.sqrt(numpy.pi * 2.0)
		log_root_pi_2 = numpy.log(onebyroot2pi)
		log_p_of_tj_given_alpha = numpy.log(p_of_tj_given_alpha)

		for ix in range(len(self)):
			if ix < start_ix or ix >= stop_ix:
				continue
			kdj, indices = self.neighborhood_overlaps(ix, n=n)
			for jx, rho in enumerate(rhos):
				log_p = logsumexp(log_p_of_tj_given_alpha[indices] + log_root_pi_2 - rho ** 2 / 2. * (1.0 - 2. * kdj + kdj ** 2))
				assert log_p < 0, log_p
				log_p_of_tk_given_alpha_rho[jx, ix] = log_p
			if verbose and not ix % 100:
				print("processing row %d" % (ix,))
		return rhos, log_p_of_tk_given_alpha_rho

	def mass_model_coefficients(self, rhos, log_p_of_tk_given_alpha_rho):
		# normalize the result
		for jx, rho in enumerate(rhos):
			# work around that nans are valid for incomplete mass models, but still return a sensible normalization...
			norm_array = log_p_of_tk_given_alpha_rho[jx, :]
			norm_array = norm_array[~numpy.isnan(norm_array)]
			if len(norm_array) >= 1:
				norm = logsumexp(norm_array)
				if numpy.isfinite(norm):
					log_p_of_tk_given_alpha_rho[jx, :] -= norm

		# and create the piecewise polynomial fit TO THE NATURAL LOG
		coefficients = numpy.zeros((4, len(rhos) + 3, len(self)), dtype=float)
		breakpoints = numpy.zeros(len(rhos) + 4, dtype=float)
		# FIXME YUCK. work around not being able to interpolate over negative inf without getting nans
		log_p_copy = log_p_of_tk_given_alpha_rho.copy()
		minfinite = (log_p_copy[numpy.isfinite(log_p_copy)]).min()
		# We assume negative infinity maps to a probability 1,000,000 times lower than the smallest finite value
		log_p_copy[~numpy.isfinite(log_p_copy)] = minfinite - numpy.log(1e6)
		for tix in range(log_p_copy.shape[1]):
			tck = splrep(rhos, log_p_copy[:, tix])
			p = PPoly.from_spline(tck)
			coefficients[:, :, tix] = p.c
			if tix == 0:
				breakpoints[:] = p.x

		return coefficients, breakpoints

	@staticmethod
	def m1_constraint(constraints, c):
		return (c[M1] < constraints['m1'][0]) or (c[M1] > constraints['m1'][1])

	@staticmethod
	def m2_constraint(constraints, c):
		return (c[M2] < constraints['m2'][0]) or (c[M2] > constraints['m2'][1])

	@staticmethod
	def M_constraint(constraints, c):
		M = c[M1] + c[M2]
		return (M < constraints['M'][0]) or (M > constraints['M'][1])

	@staticmethod
	def mc_constraint(constraints, c):
		mc = chirpmass(c[M1], c[M2])
		return (mc < constraints['mc'][0]) or (mc > constraints['mc'][1])

	@staticmethod
	def q_constraint(constraints, c):
		q = c[M1] / c[M2]
		return (q < constraints['q'][0]) or (q > constraints['q'][1])

	@staticmethod
	def chi_constraint(constraints, c):
		m1, m2 = c[M1], c[M2]
		chi = (c[M1] * c[S1Z] + c[M2] * c[S2Z]) / (c[M1] + c[M2])
		min_sz, max_sz = constraints['chi']
		min_ns_mass, max_ns_mass = constraints['ns-mass']
		min_ns_s1z, max_ns_s1z = constraints['ns-s1z']
		# if both m1 and m2 are NS, then chi must be the same interval as the ns spin
		if (min_ns_mass <= m1 <= max_ns_mass) and (min_ns_mass <= m2 <= max_ns_mass):
			return (chi < min_ns_s1z) or (chi > max_ns_s1z)
		if (min_ns_mass <= m1 <= max_ns_mass) and not (min_ns_mass <= m2 <= max_ns_mass):
			min_chi = (min_ns_s1z * m1 + min_sz * m2) / (m1 + m2)
			max_chi = (max_ns_s1z * m1 + max_sz * m2) / (m1 + m2)
			return (chi < min_chi) or (chi > max_chi)
		if not (min_ns_mass <= m1 <= max_ns_mass) and (min_ns_mass <= m2 <= max_ns_mass):
			min_chi = (min_ns_s1z * m2 + min_sz * m1) / (m1 + m2)
			max_chi = (max_ns_s1z * m2 + max_sz * m1) / (m1 + m2)
			return (chi < min_chi) or (chi > max_chi)
		# otherwise, use the specified chi range. Note that we probably overcover NSBH somewhat but it is a hard curved boundary to tile properly.
		if chi < constraints['chi'][0] or chi > constraints['chi'][1]:
			return True
		return False

	def constraint(self, c):
		return any(f(self.constraints(), c) for f in [Bank.m1_constraint, Bank.m2_constraint, Bank.M_constraint, Bank.mc_constraint, Bank.q_constraint, Bank.chi_constraint])

	@staticmethod
	def fromChart(chart, constraint_sets: Dict[str, Dict[str, Tuple[float, float]]] = None, default_constraint_set_name: str = None):
		return Bank(rectangles=tuple(chart.atlas()),
					constraint_sets=constraint_sets,
					default_constraint_set_name=default_constraint_set_name,
					exclusions_branched=None if chart.excluded is None else chart.excluded)

	@classmethod
	def load(cls, h5fn):
		# @shio figure out how to parse the file back in and make one of these Bank classes
		d = io.h5read(h5fn)
		rectangles = cls._deserialize_rectangles(d, prefix='')
		excluded_branching = cls._deserialize_rectangles(d, prefix='exclusion_branching_')
		excluded_trimming = cls._deserialize_rectangles(d, prefix='exclusion_trimming_')

		# Legacy case for constraints, only a single set specified with stale key "constraints", convert to new format key "constraint_sets"
		if 'constraints' in d:
			d['constraint_sets'] = {'default': d['constraints']}
			d['default_constraint_set_name'] = 'default'
			del d['constraints']

		# Sentinel value for serialization (H5py needs a non-None value, so we humor it)
		if d['default_constraint_set_name'] == '__load__':
			d['default_constraint_set_name'] = None

		return cls(rectangles, constraint_sets=d['constraint_sets'], pns=None if 'pns' not in d else d['pns'], ids=None if 'ids' not in d else d['ids'], process=d['__process'],
				   default_constraint_set_name=d['default_constraint_set_name'], exclusions_branched=excluded_branching, exclusions_trimmed=excluded_trimming)

	@staticmethod
	def _serialize_rectangles(rectangles: List[ManifoldRectangle], prefix: str = ''):
		out = {
			prefix + "metric": {} if len(rectangles) == 0 else rectangles[0].metric._to_dict(),
			prefix + "mins": numpy.array([r.mins for r in rectangles]),
			prefix + "maxes": numpy.array([r.maxes for r in rectangles]),
			prefix + "gs": numpy.array([r.g for r in rectangles]),
			prefix + "rights": numpy.array([r.right for r in rectangles]),
			prefix + "lefts": numpy.array([r.left for r in rectangles]),
		}

		# Def serialize Rectangle metadata
		meta_keys = []
		for r in rectangles:
			meta_keys.extend(list(r.meta.keys()))
		meta_keys = list(set(meta_keys))

		if meta_keys:
			out[prefix + 'meta'] = {}

			for k in meta_keys:
				values = [r.meta.get(k, None) for r in rectangles]

				# Infer array type (either int or float)
				vtypes = set([type(v) for v in values])
				if float in vtypes or numpy.float64 in vtypes:
					dtype = 'float64'
				elif int in vtypes or numpy.int64 in vtypes:
					dtype = 'int64'
					values = [v if v is not None else io.INT64_NAN for v in values]
				elif vtypes == {type(None)}:
					dtype = 'float64'
					print(f'Warning: meta data for key {k} is all None, defaulting to float64')
				else:
					raise ValueError(f"Unsupported type in meta data for key {k}, types: {vtypes}")

				out[prefix + 'meta'][k] = numpy.array(values).astype(dtype)

		return out

	@staticmethod
	def _deserialize_rectangles(d, prefix: str = ''):
		rectangles = []

		# Check that prefix is in the keys at all, if not return empty list
		if not any(k.startswith(prefix) for k in d.keys()):
			return rectangles

		r_metric = None if d[prefix + 'metric'] == {} else CBCMetric._from_dict(d[prefix + 'metric'])

		# Check for rectangle meta data
		if prefix + 'meta' in d:
			meta = d[prefix + 'meta']
			keys = list(sorted(meta.keys()))

			# Invert integer sentinel values to None
			valss = []
			for k in keys:
				vals = meta[k]
				if vals.dtype == numpy.int64:
					vals = vals.astype('O')
					vals = numpy.where(vals == io.INT64_NAN, None, vals)

				# Deserialize all None
				if vals.dtype == 'float64' and numpy.all(numpy.isnan(vals)):
					vals = [None for _ in vals]
				valss.append(vals)

			valss = list(zip(*valss))
			meta_dicts = [dict(zip(keys, vals)) for vals in valss]
		else:
			meta_dicts = [{} for _ in d['mins']]

		for mn, mx, g, right, left, meta in zip(d[prefix + 'mins'], d[prefix + 'maxes'], d[prefix + 'gs'], d[prefix + 'rights'], d[prefix + 'lefts'], meta_dicts):
			rectangles.append(ManifoldRectangle(mn, mx, r_metric, g, right=right, left=left, meta=meta))

		return rectangles

	def save(self, h5fn):
		# FIXME @shio please find a way to serialize the data in a metric so that it can be recorded to hdf5
		# FIXME completely untested

		out = {
			"constraint_sets": self.constraint_sets,
			"default_constraint_set_name": self.default_constraint_set_name or '__none__',
			"ids": self.ids,
		}

		# Serialize rectangles
		out.update(self._serialize_rectangles(self.rectangles))

		# Serialize Excluded Rectangles
		if self.exclusions['branching']:
			out.update(self._serialize_rectangles(self.exclusions['branching'], prefix='exclusion_branching_'))
		if self.exclusions['trimming']:
			out.update(self._serialize_rectangles(self.exclusions['trimming'], prefix='exclusion_trimming_'))

		if self.__process is not None:
			out['__process'] = self.__process
		if self.pns is not None:
			out['pns'] = self.pns
		io.h5write(h5fn, out)

	def to_ligolw(self, xmlfn, program_name=None, optdict={}):
		# prepare a new XML document for writing template bank
		xmldoc = ligolw.Document()
		xmldoc.appendChild(ligolw.LIGO_LW())
		sngl_inspiral_columns = (
			"process:process_id", "ifo", "search", "channel", "end_time", "end_time_ns", "end_time_gmst", "impulse_time", "impulse_time_ns", "template_duration", "event_duration", "amplitude", "eff_distance", "coa_phase", "mass1", "mass2", "mchirp",
			"mtotal", "eta", "kappa", "chi", "tau0", "tau2", "tau3", "tau4", "tau5", "ttotal", "psi0", "psi3", "alpha", "alpha1", "alpha2", "alpha3", "alpha4", "alpha5", "alpha6", "beta", "f_final", "snr", "chisq", "chisq_dof", "bank_chisq",
			"bank_chisq_dof", "cont_chisq", "cont_chisq_dof", "sigmasq", "rsqveto_duration", "Gamma0", "Gamma1", "Gamma2", "Gamma3", "Gamma4", "Gamma5", "Gamma6", "Gamma7", "Gamma8", "Gamma9", "spin1x", "spin1y", "spin1z", "spin2x", "spin2y",
			"spin2z",
			"event_id")
		tbl = lsctables.New(lsctables.SnglInspiralTable, columns=sngl_inspiral_columns)
		xmldoc.childNodes[-1].appendChild(tbl)
		# FIXME make a real process table
		process = ligolw_process.register_to_xmldoc(xmldoc, program_name or sys.argv[0], optdict)
		process.set_end_time_now()

		for n, c in zip(self.ids, self.rectangles):
			center_dict = self.cfunc(numpy.array(c.center))
			row = c.sngl_row
			row.f_final = 1024  # FIXME!!!
			row.mchirp = chirpmass(center_dict[M1], center_dict[M2])
			row.process_id = process.process_id
			row.event_id = n
			row.template_id = n
			tbl.append(row)

		# Write out the bank file
		utils.write_filename(xmldoc, xmlfn)

	def find_rectangle_constraint_interior(self, r: ManifoldRectangle):
		"""Find a point of a given rectangle that is in the interior of the bank constraint manifold. If not possible,
		return None.

		Returns:
			ManifoldRectangle, the rectangle with the interior point
		"""
		p = r.center
		# Check center first
		if not self.constraint(self.cfunc(p)):  # Recall: constraint returns True if point fails constraints, so we want "False" to indicate interior
			return p

		# Check vertices returning the first one that passes constraints
		for p in r.vertices:
			if not self.constraint(self.cfunc(numpy.array(p))):
				return p

	def ellipse_boundary_ratio(self, r: ManifoldRectangle, target_mismatch: float = 0.03, n_points: int = 100, object_symmetric: bool = False, scale_method: str = None, scale_pow: float = 5.0) -> float:
		"""Compute a measure of how 'near' a given rectangle's center is to the
		boundary of the constraint manifold (nearness in terms of match). This
		is measured by using a constant-mismatch ellipse centered at the center of
		the rectangle, randomly sampling points within the ellipse, and comparing
		a ratio of points that passed constraints to those that dont

		Args:
			r:
				ManifoldRectangle, the rectangle to compute the boundary ratio for
			target_mismatch:
				float, default 0.03, the target mismatch for the ellipse
			n_points:
				int, default 100, the number of points to sample in the ellipse
			object_symmetric:
				bool, default False, if True, will also check points with m1 and m2 swapped
			scale_method:
				str, default None, if specified, will scale the target mismatch for local behavior. Currently supported values are 'pow-log-volume'
			scale_pow:
				float, default 5.0, the power to raise the scale factor to. Only used if scale_method is 'pow-log-volume'

		Returns:
			float, the ratio of (points in ellipse failing constraints) / (total sampled points)
		"""
		# Determine interior
		p_interior = self.find_rectangle_constraint_interior(r)
		if p_interior is None:
			return 1  # TODO check remaining cases of this
		# p_interior = r.center

		# Optionally scale the target mismatch for local behavior
		if scale_method is not None:
			if scale_method == 'pow-log-volume':
				if 'volume_element' not in self.rectangle_meta:
					print('Warning: log-volume scale requested but no volume_element meta data found. Computing min and max volume elements in bank.')
					volumes = [cover.volume_element(r.g) for r in self.rectangles]
					self.rectangle_meta['volume_element'] = {'min': min(volumes), 'max': max(volumes)}

				# Get volume element bounds
				v_min, v_max = self.rectangle_meta['volume_element']['min'], self.rectangle_meta['volume_element']['max']
				scale_factor = numpy.log10(cover.volume_element(r.g)) / numpy.log10(v_min)
				target_mismatch = target_mismatch * scale_factor ** scale_pow  # FIXME hard-coded power of 5
			else:
				raise ValueError(f"Invalid scale value {scale_method}")

		# TODO vectorize this loop
		pts = [jump.random_ellipse_point_from_center(center=p_interior, ell_mat=r.g, scale=target_mismatch) for i in range(n_points)]

		# Note, currently the 'constraint' method return True if a point fails (DOES NOT pass) constraints
		# Include points with m1 and m2 swapped to increase chance of passing constraints
		pt_scores = []
		for p in pts:
			p_dict = self.cfunc(p)
			score = self.constraint(p_dict)

			if object_symmetric:
				p_dict_alt = p_dict.copy()
				p_dict_alt[M1], p_dict_alt[M2] = p_dict[M2], p_dict[M1]
				p_dict_alt[S1Z], p_dict_alt[S2Z] = p_dict[S2Z], p_dict[S1Z]
				score = score and self.constraint(p_dict_alt)

			pt_scores.append(score)

		# Coerce to int
		pt_scores = [int(p) for p in pt_scores]
		return sum(pt_scores) / n_points

	def fine_grain_boundary(self, threshold: float = 0.5, target_mismatch: float = 0.03, n_points: int = 100, max_splits: int = 3, object_symmetric: bool = False, sharpness: float = 1.0, aspect_ratios = None,
							size_method = SizeMethod.Proper, global_factor: float = None, local_dist_bounds: Tuple[float, float] = None, local_dist_beta: Tuple[float, float] = None,
							ebr_scale_method: str = None, ebr_scale_pow: float = 5.0):
		"""Apply a fine-grain technique to increase resolution on the boundary as defined
		by the constraints
		"""
		print(f'Fine-graining bank with threshold {threshold}, target mismatch {target_mismatch}, n_points {n_points}, max_splits {max_splits}, object_symmetric {object_symmetric}')
		coarse_grains = []
		for r in self.rectangles:
			ebr = self.ellipse_boundary_ratio(r, target_mismatch, n_points, object_symmetric=object_symmetric, scale_method=ebr_scale_method, scale_pow=ebr_scale_pow)
			r.update_meta(ebr=ebr)

			if ebr > threshold:
				coarse_grains.append((r, ebr))

		# Separate iteration to avoid modifying the list while iterating
		for r, ebr in coarse_grains:

			# split the rectangle according to the EBR
			split_score = (ebr / (sharpness * (1 - ebr))) if ebr < 1.0 else 2 ** max_splits
			n_splits = min(max(0, int(numpy.log2(split_score))), max_splits)

			r_children = [r]
			for i in range(n_splits):
				new_r_children = []
				for r in r_children:
					right, left = r.divide(reuse_g=True, size_method=size_method, aspect_ratios = aspect_ratios, global_factor=global_factor, local_dist_bounds=local_dist_bounds, local_dist_beta=local_dist_beta)
					right.update_meta(ebr=ebr, fine_grained=1.0, n_splits=n_splits)
					left.update_meta(ebr=ebr, fine_grained=1.0, n_splits=n_splits)
					new_r_children.append(right)
					new_r_children.append(left)
				r_children = new_r_children

			# Update the bank with the new rectangles
			if r in self.rectangles:
				self.rectangles.remove(r)
			for c in r_children:
				if c not in self.rectangles:
					self.rectangles.append(c)

		# Reassign ids after tinkering with rectangles list
		self.assign_ids(reassign=True)

	def copy(self, deep: bool = False):
		"""Make a copy of the Bank object.

		Args:
			deep:
				bool, default False, if True, a deep copy of the object is made. If False, a shallow copy is made.

		Returns:
			Bank, a copy of the Bank object.
		"""
		if deep:
			raise NotImplementedError("Deep copy of banks not implemented yet, shallow copy available.")
		return Bank(rectangles=self.rectangles,
					constraint_sets=self.constraint_sets,
					default_constraint_set_name=self.default_constraint_set_name,
					ids=self.ids,
					pns=self.pns,
					process=self.__process)

	def merge(self, *others: Tuple['Bank'], new_constraints: Dict[str, Tuple[float, float]] = None) -> 'Bank':
		"""Merge two or more Bank objects, preserving this banks template ids.

		Args:
			others:
				Tuple[Bank], the other Bank object to merge with.
			new_constraints:
				Dict[str, Callable], default None, if specified, the new constraints to use for the merged Bank object. Note, if
				new_constraints is not specified, then the constraints of all banks must be the same, otherwise a ValueError is raised.

		Returns:
			Bank, the merged Bank object.
		"""
		# Reconcile constraints using union of domains
		constraints = new_constraints
		if constraints is None:
			constraints = {}
			for bank in [self] + list(others):
				for k, v in bank.constraints().items():
					if k in constraints:
						constraints[k] = [min(constraints[k][0], v[0]), max(constraints[k][1], v[1])]
					else:
						constraints[k] = v

		# Preserve the ids of this bank
		reserved_ids = self.ids

		# Reassign the ids of the other banks, avoiding collisions with the ids of this bank, gathering cumulative attributes
		all_rectangles = list(self.rectangles)
		all_ids = list(self.ids)
		all_pns = list(self.pns)

		# Track exclusions
		exclusions = self.exclusions

		for other in others:
			other_cpy = other.copy()
			other_cpy.assign_ids(reserved_ids=reserved_ids, reassign=True)

			# TODO should we ensure that rectangles are unique?
			all_rectangles = all_rectangles + list(other_cpy.rectangles)
			all_ids = all_ids + list(other_cpy.ids)
			all_pns = all_pns + list(other_cpy.pns)

			# Track exclusions
			exclusions['branching'].extend(other_cpy.exclusions['branching'])
			exclusions['trimming'].extend(other_cpy.exclusions['trimming'])

		# Merge the banks
		bank = Bank(rectangles=all_rectangles,
					constraint_sets={'default': constraints},
					default_constraint_set_name='default',
					ids=all_ids,
					pns=all_pns,
					process=self.__process,
					exclusions_branched=exclusions['branching'],
					exclusions_trimmed=exclusions['trimming'])
		bank.__sort()
		return bank


# FIXME are these functions below used??

def metric_m1_m2(min_mass, max_mass, psd_path: pathlib.Path = data.TREEBANK_PSD, detector: str = 'H1', verbose: bool = False,
				 freq_low: int = 15, freq_high: int = 512):
	g = CBCMetric(data.read_psd(psd_path, detector, verbose=verbose),
				  M1M2(**common.bounds_from_lists(['m1', 'm2'], [min_mass, min_mass], [max_mass, max_mass])),
				  freq_low, freq_high, approximant="IMRPhenomD")
	return g


def bank_m1_m2(min_mass, max_mass, psd_path: pathlib.Path = data.TREEBANK_PSD, detector: str = 'H1', verbose: bool = False,
			   freq_low: int = 15, freq_high: int = 512, reuse_g_mm: float = 0.0):
	g = metric_m1_m2(min_mass, max_mass, psd_path, detector, verbose, freq_low, freq_high)
	R = CBCManifoldRectangle(g.coord_func.mins, g.coord_func.maxes, g)
	Bank = cover.Chart(R)

	# FIXME this constraint function only makes sense if the first two coordinates are m1, m2
	def constraint(o):
		return not all(v[0] < v[1] for v in o.data.vertices)

	Bank.branch(mm=10.0,
				reuse_g_mm=reuse_g_mm,
				constraint=constraint,
				verbose=False)

	return Bank


def scale_g(g, scale=None):
	if scale is not None:
		scale = max(min(0.2, scale), 1e-4)
		V, Q = numpy.linalg.eig(g)
		V[V < max(V) * scale] = max(V) * scale
		g = numpy.dot(Q, numpy.dot(numpy.diag(abs(V)), numpy.linalg.inv(Q)))
	return g
