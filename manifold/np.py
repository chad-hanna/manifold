import lal
import numpy
from lal import LIGOTimeGPS
from ligo import segments

# Cross version compatibility for numpy types
try:
	numpy_float = numpy.float
except AttributeError:
	numpy_float = numpy.float64

#
# Specially defined numpy array types
#

# Stores a LIGOTimeGPS
gpstype = numpy.dtype([('s', numpy.int32), ('ns', numpy.int32)])

# Stores a segment list of LIGOTimeGPS
gpsseglisttype = numpy.dtype([('start', gpstype), ('end', gpstype)])


# Stores and arbitrary length REAL8FrequencySeries
def r8ftype(N):
	return numpy.dtype([('__type__', 'S8'),
						('name', 'S256'),
						('epoch', gpstype),
						('f0', 'f8'),
						('deltaF', 'f8'),
						('sampleUnits', 'S256'),
						('length', numpy.int64),
						('data', 'f8', N)])


# Stores a mass event (without injections)
event_type = numpy.dtype([
	('id', 'S24'),
	('trig_start', gpstype),
	('trig_stop', gpstype),
	('data_epoch', gpstype),
	('data_length', numpy.int32),
	('run_time', numpy.float32),
	('m1', numpy_float),
	('m2', numpy_float),
	('S1z', numpy_float),
	('S2z', numpy_float),
	('snr', numpy.float32),
	('L', numpy.float32),
	('far', numpy.float32),
	('fmerg', numpy.float32),
	('H1_snr', numpy.float32),
	('L1_snr', numpy.float32),
	('V1_snr', numpy.float32),
	('K1_snr', numpy.float32),
	('H1_chisq', numpy.float32),
	('L1_chisq', numpy.float32),
	('V1_chisq', numpy.float32),
	('K1_chisq', numpy.float32),
	('H1_chisq_dof', numpy.float32),
	('L1_chisq_dof', numpy.float32),
	('V1_chisq_dof', numpy.float32),
	('K1_chisq_dof', numpy.float32),
	('H1_perp_snr', numpy.float32),
	('L1_perp_snr', numpy.float32),
	('V1_perp_snr', numpy.float32),
	('K1_perp_snr', numpy.float32),
	('H1_L', numpy.float32),
	('L1_L', numpy.float32),
	('V1_L', numpy.float32),
	('K1_L', numpy.float32),
	('H1_fstd', numpy.float32),
	('L1_fstd', numpy.float32),
	('V1_fstd', numpy.float32),
	('K1_fstd', numpy.float32),
	('H1_time', numpy.float64),
	('L1_time', numpy.float64),
	('V1_time', numpy.float64),
	('K1_time', numpy.float64),
	('H1_phase', numpy.float32),
	('L1_phase', numpy.float32),
	('V1_phase', numpy.float32),
	('K1_phase', numpy.float32)
])

# Stores a mass sim_inspiral row
# FIXME, we probably want to bring in other parameters here
sim_type = numpy.dtype([
	("time_geocent", gpstype),
	("h_end_time", gpstype),
	("l_end_time", gpstype),
	("v_end_time", gpstype),
	("k_end_time", gpstype),
	("mass1", numpy_float),
	("mass2", numpy_float),
	("spin1x", numpy_float),
	("spin1y", numpy_float),
	("spin1z", numpy_float),
	("spin2x", numpy_float),
	("spin2y", numpy_float),
	("spin2z", numpy_float),
	("distance", numpy_float),
	("inclination", numpy_float),
	("coa_phase", numpy_float),
	("H1_snr", numpy.float32),
	("L1_snr", numpy.float32),
	("V1_snr", numpy.float32),
	("K1_snr", numpy.float32),
	("network_snr", numpy.float32)
])

# Stores a mass event that is the result of an injection
injection_event_type = numpy.dtype([("event", event_type), ("sim", sim_type)])


#
# Numpy array encoding
#


def npencode(o):
	if isinstance(o, dict):
		return dict2np(o)
	elif isinstance(o, segments.segmentlist):
		return seglist2np(o)
	elif isinstance(o, LIGOTimeGPS):
		return gps2np(o)
	elif isinstance(o, lal.REAL8FrequencySeries):
		return r8f2np(o)
	elif isinstance(o, str):
		return numpy.array(o, 'S')
	else:
		# NOTE numpy will encode a lot of things, it doesn't gaurantee h5py can write them to disk
		try:
			return numpy.array(o)
		except numpy.VisibleDeprecationWarning:
			print(o)


def dict2np(d):
	dtype = []
	arr = []
	for k, v in d.items():
		_arr = npencode(v)
		dtype.append((k, _arr.dtype, _arr.shape))
		arr.append(_arr)
	dtype = numpy.dtype(dtype)
	return numpy.array(tuple(arr), dtype=dtype)


def seglist2np(seglist):
	return numpy.array([(numpy.array((s[0].gpsSeconds, s[0].gpsNanoSeconds), dtype=gpstype), numpy.array((s[1].gpsSeconds, s[1].gpsNanoSeconds), dtype=gpstype)) for s in seglist], gpsseglisttype)


def gps2np(gps):
	return numpy.array([(gps.gpsSeconds, gps.gpsNanoSeconds)], dtype=gpstype)


def r8f2np(o):
	return numpy.array(('r8f', o.name, gps2np(o.epoch), o.f0, o.deltaF, o.sampleUnits, o.data.length, tuple(o.data.data)), dtype=r8ftype(o.data.length))


#
# Numpy array decoding
#


def npdecode(x):
    # NOTE THE ORDER MATTERS!
    # you need to check the generic dtype.names and shape == () at the end or stuff will break
    if x.dtype == gpsseglisttype:
        return np2seglist(x)
    if x.dtype == gpstype:
        return np2gps(x)
    if x.dtype.names is not None and "__type__" in x.dtype.names and x['__type__'].item().decode('utf-8') == "r8f":
        return np2r8f(x)
    if x.dtype in (event_type, sim_type, injection_event_type):
        return x
    if x.dtype.names is not None:
        return {k: npdecode(x[k]) for k in x.dtype.names}
    if x.dtype.char == "S":
        return x.item().decode('utf-8')
    if x.shape == ():
        return x.item()
    else:
        return x



def np2seglist(a):
    return segments.segmentlist([(np2gps(x['start']), np2gps(x['end'])) for x in a])



def np2gps(a):
    return LIGOTimeGPS(int(a['s']), int(a['ns']))



def np2r8f(o):
	out = lal.CreateREAL8FrequencySeries(
		name=str(o["name"]),
		epoch=np2gps(o["epoch"]),
		f0=float(o["f0"]),
		deltaF=float(o["deltaF"]),
		sampleUnits=lal.Unit(o["sampleUnits"].item().decode('utf-8')),
		length=int(o["length"].item())
	)
	out.data.data[:] = o["data"][:]
	return out
