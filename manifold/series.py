# Copyright (C) 2020 Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import numpy
import lal
from scipy.signal import resample
from lal import LIGOTimeGPS

def r8t(data, epoch, deltaT, f0 = 0.0, name = "", sampleUnits = lal.Unit("strain")):
	if isinstance(data, int):
		length = data
	else:
		length = len(data)
	x = lal.CreateREAL8TimeSeries(
		name = name,
		epoch = epoch,
		f0 = f0,
		deltaT = deltaT,
		sampleUnits = sampleUnits,
		length = length
	)
	if not isinstance(data, int):
		x.data.data[:] = data
	else:
		x.data.data[:] = 0.
	return x

def c8t(data, epoch, deltaT, f0 = 0.0, name = "", sampleUnits = lal.Unit("strain")):
	if isinstance(data, int):
		length = data
	else:
		length = len(data)
	x = lal.CreateCOMPLEX8TimeSeries(
		name = name,
		epoch = epoch,
		f0 = f0,
		deltaT = deltaT,
		sampleUnits = sampleUnits,
		length = length
	)
	if not isinstance(data, int):
		x.data.data[:] = data
	else:
		x.data.data[:] = 0.
	return x

def r4t(data, epoch, deltaT, f0 = 0.0, name = "", sampleUnits = lal.Unit("strain")):
	if isinstance(data, int):
		length = data
	else:
		length = len(data)
	x = lal.CreateREAL4TimeSeries(
		name = name,
		epoch = epoch,
		f0 = f0,
		deltaT = deltaT,
		sampleUnits = sampleUnits,
		length = length
	)
	if not isinstance(data, int):
		x.data.data[:] = data
	else:
		x.data.data[:] = 0.
	return x

def cpr4t(o):
	return r4t(o.data.data, o.epoch, o.deltaT, o.f0, o.name, o.sampleUnits)

def c8f(data, epoch, deltaF, f0 = 0.0, name = "", sampleUnits = lal.Unit("strain")):
	if isinstance(data, int):
		length = data
	else:
		length = len(data)

	x = lal.CreateCOMPLEX8FrequencySeries(
		name = name,
		epoch = epoch,
		f0 = f0,
		deltaF = deltaF,
		sampleUnits = sampleUnits,
		length = length
	)

	if not isinstance(data, int):
		x.data.data[:] = data
	else:
		x.data.data[:] = 0.0
	return x

def c16f(data, epoch, deltaF, f0 = 0.0, name = "", sampleUnits = lal.Unit("strain")):
	if isinstance(data, int):
		length = data
	else:
		length = len(data)

	x = lal.CreateCOMPLEX16FrequencySeries(
		name = name,
		epoch = epoch,
		f0 = f0,
		deltaF = deltaF,
		sampleUnits = sampleUnits,
		length = length
	)

	if not isinstance(data, int):
		x.data.data[:] = data
	else:
		x.data.data[:] = 0.
	return x

def r8f(data, epoch, deltaF, f0 = 0.0, name = "", sampleUnits = lal.Unit("strain")):
	if isinstance(data, int):
		length = data
	else:
		length = len(data)

	x = lal.CreateREAL8FrequencySeries(
		name = name,
		epoch = epoch,
		f0 = f0,
		deltaF = deltaF,
		sampleUnits = sampleUnits,
		length = length
	)

	if not isinstance(data, int):
		x.data.data[:] = data
	else:
		x.data.data[:] = 0.
	return x

def cpc16f(o):
	return c16f(o.data.data, o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)

def cpr8f(o):
	return r8f(o.data.data, o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)

def truncater8f(o, fmax):
	ix = int(fmax / o.deltaF) + 1
	assert ix <= o.data.length
	x = r8f(o.data.data[:ix], o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)
	return x

# FIXME make this take freq?
def truncatec16f(o, length):
	assert length <= o.data.length
	x = c16f(o.data.data[:length], o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)
	return x

# FIXME make this take freq?
def expandc16f(o, length):
	assert length >= o.data.length
	x = c16f(length, o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)
	x.data.data[:o.data.length] = o.data.data[:]
	return x

def expandc8f(o, length):
	assert length >= o.data.length
	x = c8f(length, o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)
	x.data.data[:o.data.length] = o.data.data[:]
	return x

def truncatec8f(o, length):
	assert length <= o.data.length
	x = c8f(length, o.epoch, o.deltaF, o.f0, o.name, o.sampleUnits)
	x.data.data[:] = o.data.data[:length]
	return x

def nipr4t(o, duration):
	"""
	o is a real 4 time series
	duration is the last N seconds of the data to keep
	"""
	length = int(duration / o.deltaT)
	assert (length <= o.data.length)
	assert (length > 0)
	epoch = o.epoch + o.data.length * o.deltaT - duration
	x = r4t(o.data.data[-length:], epoch, o.deltaT, o.f0, o.name, o.sampleUnits)
	return x

#def rs2r4t(o, rate):
#	"""
#	resample real 4 time series that are a power of 2 duration and frequency to another power of 2 frequency
#	"""
#	in_rate = 1. / o.deltaT
#	if in_rate == rate:
#		return cpr4t(o) # short cut for no resampling
#	# make sure everything is a power of 2
#	assert (not (in_rate % 2))
#	assert (not (rate % 2))
#	assert (not (o.data.length % 2))
#	nlength = int(o.data.length * float(rate) / in_rate)
#	x = r4t(resample(o.data.data, nlength), o.epoch, 1. / rate, o.f0, o.name, o.sampleUnits)
#	return x

class rs2r4t(object):
	def __init__(self):
		self.FWD_PLAN = {}
		self.REV_PLAN = {}
	def __call__(self, o, rate):
		"""
		resample real 4 time series that are a power of 2 duration and frequency to another power of 2 frequency
		"""
		in_rate = 1. / o.deltaT
		if in_rate == rate:
			return cpr4t(o) # short cut for no resampling
		# make sure everything is a power of 2
		assert (not (in_rate % 2))
		assert (not (rate % 2))
		assert (not (o.data.length % 2))

		# forward FFT
		if o.data.length not in self.FWD_PLAN:
			self.FWD_PLAN[o.data.length] = lal.CreateForwardREAL4FFTPlan(o.data.length, 1)
		df = c8f(o.data.length // 2 + 1, o.epoch, 1.0 / (o.data.length * o.deltaT))
		lal.REAL4TimeFreqFFT(df, o, self.FWD_PLAN[o.data.length])

		# expand or truncate
		nlength = int(float(rate) / in_rate * o.data.length / 2 + 1)
		if rate > in_rate:
			df = expandc8f(df, nlength)
		else:
			df = truncatec8f(df, nlength)
			
		# inverse FFT
		length = 2 * (nlength - 1)
		if length not in self.REV_PLAN:
			self.REV_PLAN[length] = lal.CreateReverseREAL4FFTPlan(length, 1)
		d = r4t(length, o.epoch, 1. / rate)
		lal.REAL4FreqTimeFFT(d, df, self.REV_PLAN[length])

		return d

class rsarr(object):
	def __init__(self):
		self.FWD_PLAN = {}
		self.REV_PLAN = {}
	def __call__(self, o, length):
		"""
		resample numpy arrays that are a power of 2 duration and frequency to another power of 2 frequency
		"""
		if len(o) == length:
			return o.copy() # short cut for no resampling

		# forward FFT
		if len(o) not in self.FWD_PLAN:
			self.FWD_PLAN[len(o)] = lal.CreateForwardREAL4FFTPlan(len(o), 1)
		dt = r4t(o, LIGOTimeGPS(0), 1. / len(o))
		df = c8f(len(o) // 2 + 1, LIGOTimeGPS(0), 1.0)
		lal.REAL4TimeFreqFFT(df, dt, self.FWD_PLAN[len(o)])

		# expand or truncate
		nlength = int(length / 2 + 1)
		if length > len(o):
			df = expandc8f(df, nlength)
		else:
			df = truncatec8f(df, nlength)
			
		# inverse FFT
		if length not in self.REV_PLAN:
			self.REV_PLAN[length] = lal.CreateReverseREAL4FFTPlan(length, 1)
		d = r4t(length, LIGOTimeGPS(0), 1. / length)
		lal.REAL4FreqTimeFFT(d, df, self.REV_PLAN[length])

		return d.data.data[:]

class r4fft(object):
	def __init__(self):
		self.FWD_PLAN = {}

	def __call__(self, a):
		plan = self.FWD_PLAN.setdefault(len(a), lal.CreateForwardREAL4FFTPlan(len(a), 1))
		d = r4t(a, lal.LIGOTimeGPS(0), 1.0)
		df = c8f(len(a) // 2 + 1, lal.LIGOTimeGPS(0), 1.0 / len(a))
		lal.REAL4TimeFreqFFT(df, d, plan)
		if len(a) % 2:
			return numpy.concatenate((numpy.array(df.data.data, dtype=numpy.complex), numpy.conj(numpy.array(df.data.data[1:], dtype=numpy.complex))[::-1]))
		else:
			return numpy.concatenate((numpy.array(df.data.data, dtype=numpy.complex), numpy.conj(numpy.array(df.data.data[1:-1], dtype=numpy.complex))[::-1]))

class r4ifft(object):
	def __init__(self):
		self.REV_PLAN = {}

	def __call__(self, a):
		assert len(a) % 2
		length = 2 * (len(a)-1)
		plan = self.REV_PLAN.setdefault(length, lal.CreateReverseREAL4FFTPlan(length, 1))
		d = r4t(length, lal.LIGOTimeGPS(0), 1.0)
		df = c8f(a, lal.LIGOTimeGPS(0), 1.0 / length)

		lal.REAL4FreqTimeFFT(d, df, plan)
		return numpy.array(d.data.data, dtype=numpy.float32)
