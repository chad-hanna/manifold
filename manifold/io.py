# Copyright (C) 2020 Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import lal
from lal import LIGOTimeGPS
import os, json, h5py, numpy, sys, pwd, datetime, tempfile, shutil
from ligo import segments
from manifold.np import gpstype, gpsseglisttype, r8ftype, gps2np, seglist2np, r8f2np, npdecode, npencode


INT64_NAN = numpy.iinfo(numpy.int64).min


#
# Other random encoding and decoding of special objects
#

class JSONEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, LIGOTimeGPS):
			return "%d.%d" % (obj.gpsSeconds, obj.gpsNanoSeconds)
		return json.JSONEncoder.default(self, obj)

def gps2k(t):
	return "%d.%09d" % (t.gpsSeconds, t.gpsNanoSeconds)

def k2gps(k):
	s,ns = k.split(".")
	return LIGOTimeGPS(int(s), int(ns))

def dict2r8f(o):
	out = lal.CreateREAL8FrequencySeries(
		name = str(o["name"]),
		epoch = o["epoch"],
		f0 = float(o["f0"]),
		deltaF = float(o["deltaF"]),
		sampleUnits = lal.Unit((o["sampleUnits"]).decode('utf-8')),
		length = len(o["data"])
	)
	out.data.data[:] = o["data"][:]
	return out


#
# utilities to read and write h5 files
#


def record_env(env_vars = ("GI_TYPELIB_PATH", "GST_PLUGIN_PATH", "LAL_DATA_PATH", "LAL_PATH", "LD_LIBRARY_PATH", "LIBRARY_PATH", "PATH", "PKG_CONFIG_PATH", "PYTHONPATH")):
	info = {}
	info.update({k:v for k,v in os.environ.items() if k in env_vars})
	info.update({k: os.uname()[i] for (i,k) in enumerate(("sysname", "nodename", "release", "version", "machine"))})
	info.update({"cwd": os.getcwd()})
	info.update({"pid": os.getpid()})
	info.update({"user": pwd.getpwuid(os.geteuid())[0]})
	info.update({"cmd":" ".join(sys.argv)})
	return {str(datetime.datetime.now()): info}

def h5encode(f, obj, compression = None):
	for k in obj:
		try:
			if isinstance(obj[k], dict):
				# handle special case where dict is just used to annotate a sngl dataset
				# NOTE: assumes that what is in __data__ is already serializable
				if "__attrs__" in obj[k] and (set(obj[k]) - set(("__attrs__",)) == set(("__data__",))):
					d = f.create_dataset(str(k), data = obj[k]["__data__"])
					for k,v in obj[k]["__attrs__"].items():
						d.attrs[k] = npencode(v)
				# FIXME this could probably be supported fairly easily
				elif "__attrs__" in obj[k]:
					raise ValueError("Currently only attribute for data sets are supported and are given in the form {'__attrs__': <dict>, '__data__': <something directly encodable>}")
				else:
					g = f.create_group(str(k))
					h5encode(g, obj[k], compression = compression)
			elif isinstance(obj[k], segments.segmentlist):
				f.create_dataset(str(k), data=seglist2np(obj[k]))
			elif isinstance(obj[k], LIGOTimeGPS):
				f.create_dataset(str(k), data = gps2np(obj[k]))
			elif isinstance(obj[k], lal.REAL8FrequencySeries):
				f.create_dataset(str(k), data = r8f2np(obj[k]))
			elif isinstance(obj[k], numpy.ndarray):
				f.create_dataset(str(k), data = obj[k], compression = compression)
			else:
				f.create_dataset(str(k), data = obj[k])
		except Exception as e:
			raise ValueError(f'Could not write {k} of type {type(obj[k])} to h5 file: {e}. Object: {obj[k]}') from e

def h5write(fname, obj, compression = None):
	if '__process' in obj:
		obj['__process'].update(record_env())
	else:
		obj['__process'] = record_env()
	with h5py.File(fname, "w") as f:
		h5encode(f, obj, compression = compression)

def h5snapshot(fname, obj, compression = None):
	f, tmpfname = tempfile.mkstemp(dir=os.getenv("_CONDOR_SCRATCH_DIR", tempfile.gettempdir()))
	os.close(f)
	h5write(tmpfname, obj, compression = compression)
	shutil.move(tmpfname, fname)

def h5decode(f, ignore_groups = ()):
	if isinstance(f, h5py.Dataset):
		x = numpy.array(f)
		if f.attrs:
			return {"__attrs__": {k:npdecode(v) for k,v in f.attrs.items()}, "__data__": npdecode(x)}
		else:
			return npdecode(x)

	# otherwise we descend
	out = {}
	for k,v in tuple(f.items()):
		if k in ignore_groups: continue
		else:
			out[k] = h5decode(v, ignore_groups)
	return out

def h5read(fname, g = None, ignore_groups = ()):
	try:
		with h5py.File(fname, 'r') as f:
			if g is None:
				return h5decode(f, ignore_groups = ignore_groups)
			else:
				return h5decode(f[g], ignore_groups = ignore_groups)
	except OSError:
		return {}

def sim_inspiral_to_dict(sim, snr = None):
	# FIXME decide what we are doing with data formats. This is meant to
	# basically give the minimum parameters to match an injection to one in
	# a proper LIGOLW file. That should be taken as the source of truth
	out = {k: getattr(sim, k) for k in ("time_geocent", "mass1", "mass2", "spin1x", "spin1y", "spin1z", "spin2x", "spin2y", "spin2z", "distance", "inclination", "coa_phase")}
	if snr is not None:
		out['snr'] = snr
	# FIXME there is no k_end_time columns, we need to switch this to the ligolw methods
	for k in ('h_end_time', 'l_end_time', 'v_end_time'):#, 'k_end_time'):
		out[k] = LIGOTimeGPS(getattr(sim, k), getattr(sim, '%s_ns' % k))
	return out
