#!/usr/bin/env python
import enum
import itertools
import types
from typing import Dict, Any, Callable, Iterable, List, Tuple

import numpy
from matplotlib import patches
from scipy.spatial import Rectangle
from scipy.special import gamma
from scipy.stats import beta

from manifold.metric import volume_element, EvaluationMethod, JumpMethod


class SizeMethod(str, enum.Enum):
	"""Enumeration of methods for determining the size of a rectangle. Measure the sizes is primarily used for the purpose of splitting the rectangle into two smaller rectangles.
	The algorithm splits along the "longest" side of the rectangle, where the "longest" side is determined by the size method.

	Attributes:
		Coordinate: str, "coordinate", the size of the rectangle in each dimension, ignoring the metric
		Interpolated: str, "interpolated", the size of the rectangle in each dimension, interpolated between the coordinate and proper sizes
		Proper: str, "proper", the size of the rectangle in each dimension, as measured by the metric
	"""
	Coordinate = "coordinate"
	Interpolated = "interpolated"
	Proper = "proper"


class DummyRectangle:
	"""Dummy rectangle class for checking point-is-inside constraints"""

	def __init__(self, center):
		self.center = center


class ConstraintExteriorError(ValueError):
	"""Raised when a point is outside of a constraint submanifold"""


class ManifoldRectangle(Rectangle):
	def __init__(self, mins, maxes, metric, g=None, level=0, right=False, left=False, method: str = EvaluationMethod.Deterministic,
				 jump_method: str = JumpMethod.Truncation, jump_metric=None, jump_validator: types.FunctionType = None,
				 meta: Dict[str, Any] = None):
		Rectangle.__init__(self, mins, maxes)
		self.metric = metric
		self.method = method
		self.jump_method = jump_method
		self.jump_metric = jump_metric
		self.jump_validator = jump_validator
		self.meta = meta or {}
		# TODO add arguments for jump configuration
		# TODO rethink this architecture of embedding metric function arguments as attributes of ManifoldRectangle?
		# TODO rename "method" to "est_method" for clarity
		if g is None and metric is not None:

			# Determine proper 'center' point for metric evaluation. This point must satisfy the jump_validator
			# Note that we assume that the rectangle intersects the constraint submanifold at somepoint (e.g. jump_validator will be satisfied for at least one of {center} U {vertices})
			# TODO provide a more natural way of passing around constraints, as it is more general than just for jumping
			jump_validator = jump_validator or (lambda x: True)

			if jump_validator(self.center):
				metric_center = self.center
			else:
				metric_center = None
				for v in self.vertices:
					if jump_validator(v):
						metric_center = v
						break

			if metric_center is None:
				print(f'Rectangle does not intersect constraint submanifold: {self.mins}, {self.maxes}, skipping metric eval and using jump_metric {jump_metric}')
				if jump_metric is None:
					raise ConstraintExteriorError(f'Rectangle does not intersect constraint submanifold: {self.mins}, {self.maxes}, skipping metric eval and no jump_metric provided')
				self.g = jump_metric
			else:
				self.g = self.metric(metric_center, method=self.method, jump_method=self.jump_method, jump_metric=self.jump_metric, jump_validator=jump_validator)

		elif g is not None:
			self.g = g
		else:
			self.g = numpy.diag(numpy.ones(len(mins)))
		self.edges = numpy.diag(self.coordinate_size)
		self.level = level
		self.right = right
		self.left = left

	# FIXME this object is technically not immutable so it is a bad idea, so
	# be careful
	def __hash__(self):
		return hash(tuple(list(numpy.round(self.mins, 4)) + list(numpy.round(self.maxes, 4))))

	def __eq__(self, other):
		return numpy.array_equal(self.mins, other.mins) and numpy.array_equal(self.maxes, other.maxes)

	def ball_volume(self, r=0.17):
		# NOTE for templates r = mismatch**.5; So r=0.17 corresponds to
		# a mismatch of 2.9%
		N = len(self.mins)
		return numpy.pi * (N / 2) / gamma(N / 2 + 1) * r ** (N)

	def num_balls(self, r=0.17):
		# NOTE for templates r = mismatch**.5; So r=0.17 corresponds to
		# a mismatch of 2.9%
		# NOTE it is impossible to pack spheres this tightly. This
		# should be considered a lower bound
		return self.proper_volume / self.ball_volume(r=r)

	def update_meta(self, **kwargs):
		if self.meta is None:
			self.meta = {}
		self.meta.update(kwargs)

	@property
	def vertices(self):
		for v in itertools.product(*zip(self.mins, self.maxes)):
			yield v

	@property
	def center(self):
		return self.mins + self.coordinate_size / 2

	@property
	def metric_center(self):
		# NOTE you could override this method to provide a more natural notion of "center" for a given coordinate system
		return self.center

	def split(self, d, split, reuse_g=False):
		"""Produce two hyperrectangles by splitting.
		In general, if you need to compute maximum and minimum
		distances to the children, it can be done more efficiently
		by updating the maximum and minimum distances to the parent.
		Parameters
		----------
		d : int
			Axis to split hyperrectangle along.
		split : float
			Position along axis `d` to split at.
		"""
		mid = numpy.copy(self.maxes)
		mid[d] = split
		less = type(self)(self.mins, mid, self.metric, g=None if not reuse_g else self.g, level=self.level + 1, right=True, left=False, method=self.method, jump_method=self.jump_method, jump_metric=self.g)
		mid = numpy.copy(self.mins)
		mid[d] = split
		greater = type(self)(mid, self.maxes, self.metric, g=None if not reuse_g else self.g, level=self.level + 1, right=False, left=True, method=self.method, jump_method=self.jump_method, jump_metric=self.g)
		return less, greater

	def bisect(self, d, reuse_g=False):
		return self.split(d, self.center[d], reuse_g=reuse_g)

	def divide(self, size_method: SizeMethod = SizeMethod.Proper, reuse_g=False, aspect_ratios=None, global_factor: float = None, local_dist_bounds: Tuple[float, float] = None, local_dist_beta: Tuple[float, float] = None):
		proper_size = self.size(method=size_method, global_factor=global_factor, local_dist_bounds=local_dist_bounds, local_dist_beta=local_dist_beta, aspect_ratios=aspect_ratios)
		return self.bisect(numpy.argmax(proper_size), reuse_g=reuse_g)


	@property
	def coordinate_size(self):
		"""Determine the coordinate size of the rectangle. This is the size of the rectangle in each dimension ignoring the metric.

		Returns:
			numpy.ndarray, the size of the rectangle in each dimension (ignoring the metric)
		"""
		return self.maxes - self.mins

	@property
	def proper_size(self):
		"""Determines the proper size of the rectangle. This is the size of the rectangle in each dimension, as measured by the metric.

		Returns:
			numpy.ndarray, the size of the rectangle in each dimension (as measured by the metric)
		"""
		origin = numpy.zeros(len(self.edges))
		return numpy.array([self.metric.distance(self.g, origin, edge) for edge in self.edges])

	@property
	def local_aspect_ratio(self):
		"""The local aspect ratio is proper sizes of a unit-rectangle (unit in the coordinate sense) that is concentric with this rectangle.

		Returns:
			numpy.ndarray, the local aspect ratio of the rectangle
		"""
		origin = numpy.zeros(len(self.edges))
		p111 = numpy.diag(numpy.ones(len(self.edges)))
		return numpy.array([self.metric.distance(self.g, origin, unit_edge) for unit_edge in p111])

	@property
	def local_distortion(self):
		"""The local distortion is the ratio of the maximum aspect to minimum aspect of the local aspect ratio.

		Returns:
			float, the local distortion of the rectangle
		"""
		return numpy.max(self.local_aspect_ratio) / numpy.min(self.local_aspect_ratio)

	def interpolated_size(self, global_factor: float = None, local_dist_bounds: Tuple[float, float] = None, local_dist_beta: Tuple[float, float] = None):
		"""Interpolate the size of the rectangle between coordinate distance and metric distance in each dimension, allowing for both global and local control.

			Global control:
				Simply the linear interpolation factor, where 0 means the proper size and 1 means the coordinate size. The interpolation factor can be thought of
				as a control for metric insensitivity, where 0 means the distances are fully sensitive to the metric and 1 means distances are insensitive to the metric.

			Local control:
				More complicated, relies on a notion of "local distortion" and a beta distribution. "Local Distortion" is a relative measure of the local aspect ratio of the metric
				(local = at the center of the rectangle). We define the local distortion as the ratio of the maximum aspect to minimum aspect. Specifically, the local distortion is:

					local_distortion = max(local_aspect_ratio) / min(local_aspect_ratio)

				The local distortion is then used to determine the interpolation factor, which is controlled by a beta distribution. The beta distribution is parameterized by local_dist_beta, which
				controls the shape of the distribution, and local_dist_bounds, which controls the range of the distribution. Some examples:

					- If local_dist_beta = (1, 1), then the beta distribution is uniform, and the interpolation factor is simply the local_distortion, scaled to the range of local_dist_bounds.
					- If local_dist_beta = (1, 2), then the beta distribution is skewed towards the lower bound (proper size), and the interpolation factor is biased towards the lower bound (proper size).
					- If local_dist_beta = (2, 1), then the beta distribution is skewed towards the upper bound (coordinate size), and the interpolation factor is biased towards the upper bound (coordinate size).

				A motivating use case for local control is when a particular coordinate becomes nearly degenerate, and the metric becomes somewhat insensitive to the coordinate. For example, in the
				LogM1, LogM2, Chi coordinates, at low masses, the metric becomes less sensitive to the chi coordinate, and the chi coordinate becomes nearly degenerate. If unchecked, this can result
				in template rectangles that are extremely "skinny" in the chi direction, which can lead to poor coverage of the parameter space. By using local control, we can partially force the
				splitting to occur in the chi direction, even when the metric is insensitive to the chi coordinate, by forcing the interpolation factor to be biased towards the coordinate size.

		Args:
			global_factor:
				float, default None, the global factor for scaling the size of the rectangle. If None, then no global scaling is applied. If specified, then the size of the rectangle is
				linearly interpolated between the coordinate and proper sizes, with the global factor controlling the interpolation, where 0 means the coordinate size and 1 means the proper size.
			local_dist_bounds:
				Tuple[float, float], default None, the bounds for the local distortion. If specified, then the local distortion is scaled to the range of the local_dist_bounds. If None, then no local
				scaling is applied.
			local_dist_beta:
				Tuple[float, float], default None, the parameters for the beta distribution that controls the interpolation factor. If specified, then the interpolation factor is controlled by the beta
				distribution. If None, then no local scaling is applied.

		References:
			[1] https://en.wikipedia.org/wiki/Beta_distribution

		Returns:
			numpy.ndarray, the size of the rectangle in each dimension, interpolated between the coordinate and proper sizes
		"""
		factor = None

		if global_factor is not None:
			if global_factor < 0 or global_factor > 1:
				raise ValueError(f"Global factor must be in [0, 1], got {global_factor}")

			factor = global_factor

		if local_dist_bounds is not None and local_dist_beta is not None:
			# TODO move this config validation to a separate layer, likely with a formal specification of the config parameters
			if local_dist_bounds[0] > local_dist_bounds[1]:
				raise ValueError(f"Local distance bounds must be in increasing order, got local_dist_bounds={local_dist_bounds}")
			if local_dist_beta[0] <= 0 or local_dist_beta[1] <= 0:
				raise ValueError(f"Local distance beta parameters must be positive, got local_dist_beta={local_dist_beta}")

			# Unpack the local distance bounds and beta parameters
			dist_min, dist_max = local_dist_bounds
			beta_a, beta_b = local_dist_beta

			# Compute local distortion
			local_dist = self.local_distortion

			# Clip the local distortion to the bounds
			local_dist = min(dist_max, max(dist_min, local_dist))

			# Regularize the local distortion to the range of the beta distribution
			local_dist = (local_dist - dist_min) / (dist_max - dist_min)

			# Compute the interpolation factor using the beta distribution
			factor = beta(beta_a, beta_b).cdf(local_dist)

		if factor is not None:
			return factor * self.coordinate_size + (1 - factor) * self.proper_size

		raise ValueError(f"Must specify either global factor or local distance bounds and beta to interpolate size of rectangle, got global_factor={global_factor}, local_dist_bounds={local_dist_bounds}, local_dist_beta={local_dist_beta}")

	def size(self, method: SizeMethod = SizeMethod.Proper, global_factor: float = None, local_dist_bounds: Tuple[float, float] = None, local_dist_beta: Tuple[float, float] = None, aspect_ratios: numpy.ndarray = None):
		"""Determine the size of the rectangle in each dimension, with varying methodology for scaling the size. There are essentially three modes:

			1. method = "proper" (default): the size of the rectangle in each dimension, as measured by the metric
			2. method = "coordinate": the size of the rectangle in each dimension, ignoring the metric
			3. method = "interpolated": the size of the rectangle in each dimension, interpolated between the coordinate and proper sizes. The interpolation
				is controlled by several parameters, allowing for both global and local control, including the scale_strength, reg_a, reg_b, min_reg, and max_reg.

		Args:
			method:
				str, default "proper", the method for determining the size of the rectangle. Options are:
					- "coordinate": the size of the rectangle in each dimension, ignoring the metric
					- "interpolated": the size of the rectangle in each dimension, interpolated between the coordinate and proper sizes
					- "proper": the size of the rectangle in each dimension, as measured by the metric
			global_factor:
				float, default None, the global factor for scaling the size of the rectangle. If None, then no global scaling is applied. If specified, then the size of the rectangle is
				linearly interpolated between the coordinate and proper sizes, with the global factor controlling the interpolation, where 0 means the coordinate size and 1 means the proper size.
			local_dist_bounds:
				Tuple[float, float], default None, the bounds for the local distortion. If specified, then the local distortion is scaled to the range of the local_dist_bounds. If None, then no local
				scaling is applied.
			local_dist_beta:
				Tuple[float, float], default None, the parameters for the beta distribution that controls the interpolation factor. If specified, then the interpolation factor is controlled by the beta
				distribution. If None, then no local scaling is applied.
			aspect_ratios:
				numpy.ndarray, default None, the aspect ratios to use for splitting the rectangle. If None, then the aspect ratio is determined by the proper size of the rectangle. Only applied if method = "proper".

		Returns:
			numpy.ndarray, the size of the rectangle in each dimension, according to the specified method
		"""
		if method == SizeMethod.Coordinate:
			return self.coordinate_size

		if method == SizeMethod.Proper:
			if aspect_ratios is None:
				return self.proper_size

			return self.proper_size * aspect_ratios

		if method == SizeMethod.Interpolated:
			return self.interpolated_size(global_factor=global_factor, local_dist_bounds=local_dist_bounds, local_dist_beta=local_dist_beta)

		raise ValueError(f"Unknown size method: {method}, options are: {SizeMethod.Coordinate}, {SizeMethod.Proper}, {SizeMethod.Interpolated}")

	@property
	def volume(self):
		return Rectangle.volume(self)

	@property
	def proper_volume(self):
		return volume_element(self.g) * self.volume

	def patch(self, x=0, y=1):
		assert len(self.mins) > 1
		return patches.Rectangle(numpy.array([self.mins[x], self.mins[y]]), self.coordinate_size[x], self.coordinate_size[y], edgecolor='k', facecolor='none')

	def is_inside_constraints(self, constraints: Iterable[Callable], interpolation: int = 0):
		"""Check if the rectangle is inside a set of constraint functions

		Args:
			constraints:
				Iterable[Callable], a collection of functions that take a point and return True if the point is INSIDE the constraint,
				such that constraint(p) == True implies p PASSES the constraint
			interpolation:
				int, default 0, if > 0, then subdivide the rectangle into a grid of interpolation**len(self.mins) points and check each point
				for constraint satisfaction. If any point is inside the constraint, then the rectangle is considered inside the constraint.
				By default, the center and vertices of the rectangle are checked.

		Returns:
			bool, True if the rectangle is inside the constraints, False otherwise
		"""
		if interpolation > 0:
			# If specified, subdivide each axis into 2^interpolation + 1 points, then take cartesian product of these points to form grid
			grid = list(itertools.product(*[numpy.linspace(self.mins[i], self.maxes[i], 2 ** interpolation + 1) for i in range(len(self.mins))]))
		else:
			# Otherwise, just check the center and vertices
			grid = [self.center] + list(self.vertices)

		for p in grid:
			# Eagerly return True if any point is inside the constraint
			if all(constraint(p) for constraint in constraints):
				return True

		# If no point is inside the constraint, then the rectangle is outside the constraint
		return False


def tmpvol(mm=0.01, N=2):
	return (2 * (mm / N) ** .5) ** N


class Chart(object):

	def __init__(self, data, parent=None, excluded: List = None):
		self.left = None
		self.right = None
		self.data = data
		self.parent = parent

		# Determine the proper behavior regarding recording of excluded rectangles
		# If no excluded specified, attempt to inherit from parent
		if excluded is None:
			# If parent is None, then not tracking
			if parent is None:
				self.excluded = None
			# Otherwise, inherit from parent
			else:
				self.excluded = parent.excluded
		# Otherwise, set to specified value
		else:
			self.excluded = excluded

	def branch(self, mm=.02, reuse_g_mm=0.0, reuse_g_vol_tol=0.1, constraints=(lambda x: True,), verbose=False, cnt=[0], aspect_ratios=None, min_coord_vol=numpy.inf, min_depth=11, max_num_templates=1,
			   size_method: SizeMethod = SizeMethod.Proper, global_factor: float = None, local_dist_bounds: Tuple[float, float] = None, local_dist_beta: Tuple[float, float] = None):

		def inside(r, constraints=constraints):
			# Assume inside
			out = True

			# For each constraint
			for constraint in constraints:
				# Recall, constraint(v) returns True if v is OUTSIDE the constraint, so True = Failure
				# If all vertices and the center are outside the constraint, then the rectangle is outside the constraint
				out &= not all(constraint(v) for v in list(r.data.vertices) + [list(r.data.center)])
			return out

		def boundary(r, constraints=constraints):
			out = False
			for constraint in constraints:
				out |= any(constraint(v) for v in r.data.vertices)
			return out

		proper_volume = self.data.proper_volume
		tvol = tmpvol(mm, len(self.data.center))
		Ntmps = max(proper_volume / tvol, self.data.volume / min_coord_vol)
		volratio = (numpy.inf if self.parent is None else self.parent.data.proper_volume) / proper_volume / 2.0
		reuse_g = (proper_volume < tmpvol(reuse_g_mm, len(self.data.center))) and ((1 - reuse_g_vol_tol) < volratio < (1 + reuse_g_vol_tol))
		if Ntmps > max_num_templates or self.data.level < min_depth + 1:
			ldat, rdat = self.data.divide(size_method=size_method, reuse_g=reuse_g, aspect_ratios=aspect_ratios, global_factor=global_factor, local_dist_bounds=local_dist_bounds, local_dist_beta=local_dist_beta)
			left = Chart(ldat, self)
			right = Chart(rdat, self)
			inl = inside(left)
			inr = inside(right)

			# If left subdivision is inside the constraints (or is insufficiently deep), then keep track of it and potentially proceed to the next level
			if inl or left.data.level < min_depth:
				self.left = left
				self.left.branch(mm=mm, reuse_g_mm=reuse_g_mm, reuse_g_vol_tol=reuse_g_vol_tol, constraints=constraints, verbose=verbose, cnt=cnt, aspect_ratios=aspect_ratios, min_coord_vol=min_coord_vol, min_depth=min_depth, max_num_templates=max_num_templates,
								 size_method=size_method, global_factor=global_factor, local_dist_bounds=local_dist_bounds, local_dist_beta=local_dist_beta)
			else:
				# Record the pruned rectangle if applicable
				if self.excluded is not None:
					self.excluded.append(left.data)

			# If right subdivision is inside the constraints (or is insufficiently deep), then keep track of it and potentially proceed to the next level
			if inr or right.data.level < min_depth:
				self.right = right
				self.right.branch(mm=mm, reuse_g_mm=reuse_g_mm, reuse_g_vol_tol=reuse_g_vol_tol, constraints=constraints, verbose=verbose, cnt=cnt, aspect_ratios=aspect_ratios, min_coord_vol=min_coord_vol, min_depth=min_depth, max_num_templates=max_num_templates,
								  size_method=size_method, global_factor=global_factor, local_dist_bounds=local_dist_bounds, local_dist_beta=local_dist_beta)
			else:
				# Record the pruned rectangle if applicable
				if self.excluded is not None:
					self.excluded.append(right.data)

		elif verbose:
			cnt[0] += 1
			if not cnt[0] % 100: print(cnt[0], self.data, volratio)

	def atlas(self):
		if self.left:
			yield from self.left.atlas()
		if self.right:
			yield from self.right.atlas()
		if not self.left and not self.right:
			yield self.data
