"""CBC specific script interfaces for bank generation and testing.

This module contains the following functions:

- bank: Generate a bank of templates
- group: Group a bank of templates into multiple banks
- add: Add multiple banks together
- constrain: Constrain a bank of templates
- test: Test a bank of templates
- test_add: Add multiple tests together
- test_plot: Plot a bank of tests
- mass_model: Generate a mass model for a bank
- mass_model_add: Add multiple mass models together

Most of these functions are called from the command line, and are intended to be used as standalone scripts. They are not intended to be imported into other scripts.
Also, most of these functions accept a configuration dictionary as an argument, which is derived from a YAML file. We specify the configuration options for each function in the docstring of the function itself.
"""

import numpy
import os
import h5py

import matplotlib

from manifold.cover import SizeMethod
from manifold.metric import EvaluationMethod, JumpMethod

matplotlib.use('agg')
matplotlib.rcParams.update({
    "font.size": 9.0,
    "axes.titlesize": 9.0,
    "axes.labelsize": 9.0,
    "xtick.labelsize": 9.0,
    "ytick.labelsize": 9.0,
    "legend.fontsize": 8.0,
    "figure.dpi": 300,
    "savefig.dpi": 300,
    "text.usetex": True,
    "path.simplify": True,
    "font.family": "serif"
})
from matplotlib import pyplot
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from math import ceil, floor, log10

import manifold.utilities.common
import manifold.utilities.data
from manifold import cover
from manifold import io
from manifold.sources import cbc

def compute_mchirp(m1, m2):
	return (m1 * m1 * m1 * m2 * m2 * m2 / (m1 + m2))**0.2

def compute_eta(m1, m2):
	return (m1 * m2)/(m1 + m2)**2.

def compute_M(m1, m2):
	return (m1 + m2)

def compute_chi(m1, m2, s1z, s2z):
	return (m1 * s1z + m2 * s2z) / (m1 + m2)

def compute_mratio(m1, m2):
	return (m1 / m2)

def override(config, opt, *opts):
    for o in opts:
        thisopt = getattr(opt, o.replace("-","_"))
        if thisopt: config[o] = thisopt

def bank(config: dict):
    """Bank generation script for CBC templates

    Args:
        config:
            dict of config options

    Returns:
        None
    """
    # NOTE FIXME require symmetric chi for now
    assert (config['chi'][0] == -config['chi'][1])
    if config['chi'][0] == config['chi'][1] == 0:
        cfunc = cbc.coord_funcs['log10m1_log10m2'](**manifold.utilities.common.bounds_from_lists(['m1', 'm2',], [config['m1'][0], config['m2'][0]], [config['m1'][1], config['m2'][1]]))
        aspect_ratios = numpy.array([1., 1.])
    else:
        cfunc = cbc.coord_funcs['log10m1_log10m2_chi'](**manifold.utilities.common.bounds_from_lists(['m1', 'm2', 'S1z', 'S2z'], [config['m1'][0], config['m2'][0], config['chi'][0], config['chi'][0]], [config['m1'][1], config['m2'][1], config['chi'][1], config['chi'][1]]))
        aspect_ratios = numpy.array([1., 1., 4.])

    # override aspect ratios if they exist
    # TODO make the default "None" (the above lines are subtle and disagree with method signatures where aspect_ratios is defaulted to None,
    #  users might be surprised at default behavior of script != default behavior of method)
    if "aspect-ratios" in config:
        if config['aspect-ratios'] is None or config['aspect-ratios'] == 'None':
            aspect_ratios = None
        else:
            aspect_ratios = numpy.array(config['aspect-ratios'])

    # Check evaluation method config options
    if 'method' not in config:
        config['method'] = EvaluationMethod.Deterministic

    # Check jump config options:
    if 'jump_method' not in config:
        config['jump_method'] = JumpMethod.Truncation

    g = cbc.CBCMetric(manifold.utilities.data.read_psd(config['psd-xml'], config['instrument'], config['verbose'],),
			  cfunc,
			  config['freq'][0],
			  config['freq'][1],
			  max_duration = config['max-duration'],
			  approximant = config['approximant'])

    if config['jump_method'] == JumpMethod.Ellipsoid:
        # Need to compute the initial jump metric (subsequent ones will inherit from parent rectangle)
        center_point = (numpy.array(g.coord_func.mins) + numpy.array(g.coord_func.maxes)) / 2
        center_metric = g(center_point, method=config['method'], jump_method=JumpMethod.Truncation)
    else:
        center_metric = None

    constraints = {'M': numpy.array(config['M']),
                   'mc': numpy.array(config['mc']),
                   'q': numpy.array(config['q']),
                   'chi': numpy.array(config['chi']),
                   'm1': numpy.array(config['m1']),
                   'm2': numpy.array(config['m2']),
                   'ns-mass': numpy.array(config['ns-mass']),
                   'ns-s1z': numpy.array(config['ns-s1z'])
                   }

    # FIXME replace these with inline lambdas below
    def m1c(v, constraints=constraints, cfunc=cfunc):
        return cbc.Bank.m1_constraint(constraints, cfunc(numpy.array(v)))

    def m2c(v, constraints=constraints, cfunc=cfunc):
        return cbc.Bank.m2_constraint(constraints, cfunc(numpy.array(v)))

    def Mc(v, constraints=constraints, cfunc=cfunc):
        return cbc.Bank.M_constraint(constraints, cfunc(numpy.array(v)))

    def mcc(v, constraints=constraints, cfunc=cfunc):
        return cbc.Bank.mc_constraint(constraints, cfunc(numpy.array(v)))

    def qc(v, constraints=constraints, cfunc=cfunc):
        return cbc.Bank.q_constraint(constraints, cfunc(numpy.array(v)))

    def chic(v, constraints=constraints, cfunc=cfunc):
        return cbc.Bank.chi_constraint(constraints, cfunc(numpy.array(v)))

    constraint_funcs = [m1c, m2c, Mc, mcc, qc, chic]

    # Assemble jump validator
    def validator(jump_point):
        dummy_rect = cover.DummyRectangle(jump_point)
        return cbc.Bank._inside(r=dummy_rect, constraints=constraints, cfunc=cfunc, check_vertices=False)


    if 'seedbank' in config and config['seedbank'] is not None:
        seedbank = cbc.Bank.load(config['seedbank'])
        # FIXME add some other checks
        assert len(seedbank.rectangles) == 1
        R = cbc.ManifoldRectangle(seedbank.rectangles[0].mins, seedbank.rectangles[0].maxes, g, method=config['method'],
                                  jump_method=config['jump_method'], jump_metric=center_metric, jump_validator=validator)
    else:
        R = cbc.ManifoldRectangle(g.coord_func.mins, g.coord_func.maxes, g, method=config['method'],
                                  jump_method=config['jump_method'], jump_metric=center_metric, jump_validator=validator)

    if config.get('track-excluded', False):
        print("Tracking excluded templates")
        C = cbc.Chart(R, excluded=[])
    else:
        C = cbc.Chart(R)


    size_kwargs = {}

    if "size" in config and config['size']:
        size_kwargs['size_method'] = config['size']['method']
        size_kwargs['global_factor'] = config['size'].get('global-factor', None)
        size_kwargs['local_dist_bounds'] = config['size'].get('local-dist-bounds', None)
        size_kwargs['local_dist_beta'] = config['size'].get('local-dist-beta', None)

    C.branch(mm = config['mm'], reuse_g_mm = config['reuse-g-mm'], constraints = constraint_funcs, verbose = config['verbose'], aspect_ratios = aspect_ratios,
             min_coord_vol = config['min-coord-vol'], max_num_templates = config['max-num-templates'], min_depth = config['min-depth'], **size_kwargs)

    bank = cbc.Bank.fromChart(C, constraint_sets={'padding': constraints}, default_constraint_set_name='padding')

    if "post-constraints" in config and config['post-constraints']:
        # now that the bank is made with padded constraints, optionally constrain down to what you want and make that default.
        nc = bank.constraints().copy()
        for c in ("M", "mc", "q", "chi", "m1", "m2", "ns-mass", "ns-s1z"):
            if c in config:
                nc[c] = numpy.array(config["post-constraints"][c])
        bank.add_constraint_set(name='search', constraints=nc, make_default=True)

    if config['trim']:
        bank.trim()

    if 'boundary-fine-grain' in config and config['boundary-fine-grain']:
        bfg = config['boundary-fine-grain']
        bank.fine_grain_boundary(threshold=bfg.get('threshold', 0.5),
                                 target_mismatch=bfg.get('target-mismatch', 0.03),
                                 n_points=bfg.get('n-points', 100),
                                 max_splits=bfg.get('max-splits', 5),
                                 object_symmetric=bfg.get('object-symmetric', False),
                                 sharpness=bfg.get('sharpness', 1.0),
                                 aspect_ratios = aspect_ratios,
                                 ebr_scale_method=bfg.get('ebr-scale-method', None),
                                 ebr_scale_pow=bfg.get('ebr-scale-pow', 5.0),
                                 **size_kwargs)

    bank.save(config['output-h5'])




def group(config):
    Bank = cbc.Bank.load(config['bank'])
    num_groups = len(Bank.rectangles) if ("num-groups" not in config or config['num-groups'] is None) else config['num-groups']
    out_dir = "." if ("out-dir" not in config or config['out-dir'] is None) else config['out-dir']
    assert (num_groups < 1000000)
    for i, bank in enumerate(Bank.group(num_groups)):
        fname = "%s-%05d_%s-%s-%s.h5" % (config['ifo'], i, config['tag'], config['start'], config['dur'])
        bank.save(os.path.join(out_dir, fname))


def add(config):
    """
Config is dictionary derived from YAML file like this:

banks: [H1L1V1-O4_MANIFOLD_BANK-0-2000000000.h5, ...]
output-h5: H1L1V1-O4_MANIFOLD_BANK_CONSTRAINED-0-2000000000.h5
    """
    constraints = None
    cfunc = None
    rectangles = []
    ids = []
    excluded_branching = []
    excluded_trimmed = []
    for n, bank in enumerate(config['banks']):
        Bank = cbc.Bank.load(bank)
        if constraints is None: constraints = Bank.constraints()
        if cfunc is None: cfunc = Bank.cfunc
        #assert (Bank.constraints == constraints)
        #assert (Bank.cfunc == cfunc)
        rectangles += Bank.rectangles
        ids += bank.ids.tolist()
        excluded_branching += Bank.exclusions['branching']
        excluded_trimmed += Bank.exclusions['trimming']
        print("processing %04d/%04d : Have %08d templates" % (n, len(config['banks']), len(rectangles)))
    Bank = cbc.Bank(rectangles, {'default': constraints}, exclusions_branched=excluded_branching, exclusions_trimmed=excluded_trimmed)
    Bank.save(config['output-h5'])

def constrain(config):
    """
    Config is dictionary derived from YAML file like this:

    m1: [1., 190]
    m2: [1.,190]
    mc: [0.871, 165]
    q: [1., 20]
    M: [2., 380]
    chi: [-0.95, 0.95]
    ns-mass: [0, 3.0]
    ns-s1z: [-0.04, 0.04]
    bank: H1L1V1-O4_MANIFOLD_BANK-0-2000000000.h5
    output-h5: H1L1V1-O4_MANIFOLD_BANK_CONSTRAINED-0-2000000000.h5
    """

    bank = cbc.Bank.load(config['bank'])
    nc = bank.constraints().copy()
    for c in ("M", "mc", "q", "chi", "m1", "m2", "ns-mass", "ns-s1z"):
        if c in config:
            nc[c] = numpy.array(config[c])
    bank.add_constraint_set(name='search', constraints=nc, make_default=True)
    if "trim" in config and config["trim"]:
    	bank.trim()
    if "assign-ids" in config and config["assign-ids"]:
    	bank.assign_ids()
    bank.save(config['output-h5'])


def test(config):#args=None, show_plot: bool = True, verbose: bool = True):
    """
Config is dictionary derived from YAML file like this:

num-injections: 20
bank: H1L1V1-O4_MANIFOLD_BANK_CONSTRAINED-0-2000000000.h5
output-h5: test.h5
    """
    Bank = cbc.Bank.load(config['bank'])

    m1s, m2s, s1s, s2s = [], [], [], []
    mismatches = []

    while len(mismatches) < config['num-injections']:
        c = Bank.rectangles[0].metric.coord_func.random()
        if Bank.constraint(c):
            continue
        match = Bank.nearest(c)
        m1s.append(c[cbc.M1])
        m2s.append(c[cbc.M2])
        s1s.append(c[cbc.S1Z])
        s2s.append(c[cbc.S2Z])
        mismatches.append(1.-match)
        print (len(mismatches), match)

    io.h5write(config['output-h5'], {'m1s': numpy.array(m1s), 'm2s':numpy.array(m2s), 's1s':numpy.array(s1s), 's2s': numpy.array(s2s), 'mismatches':numpy.array(mismatches)})

def test_add(config):
    """
Config is dictionary derived from YAML file like this:

banks: [01-test.h5, 02-test.h5, ...]
output-h5: test.h5
    """

    m1s, m2s, s1s, s2s = [], [], [], []
    mismatches = []

    for arg in config['tests']:
        t = io.h5read(arg)
        m1s.extend(numpy.array(t['m1s']))
        m2s.extend(numpy.array(t['m2s']))
        s1s.extend(numpy.array(t['s1s']))
        s2s.extend(numpy.array(t['s2s']))
        mismatches.extend(numpy.array(t['mismatches']))

    io.h5write(config['output-h5'], {'m1s': numpy.array(m1s), 'm2s':numpy.array(m2s), 's1s':numpy.array(s1s), 's2s': numpy.array(s2s), 'mismatches':numpy.array(mismatches)})

def test_plot(config):
    """
Config is dictionary derived from YAML file like this:

test: test.h5
output-png: test.png
    """
    test = io.h5read(config['test'])
    matches = None

    # obtain parameters
    mismatches = test['mismatches']
    matches = 1 - mismatches
    ix = numpy.argsort(mismatches)
    matches = matches[ix]
    m1s = test['m1s'][ix]
    m2s = test['m2s'][ix]
    s1s = test['s1s'][ix]
    s2s = test['s2s'][ix]

    # derived quatities
    match_90percent = matches[int(floor(len(matches)*0.9))]
    match_50percent = matches[int(floor(len(matches)*0.5))]
    smallest_match = matches.min()
    inj_M = compute_M(m1s, m2s)
    inj_eta = compute_eta(m1s, m2s)
    inj_mchirp = compute_mchirp(m1s, m2s)
    inj_chi = compute_chi(m1s, m2s, s1s, s2s)
    inj_mratio = compute_mratio(m1s, m2s)

    # plotting
    fig = Figure()
    ax = fig.add_subplot(111)
    ax.plot(m1s, matches, marker = '.', linestyle="None")
    ax.set_xlabel("Injection $m_1$ ($M_\odot$)", fontsize=12)
    ax.set_ylabel("Fitting Factor Between Injection and Template Bank", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injm1.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    ax.plot(m2s, matches, marker = '.', linestyle="None")
    ax.set_xlabel("Injection $m_2$ ($M_\odot$)", fontsize=12)
    ax.set_ylabel("Fitting Factor Between Injection and Template Bank", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injm2.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    ax.plot(inj_M, matches, marker = ".",linestyle="None")
    ax.set_xlabel("Injection $M$ ($M_\odot$)", fontsize=12)
    ax.set_ylabel("Fitting Factor Between Injection and Template Bank", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injM.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    ax.plot(inj_mchirp, matches, marker = ".",linestyle="None")
    ax.set_xlabel("Injection $M$ ($M_\odot$)", fontsize=12)
    ax.set_ylabel("Fitting Factor Between Injection and Template Bank", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injmchirp.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    ax.plot(inj_eta, matches, marker = ".", linestyle="None")
    ax.set_xlabel("Injection $\eta$", fontsize=12)
    ax.set_ylabel("Match Between Injection and Template Bank", fontsize=12)
    ax.set_xlim([inj_eta.min(), 0.251])
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injeta.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    collection = ax.scatter(m1s, m2s, c=matches, s=20, vmin=smallest_match, linewidth=0, alpha=0.5, vmax=1)
    ax.set_xlabel("Injected $m_1$ ($M_\odot$)", fontsize=12)
    ax.set_ylabel("Injected $m_2$ ($M_\odot$)", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    fig.colorbar(collection, ax=ax).set_label("Fitting Factor", fontsize=12)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injm2_vs_injm1.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    collection = ax.scatter(s1s, s2s, c=matches, s=20, vmin=smallest_match, linewidth=0, alpha=0.5, vmax=1)
    ax.set_xlabel("Injected $s_{1z}$", fontsize=12)
    ax.set_ylabel("Injected $s_{2z}$", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    fig.colorbar(collection, ax=ax).set_label("Fitting Factor", fontsize=12)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injs1z_vs_injs2z.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    ax.plot(inj_mratio, matches, marker = ".", linestyle="None")
    ax.set_xlabel("Injection mass ratio", fontsize=12)
    ax.set_ylabel("Fitting Factor Between Injection and Template Bank", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_inj_mass_ratio.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_axes((0.1, 0.1, 0.85, 0.85), xscale="log", yscale="log")
    count, bin_edges = numpy.histogram(numpy.log(1 - matches + 1e-5), bins=40) # fudge factor to avoid log(0)
    bin_centers = numpy.exp(-(bin_edges[:-1] * bin_edges[1:])**0.5)
    ax.plot(bin_centers, count, linewidth=2.5)
    ax.plot((1 - match_90percent, 1 - match_90percent), (1, 10**ceil(log10(count.max()))), "k--")
    ax.plot((1 - match_50percent, 1 - match_50percent), (1, 10**ceil(log10(count.max()))), "k--")
    ax.set_xlabel("mismatch (1 - fitting factor)", fontsize=12)
    ax.set_ylabel("Number", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.set_title(r'$N_\mathrm{inj}=%d$, 10th percentile=%.4f; median=%.4f' % (len(matches), match_90percent, match_50percent), fontsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_hist.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    mismatches = 1 - matches
    mismatches.sort()
    percent = numpy.arange(1, len(mismatches)+1) / len(mismatches)
    mm90 = mismatches[numpy.searchsorted(percent, 0.90)]
    ax.plot(mismatches, numpy.arange(len(mismatches)) / len(mismatches))
    ax.vlines(mm90, 0, 1, linestyles = 'dashed')
    ax.set_xlim(0, max(mismatches))
    ax.set_xlabel('mismatch (1 - fitting factor)', fontsize=12)
    ax.set_ylabel('fraction', fontsize=12)
    ax.set_title(r'$N_\mathrm{inj}=%d$, 90th percentile=%.4f' % (len(matches), match_90percent,), fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    canvas = FigureCanvas(fig)
    fig.savefig("match_cumulative.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    collection = ax.scatter(inj_M, inj_chi, c=matches, s=20, vmin=smallest_match, linewidth=0, alpha=0.5, vmax=1)
    ax.set_xlabel("Injected Total Mass", fontsize=12)
    ax.set_ylabel("Injected $%s$" % "\chi_\mathrm{eff}", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    fig.colorbar(collection, ax=ax).set_label("Fitting Factor", fontsize=12)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injM_vs_injchi.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    collection = ax.scatter(numpy.log10(inj_M), inj_chi, c=matches, s=20, vmin=smallest_match, linewidth=0, alpha=0.5, vmax=1)
    ax.set_xlabel("Injected Total Mass", fontsize=12)
    ax.set_ylabel("Injected $%s$" % "\chi_\mathrm{eff}", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    fig.colorbar(collection, ax=ax).set_label("Fitting Factor", fontsize=12)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injlogM_vs_injchi.png", bbox_inches='tight')

    fig = Figure()
    ax = fig.add_subplot(111)
    collection = ax.scatter(numpy.log10(inj_mchirp), inj_chi, c=matches, s=20, vmin=smallest_match, linewidth=0, alpha=0.5, vmax=1)
    ax.set_xlabel("Log Injected Chirp Mass", fontsize=12)
    ax.set_ylabel("Injected $%s$" % "\chi_\mathrm{eff}", fontsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.grid(True)
    fig.colorbar(collection, ax=ax).set_label("Fitting Factor", fontsize=12)
    canvas = FigureCanvas(fig)
    fig.savefig("match_vs_injlogmchirp_vs_injchi.png", bbox_inches='tight')

def unique_temp_ids(ids):
	s = set()
	for temp_id in ids:
		if temp_id in s:
			raise ValueError("template id %d exists in Bank.ids" % temp_id)
		s.add(temp_id)
	return len(ids) == len(set(ids))

def mass_model(config):
    """
Config is dictionary derived from YAML file like this (just an example):

bank: H1L1V1-MANIFOLD-0-2000000000.h5
output-h5: H1L1V1-MANIFOLD_SALPETER_MODEL-0-2000000000.h5
start-ix:
stop-ix:
mass_model:
    model: power-law
    index: -2.0
    m_min: 0.8
    cuts:
        m1: [2.5,500]
        m2: [0.9,2.5]
        x1: [-0.95,0.95]
        x2: [-0.05,0.05]
    """
    Bank = cbc.Bank.load(config['bank'])
    startix = 0 if ('start-ix' not in config or not config['start-ix']) else config['start-ix']
    stopix = len(Bank) if ('stop-ix' not in config or not config['stop-ix'])else config['stop-ix']
    numsamps = len(Bank)#stopix - startix

    # check that the template ids are unique
    assert unique_temp_ids(Bank.ids)

    # setup data for the mass model
    p_of_tj_given_alpha = numpy.zeros(numsamps)
    coords = numpy.zeros((3, numsamps))
    cfunc = Bank.rectangles[0].metric.coord_func

    # Assert that we have a log m1 log m2 coord func
    assert cfunc.key == "log10m1_log10m2_chi"

    # Find and store minimum mass of bank
    m_min_bank = min(numpy.array([cfunc(r.center)[cbc.M2] for r in Bank.rectangles]))

    if config['mass_model']['m_min'] is None:
        config['mass_model']['m_min'] = 0.8
    assert config['mass_model']['m_min']< 0.95*m_min_bank, "Please specify a minimum mass smaller than 0.95 times the minimum mass of the bank which is:"+str(m_min_bank)


    # First calculate the probability of template j given alpha
    for ix, (ID, r) in enumerate(zip(Bank.ids, Bank.rectangles)):
            coords[:,ix], p_of_tj_given_alpha[ix] = cfunc.mass_model(r, **config['mass_model'])

    rhos, log_p_of_tk_given_alpha_rho = Bank.prob_of_tk_given_rho(p_of_tj_given_alpha, start_ix = startix, stop_ix = stopix)

    f = h5py.File(config['output-h5'], "w")
    f.create_dataset("valid_templates", data = numpy.arange(startix, stopix))
    f.create_dataset("rhos", data = numpy.array(rhos))
    f.create_dataset("log_p_of_tk_given_alpha_rho", data = numpy.array(log_p_of_tk_given_alpha_rho), compression="gzip")
    f.create_dataset("template_id", data = numpy.array(Bank.ids).astype(int))
    f.create_dataset("m1s", data = coords[0,:])
    f.create_dataset("m2s", data = coords[1,:])
    f.create_dataset("chis", data = coords[2,:])
    # only compute coefficients for a complete mass model
    if startix == 0 and stopix == len(Bank):
        coefficients, breakpoints = Bank.mass_model_coefficients(rhos, log_p_of_tk_given_alpha_rho)
        f.create_dataset("SNR", data = numpy.array(breakpoints))
        f.create_dataset("coefficients", data = coefficients, compression="gzip")

    f.close()


def mass_model_add(config):
    """
Config is dictionary derived from YAML file like this:

bank: H1L1V1-MANIFOLD-0-2000000000.h5
mass-models: [model1.h5, model2.h5, ...]
output-h5: H1L1V1-MANIFOLD_SALPETER_MODEL-0-2000000000.h5
    """
    Bank = cbc.Bank.load(config['bank'])

    # Take some things from the first mass model
    with h5py.File(config['mass-models'][0]) as f:
        valid = set(numpy.array(f["valid_templates"]))
        rhos = numpy.array(f["rhos"])
        log_p_of_tk_given_alpha_rho = numpy.array(f["log_p_of_tk_given_alpha_rho"])
        tids = numpy.array(f["template_id"])
        m1s = numpy.array(f["m1s"])
        m2s = numpy.array(f["m2s"])
        chis = numpy.array(f["chis"])


    # two things are true when creating mass models
    # 1) We use nan to fill values that were not processed
    # 2) We assert that a log(p) is always less than zero
    #
    for mf in config['mass-models'][1:]:
        with h5py.File(mf) as f:
            these_valid = set(numpy.array(f["valid_templates"]))
            assert these_valid.isdisjoint(valid)
            valid |= these_valid
            this_log_p = numpy.array(f["log_p_of_tk_given_alpha_rho"])
            these_ixs = ~numpy.isnan(this_log_p)
            log_p_of_tk_given_alpha_rho[these_ixs] = this_log_p[these_ixs]


    f = h5py.File(config['output-h5'], "w")
    f.create_dataset("valid_templates", data = numpy.array(sorted(valid)))
    f.create_dataset("rhos", data = numpy.array(rhos))
    f.create_dataset("log_p_of_tk_given_alpha_rho", data = numpy.array(log_p_of_tk_given_alpha_rho), compression="gzip")
    f.create_dataset("template_id", data = tids)
    f.create_dataset("m1s", data = m1s)
    f.create_dataset("m2s", data = m2s)
    f.create_dataset("chis", data = chis)
    # only compute coefficients for complete mass models
    if len(valid) == len(Bank):
        coefficients, breakpoints = Bank.mass_model_coefficients(rhos, log_p_of_tk_given_alpha_rho)
        f.create_dataset("SNR", data = numpy.array(breakpoints))
        f.create_dataset("coefficients", data = coefficients, compression="gzip")
    f.close()


