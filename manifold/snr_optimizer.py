# Copyright (C) 2020 Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import sys
import os
import json, logging, numpy, scipy, scipy.stats
from scipy.constants import speed_of_light
import math
from collections import OrderedDict
import time
import tempfile

# Gstreamer imports
import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
GObject.threads_init()
Gst.init(None)

# lal imports
import lal
import lalsimulation as lalsim
from lalinspiral import thinca
from lalburst.snglcoinc import light_travel_time
from lal import LIGOTimeGPS
from lal import series
from ligo.lw.utils import segments as ligolw_segments
from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw import param as ligolw_param
# define a content handler
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass
lsctables.use_in(LIGOLWContentHandler)

from gstlal.psd import measure_psd, HorizonDistance
from gstlal import datasource, pipeparts, simplehandler
from gstlal.pipeparts.condition import mkcondition
from gstlal import chirptime, templates, spawaveform
from gstlal.templates import ceil_pow_2 as cp2
from gstlal.stats.inspiral_extrinsics import TimePhaseSNR

# mass imports
from manifold.initializer import initializer
from manifold.np import npencode
from manifold.series import r8t, r4t, c8f, c8t, r4fft as r4fftclass, cpc16f, rs2r4t, rsarr, nipr4t, expandc16f, truncatec16f, truncater8f
from manifold import signal
from manifold import io

# manifold imports
from manifold.sources.cbc import M1, M2, S1Z, S2Z, scale_g
from manifold import cover

# FIXME this instantiates a module level class which holds not-threadsafe fft plans
rs2r4t = rs2r4t()
resample = rsarr()

SEARCHPAD = 1

#
# Utility functions
#

def print_tseries(tseries):
	"""
	Utility function for logging LAL Time Series information
	"""
	return """
name	: %s
epoch	: %s
deltaT	: %f
length	: %d
""" % (tseries.name, tseries.epoch, tseries.deltaT, tseries.data.length)


def print_fseries(fseries):
	"""
	Utility function for logging LAL Frequency Series information
	"""
	return """
name	: %s
epoch	: %s
deltaF	: %f
length	: %d
""" % (fseries.name, fseries.epoch, fseries.deltaF, fseries.data.length)


def format_data(start, stop, max_durations, search_window, search_pad, _out, _rate, ifos):
	assert type(search_pad) == int
	assert type(max_durations) == int
	assert type(search_window) == int

	_trig =  {"data_epoch": lal.LIGOTimeGPS(start - (max_durations - search_window - search_pad)), "data_length": int(max_durations * _rate)}

	six = {ifo: int((float(_trig["data_epoch"]) - _out["data"][ifo]["t0"]) * _rate) for ifo in ifos}

	splusns_data = {ifo: r4t(_out["data"][ifo]["data"][six[ifo]:six[ifo]+_trig["data_length"]], _trig["data_epoch"], 1. / _rate) for ifo in ifos}

	return splusns_data


def computechi(m1,m2,s1,s2):
	return (m1*s1+m2*s2) / (m1+m2)


class SnglSample(object):
	"""
	A class to store a single CBC GW trigger 
	"""
	@initializer
	def __init__(self, L, snr, time, phase, chisq, chisq_dof, perp_snr, snr_series):
		"""
		L             : Likelihood ratio of the sample
		snr           : the SNR of this sample
		time          : the coalescence time
		phase         : the coalescence phase
		chisq         : autochisq
		chisq_dof     : autochisq degrees of freedom
		perp_snr      : SNR perpendicular to the template (assuming data flagged as nonstationary)
		snr_series    : The snr time series that produced this trigger
		"""

	def to_dict(self):
		"""
		Write out the sample as a dictionary suitable for json encoding
		"""
		return  {
			"L" : float(self.L),
			"snr" : float(self.snr),
			"time" : self.time,
			"phase" : float(self.phase),
			"chisq" : float(self.chisq),
			"chisq_dof" : float(self.chisq_dof),
			"perp_snr" : float(self.perp_snr)
			}

	def __repr__(self):
		return json.dumps(self.to_dict(), sort_keys=True, indent=4, cls=io.JSONEncoder)

def p_of_horizon(largest_hd):
	return (largest_hd / 315.)**3

def chirpmass(params):
	return (params['m1'] * params['m2'])**.6 / (params['m1'] + params['m2'])**.2

def _eta(params):
	return params["m1"] * params["m2"] / (params["m1"] + params["m2"])**2

def coinc_LR(inspiral_extrinsics, combos, sngl_L, time, phase, snr, horizon, maximize_L = False):
	L_details = {}
	best = []

	if not maximize_L:
		# only go through combos having the largest number of ifos, since those will be the max snr ones
		lens = [len(combo) for combo in combos]
		combos = [combo for combo in combos if len(combo) == max(lens)]
	for combo in combos:
		# FIXME Sometimes an ifo will have snr 0
		# Figure out why this is happening
		# This is a band-aid to remove such ifos
		combo = tuple(ifo for ifo in combo if snr[ifo] != 0.)

		# apply the extrinsic parameters terms
		this_snr = sum([snr[ifo]**2 for ifo in combo])**.5

		if maximize_L:
			L_details[",".join(sorted(combo))] = {
				'snr_chisq': numpy.sum([sngl_L[ifo] for ifo in combo]),
				'time_phase_snr': float(numpy.log(inspiral_extrinsics.time_phase_snr({k: time[k] for k in combo}, {k: phase[k] for k in combo}, {k: snr[k] for k in combo}, {k: horizon[k] for k in combo}) / 157.91367041648155 * this_snr**4)),# NOTE - the norm such that single IFO has p = 1 and the LogL doesn't change
			}
			this_L = sum(L_details[",".join(sorted(combo))].values())
		else:
			this_L = None
			L_details = None

		best.append((this_snr, this_L, combo))

	maxSNR, maxL, maxcombo = max(best)
	return maxL, maxSNR, maxcombo, L_details

class ThreeDetectorCoincidence():
	"""
	Class for identifying valid coincidences for a network of 3 detectors.
	"""

	_detector_locations = { lal.CachedDetectors[det].frDetector.prefix : lal.CachedDetectors[det].location for det in range(lal.NUM_DETECTORS) }

	def __init__(self, ifo_time_dict, ifo3, srate=4096):
		"""
		Initializes for a particular set of 3 IFOs, and for arrival
		times for two IFOs; e.g., {"H1": 0, "L1": 0.01}, "V1".
		"""

		assert len(ifo_time_dict) == 2, "need times for exactly 2 ifos to initialize a ThreeDetectorCoincidence object"

		# decide an order for the ifos in ifo_time_dict
		(ifo1, ifo2) = list(ifo_time_dict.keys())
		self.time1 = ifo_time_dict[ifo1]
		self.time2 = ifo_time_dict[ifo2]

		# timing uncertainties are based on sample intervals
		self.s1 = self.s2 = self.s3 = 1 / srate

		# detector locations
		r1 = self._detector_locations[ifo1]
		r2 = self._detector_locations[ifo2]
		r3 = self._detector_locations[ifo3]

		# construct detector location matrix
		rbar = r1 / self.s1**2 + r2 / self.s2**2 + r3 / self.s3**2
		rbar /= 1 / self.s1**2 + 1 / self.s2**2 + 1 / self.s3**2
		self.M = numpy.array([(r1 - rbar)/self.s1/speed_of_light,
						   (r2 - rbar)/self.s2/speed_of_light,
						   (r3 - rbar)/self.s3/speed_of_light])

		# pseudo-inverse of detector location matrix
		self.Mpinv = numpy.linalg.pinv(self.M)

	def __call__(self, t3, thresh=None):
		"""
		Determines which of an array of time-of-arrival 3-vectors represents a valid coincidence.
		Requires the arrival time of the third IFO to do so.
		"""

		if isinstance(t3, (float, int, LIGOTimeGPS)):
			# float ensures conversion from LIGOTimeGPS
			# double array required to maintain compatibility with the case where t3 is an array
			t123 = numpy.array([[float(self.time1), float(self.time2), float(t3)]])
		else:
			# assume array of t3 times to check
			t123 = numpy.array([[float(self.time1), float(self.time2), float(_t3)] for _t3 in t3])

		# construct time vector array tau
		tbar = t123.copy()
		tbar = numpy.dot(tbar, numpy.array([1 / self.s1**2, 1 / self.s2**2, 1 / self.s3**2]))
		tbar /= 1 / self.s1**2 + 1 / self.s2**2 + 1 / self.s3**2
		tau = t123.copy()
		for i, s in enumerate([self.s1, self.s2, self.s3]):
			tau[:,i] = (tau[:,i] - tbar) / s

		# compute perpendicular component of direction vector and its norm
		nperp = numpy.dot(self.Mpinv, tau.T)
		norms = numpy.linalg.norm(nperp, axis=0)

		# where the norm is greater than 1, make it 1
		nperp[:,norms > 1] /= norms[norms > 1]

		# compute chisq
		chisq = numpy.linalg.norm(numpy.dot(self.M, nperp) - tau.T, axis=0)**2

		# return chisq array or array of coinc bools based on threshold
		if thresh:
			return chisq < thresh
		else:
			return chisq

	@classmethod
	def position(cls, ifo):
		'''Returns the position vector for a specified IFO, e.g., "H1".'''
		return cls._detector_locations[ifo]

class Sample(object):
	"""
	A class to store a single CBC GW trigger 
	"""

	@initializer
	def __init__(self, params, wfd, g, snr_time_phase, inspiral_extrinsics, horizon, min_instruments, far = 1., calculate_chisq = True, instrument_override = None, seed_xmldoc = None):
		"""
		params              : intrinsic parameters dictionary, e.g., {"m1": 1.4, "m2": 1.4, ...}
		wfd                 : a dictionary of template waveforms
		g                   : the metric tensor (matrix) evaluated at params
		snr_time_phase      : a SNRTimePhase instance dictionary by ifo
		inspiral_extrinsics : an InspiralExtrinsics instance
		horizon             : dictionary of horizon distances
		min_instruments     : the minimum number of instruments to consider
		far                 : false alarm rate in Hz
		calculate_chisq     : whether to do chisq calculation (chisq is not required if L is not being calculated by snr_time_phase)
		instrument_override : the ifos you don't want to process. They will be taken directly from the seed_xmldoc
		"""

		if instrument_override and not seed_xmldoc:
			raise ValueError("A seed xmldoc is needed when instrument_override is set")

		self.chirpmass = chirpmass(params)
		self.chi = computechi(self.params['m1'], self.params['m2'], self.params['S1z'], self.params['S2z'])
		self.fmerg = spawaveform.imrffinal(self.params['m1'], self.params['m2'], self.chi, 'merger')
		self.ifoset = set(self.wfd.keys())

		# FIXME p_of_most_sensitive_ifo, largest_hd calculation removed
		# from here and coinc_LR. Put it back in if we want to calculate L

		maximize_L = all([snrtp.maximize_L for _, snrtp in self.snr_time_phase.items()])

		# some sets of ifos useful for the optimizer's add-skymap-mode,
		# in which e.g. we want to take the H and L triggers directly
		# from the seed_xmldoc, and only filter V
		ifos_to_be_filtered = self.ifoset - set(instrument_override) if instrument_override else self.ifoset

		# get initial single IFO triggers
		sngl_sample_inputs = {ifo: self.snr_time_phase[ifo].SNR_and_AC(self.wfd[ifo], self.params, calculate_ac = self.calculate_chisq) for ifo in ifos_to_be_filtered}
		initial_sngl_sample = {ifo: self.snr_time_phase[ifo].trigger(*sngl_sample_inputs[ifo], calculate_chisq = self.calculate_chisq) for ifo in ifos_to_be_filtered}

		# get the necessary data to make SnglSamples from the seed_xmldoc
		if instrument_override:
			rhos, tcs, chisqs, all_snr_series, phases, chisqdofs, event_ids = {}, {}, {}, {}, {}, {}, {}
			snglrows = lsctables.SnglInspiralTable.get_table(seed_xmldoc)
			for snglrow in snglrows:
				event_ids[snglrow.ifo] = snglrow.event_id
				rhos[snglrow.ifo] = snglrow.snr
				tcs[snglrow.ifo] = numpy.float128(snglrow.end_time + 1e-9 * snglrow.end_time_ns)
				chisqs[snglrow.ifo] = snglrow.chisq
				phases[snglrow.ifo] = snglrow.coa_phase
				chisqdofs[snglrow.ifo] = snglrow.chisq_dof
			all_snr_series_by_eventid = dict((ligolw_param.get_pyvalue(elem, u'event_id'), series.parse_COMPLEX8TimeSeries(elem)) for elem in seed_xmldoc.getElementsByTagName(ligolw.LIGO_LW.tagName) if elem.hasAttribute(u'Name') and elem.Name == u'COMPLEX8TimeSeries')
			all_snr_series = {}
			for ifo, event_id in event_ids.items():
				all_snr_series[ifo] = all_snr_series_by_eventid[event_id]

			# we want to only override ifos we have data for, from the seed_xmldoc
			instrument_override = set(instrument_override).intersection(set(all_snr_series))

			for ifo in instrument_override:
				initial_sngl_sample[ifo] = SnglSample(0., rhos[ifo], tcs[ifo], phases[ifo], chisqs[ifo], chisqdofs[ifo], 0, all_snr_series[ifo])

		# construct a list of ifos by descending snr. While forming coincs,
		# higher snr ifos are added to the coinc first, ensuring less strict
		# time constraints for them, as compared to lower snr ifos.
		ifo_snrs = [(sample.snr, ifo) for ifo, sample in initial_sngl_sample.items()]
		descending_snr_ifos = [ifo for _, ifo in sorted(ifo_snrs, reverse = True)]

		best = []
		# To form coincs, we start off with a detector, i.e. `ifo`, and try
		# to add other detectors to it, i.e. `otherifo`.
		# Explained in further detail in https://git.ligo.org/gstlal/o4-code-review/-/issues/51
		for n, ifo in enumerate(self.ifoset):
			this_sngl_sample = {ifo: initial_sngl_sample[ifo]} if not instrument_override else {_ifo: initial_sngl_sample[_ifo] for _ifo in instrument_override}# ifo:sngl_sample that have been successfully added to this coinc
			# if an instrument override is provided, this_sngl_sample
			# will always start off as the sngl_sample of the overriden
			# ifo. Otherwise, it will start off as the sngl_sample of
			# the detector we are looking at, i.e. `ifo`

			for otherifo in descending_snr_ifos:
				# try to add otherifo to the trigger given in this_sngl_sample
				if otherifo not in self.ifoset - set(this_sngl_sample) - set((ifo,)):
					# not an ifo that needs to be added to this coinc
					continue

				# create a ThreeDetectorCoincidence instance initialized with the
				# 3 ifos and with the times of the first two ifos, only when trying
				# to add the third ifo to the coinc. This is done to determine which
				# time the third ifo can have so that the coinc has a physical sky location
				ifo_time_dict = {ifo: sample.time for ifo, sample in this_sngl_sample.items()}
				tdc = ThreeDetectorCoincidence(ifo_time_dict, otherifo) if len(self.ifoset) == 3 and len(this_sngl_sample) == 2 else None

				coinc_segment = ligolw_segments.segments.segment(ligolw_segments.segments.NegInfinity, ligolw_segments.segments.PosInfinity)
				for _ifo, _sample in this_sngl_sample.items():
					coincidence_window = LIGOTimeGPS(light_travel_time(_ifo, otherifo)) #+ 0.005 # fudge factor
					try:
						coinc_segment &= ligolw_segments.segments.segment(_sample.time - coincidence_window, _sample.time + coincidence_window)
					except ValueError:
						# no time range possible for this ifo
						# don't add it to the coinc
						coinc_segment = None
						break

				if coinc_segment is not None:
					# add it to the coinc
					this_sngl_sample[otherifo] = self.snr_time_phase[otherifo].trigger(*sngl_sample_inputs[otherifo], segment = coinc_segment, calculate_chisq = self.calculate_chisq, tdc = tdc)

			time = {i: this_sngl_sample[i].time for i in this_sngl_sample}
			phase = {i: this_sngl_sample[i].phase for i in this_sngl_sample}
			snr = {i: this_sngl_sample[i].snr for i in this_sngl_sample}
			sngl_L = {i: this_sngl_sample[i].L for i in this_sngl_sample}
			maxL, maxSNR, maxcombo, L_details = coinc_LR(inspiral_extrinsics, TimePhaseSNR.instrument_combos(this_sngl_sample.keys(), min_instruments = min_instruments), sngl_L, time, phase, snr, horizon, maximize_L = maximize_L)
			best.append((maxSNR, maxL, n, {k: this_sngl_sample[k] for k in maxcombo}, L_details))

		maxbest = max(best)
		self.snr = maxbest[0]
		self.L = maxbest[1]
		self.sngl_sample = maxbest[3]
		self.L_details = maxbest[4]

	def to_dict(self):
		"""
		Write out the sample as a dictionary suitable for json encoding
		"""
		out = {
			"params" : self.params,
			"g" : self.g.tolist(),
			"snr" : float(self.snr),
			"L": float(self.L),
			"far": float(self.far),
			"fmerg": float(self.fmerg),
			"chi": float(self.chi),
			}
		out['sngls'] = {ifo:self.sngl_sample[ifo].to_dict() for ifo in self.sngl_sample}
		out['L_details'] = self.L_details
		return out

	def to_xml_file(self, channel_dict, name = None, likelihood = 0, combined_far = 0, fap = 0, psd = None):
		# variables we will need
		ifos = ",".join(sorted(self.ifoset))
		instruments = ",".join(sorted(self.sngl_sample.keys()))

		# Make an output document
		xmldoc = ligolw.Document()
		lw = xmldoc.appendChild(ligolw.LIGO_LW())
		sngl_inspiral_table = lsctables.New(lsctables.SnglInspiralTable)
		coinc_inspiral_table = lsctables.New(lsctables.CoincInspiralTable)
		coinc_event_table = lsctables.New(lsctables.CoincTable)
		coinc_def_table = lsctables.New(lsctables.CoincDefTable)
		coinc_map_table = lsctables.New(lsctables.CoincMapTable)
		time_slide_table = lsctables.New(lsctables.TimeSlideTable)
		lw.appendChild(sngl_inspiral_table)
		lw.appendChild(coinc_inspiral_table)
		lw.appendChild(coinc_event_table)
		lw.appendChild(coinc_def_table)
		lw.appendChild(coinc_map_table)
		lw.appendChild(time_slide_table)


		# store the process params
		paramdict = {} # FIXME
		process = ligolw_utils.process.register_to_xmldoc(xmldoc, program = "gstlal_inspiral", paramdict = paramdict, comment = "SNR Optimizer")
		# populate single inspiral table
		event_ids = {}
		for n, (ifo, data) in enumerate(self.sngl_sample.items()):
			rdict = {k:0 for k in ("process_id", "ifo", "search", "channel", "end_time", "end_time_ns", "end_time_gmst", "impulse_time", "impulse_time_ns", "template_duration", "event_duration", "amplitude", "eff_distance", "coa_phase", "mass1", "mass2", "mchirp", "mtotal", "eta", "kappa", "chi", "tau0", "tau2", "tau3", "tau4", "tau5", "ttotal", "psi0", "psi3", "alpha", "alpha1", "alpha2", "alpha3", "alpha4", "alpha5", "alpha6", "beta", "f_final", "snr", "chisq", "chisq_dof", "bank_chisq", "bank_chisq_dof", "cont_chisq", "cont_chisq_dof", "sigmasq", "rsqveto_duration", "Gamma0", "Gamma1", "Gamma2", "Gamma3", "Gamma4", "Gamma5", "Gamma6", "Gamma7", "Gamma8", "Gamma9", "spin1x", "spin1y", "spin1z", "spin2x", "spin2y", "spin2z", "event_id")}
			rdict.update({"ifo":ifo, "channel": channel_dict[ifo], "process_id":process.process_id, "snr": data.snr, "chisq": data.chisq, "chisq_dof": 1, "mchirp": self.chirpmass, "mtotal": (self.params['m1']+self.params['m2']), "eta": (self.params['m1'] * self.params['m2']) / (self.params['m1'] + self.params['m2'])**2, "coa_phase": data.phase, "mass1":self.params['m1'], "mass2":self.params['m2'], "spin1z":self.params['S1z'], "spin2z":self.params['S2z'], "end_time": int(data.time), "end_time_ns": (data.time%1)*1e9})
			row = lsctables.SnglInspiralTable.RowType(**rdict)
			event_ids[ifo] = n
			row.event_id = row.template_id = event_ids[ifo]
			sngl_inspiral_table.append(row)

		# Coinc definer
		rdict = {"coinc_def_id": 0, "description": thinca.InspiralCoincDef.description, "search": thinca.InspiralCoincDef.search, "search_coinc_type": thinca.InspiralCoincDef.search_coinc_type}
		coinc_def_table.append(lsctables.CoincDefTable.RowType(**rdict))

		# Coinc event
		rdict = {"coinc_def_id": 0, "coinc_event_id": 0, "instruments":instruments, "likelihood":likelihood, "nevents": len(sngl_inspiral_table), "process_id": process.process_id, "time_slide_id": 0}
		coinc_event_table.append(lsctables.CoincTable.RowType(**rdict))

		# Coinc inspiral table
		rdict = {"coinc_event_id": 0, "combined_far": combined_far, "end_time":0, "end_time_ns":0, "false_alarm_rate": fap, "ifos": ifos, "mass":(self.params['m1'] + self.params['m2']), "mchirp":self.chirpmass, "minimum_duration": 0, "snr":self.snr}
		row = lsctables.CoincInspiralTable.RowType(**rdict)
		# time of first in alphabetical order by convention
		row.end = self.sngl_sample[sorted(self.sngl_sample)[0]].time
		coinc_inspiral_table.append(row)

		# Coinc Event Map
		for row in sngl_inspiral_table:
			rdict = {"coinc_event_id": 0, "event_id": row.event_id, "table_name": "sngl_inspiral"}
			coinc_map_table.append(lsctables.CoincMapTable.RowType(**rdict))

		# Time Slide
		for ifo in ("H1","L1","V1"):
			rdict = {"instrument": ifo, "offset": 0, "process_id": process.process_id, "time_slide_id":0}
			time_slide_table.append(lsctables.TimeSlideTable.RowType(**rdict))

		# SNR time series
		for ifo, sngl_sample in self.sngl_sample.items():
			snr_time_series_element = lal.series.build_COMPLEX8TimeSeries(sngl_sample.snr_series)
			snr_time_series_element.appendChild(ligolw_param.Param.from_pyvalue(u"event_id", event_ids[ifo]))
			xmldoc.childNodes[-1].appendChild(snr_time_series_element)

		# PSD
		if psd is not None:
			# add psd frequeny series
			lal.series.make_psd_xmldoc(psd, xmldoc.childNodes[-1])

		if name is None:
			# return the xml object instead of savinf the xml file
			return xmldoc

		# else write out the file
		ligolw_utils.write_filename(xmldoc, name, trap_signals = None)


	def __repr__(self):
		d = self.to_dict()
		return json.dumps(d, sort_keys=True, indent=4, cls=io.JSONEncoder)

	def compact(self, level = 1, max_level = 1):
		rho = u'\u03c1'; m1 = u'm\u2081'; m2 = u'm\u2082'; s1 = u'S\u2081'; s2 = u'S\u2082'
		rhos = {"H1": u'\u03c1\u2095', "L1": u'\u03c1\u2097', "V1": u'\u03c1\u1D65', "K1": u'\u03c1\u2096'}
		chisqs = {"H1": u'\u03C7\u00B2\u2095', "L1": u'\u03C7\u00B2\u2097', "V1": u'\u03C7\u00B2\u1D65', "K1": u'\u03C7\u00B2\u2096'}
		s = u'[{:2}/{:2}] {:>3}:{:<7} {:>3}:{:<7} {:>3}:{:<7} {:>3}:{:<14} {:>3}:{:<7} {:>3}:{:<7} {:>3}:{:<7} {:>3}:{:<7}'.format(level, max_level, u'L', round(self.L, 1) if self.L is not None else 'None', rho, round(self.snr, 2), 'fm', round(self.fmerg, 0), u't', round(float(self.sngl_sample[sorted(self.sngl_sample)[0]].time),3), m1, round(self.params["m1"], 2), m2, round(self.params["m2"], 2), s1, round(self.params["S1z"], 2), s2, round(self.params["S2z"], 2))
		for ifo in sorted(self.sngl_sample):
			s += u'{:>3}:{:<7} {:>3}:{:<7}'.format(rhos[ifo], round(float(self.sngl_sample[ifo].snr), 2), chisqs[ifo], round(float(self.sngl_sample[ifo].chisq), 2) if self.sngl_sample[ifo].chisq is not None else 'None')
		return s
		

class R4PolyMatch(object):

	# class variables to share planning - not thread safe
	fpl = {}
	rpl = {}

	@initializer
	def __init__(self, inp, outdur = 3):
		# store parameters associated with input data
		self.inrate = int(1. / self.inp.deltaT)

		# make sure the sample rate is a power of 2
		assert (not (self.inrate % 2))

		# A dictionary to store data keyed by (sample rate, duration) as integers
		self.d = {}

		#print self.mxdur, self.mndur, self.outdur

	def __match(self, t, delta):
		t, _ = t # t is actually h+ and hx but hx isn't used right now and is set to None
		flen = t.data.length # frequency domain vector length
		tlen = (flen - 1) * 2 # time domain vector length
		rate = int(t.deltaF * tlen) # sample rate as an integer
		assert (not(rate % 2))
		deltaT = 1. / rate
		dur = int(1. / t.deltaF) # duration in seconds as an integer
		assert (not(dur % 2))
		# number of samples in the output
		nsamps = int(self.outdur * rate)

		# extract or create the workspace variables
		wspace = c8f(flen, self.d[(rate, dur)].epoch, self.d[(rate, dur)].deltaF)

		# calculate match
		wspace.data.data[:] = numpy.conj(t.data.data) * self.d[(rate, dur)].data.data
		tseries = self.revfft(wspace)
		mepoch = tseries.epoch + LIGOTimeGPS(dur - self.outdur)
		match = r4t(numpy.roll(tseries.data.data, -int(round(float(delta * rate))))[-nsamps:], mepoch, deltaT)

		# calculate the orthogonal phase match
		wspace.data.data[:] = numpy.conj(-1.j * t.data.data) * self.d[(rate, dur)].data.data
		tseries = self.revfft(wspace)
		mepoch = tseries.epoch + LIGOTimeGPS(dur - self.outdur)
		imatch = r4t(numpy.roll(tseries.data.data, -int(round(float(delta * rate))))[-nsamps:], mepoch, deltaT)

		return match, imatch

	def __actseries(self, t, ac_half_len_samps = 1024):
		t, _ = t # t is actually h+ and hx but hx isn't used right now and is set to None
		flen = t.data.length # frequency domain vector length
		tlen = (flen - 1) * 2 # time domain vector length
		rate = int(t.deltaF * tlen) # sample rate as an integer
		assert (not(rate % 2))
		dur = int(1. / t.deltaF) # duration in seconds as an integer
		assert (not(dur % 2))

		# Create the workspace variables
		wspace = c8f(flen, 0, t.deltaF)

		# calculate match
		wspace.data.data[:] = numpy.conj(t.data.data) * t.data.data
		tseries = self.revfft(wspace)
		acr = numpy.roll(tseries.data.data, ac_half_len_samps)[:2 * ac_half_len_samps+1]

		# calculate the orthogonal phase match
		wspace.data.data[:] = numpy.conj(-1.j * t.data.data) * t.data.data
		tseries = self.revfft(wspace)
		aci = numpy.roll(tseries.data.data, ac_half_len_samps)[:2 * ac_half_len_samps+1]


		return acr, aci

	def fwdfft(self, td):
		tl = td.data.length
		fl = tl // 2 + 1
		# Take the FFT of the input data and normalize it to be a unitary transformation
		fd = c8f(fl, td.epoch, 1. / (tl * td.deltaT))
		if tl not in self.fpl:
			self.fpl[tl] = lal.CreateForwardREAL4FFTPlan(tl, 1)
		lal.REAL4TimeFreqFFT(fd, td, self.fpl[tl])
		fd.data.data[:] = fd.data.data[:] * (fl)**.5
		return fd

	def revfft(self, fd):
		fl = fd.data.length
		tl = (fl - 1) * 2
		# Take the reverse FFT of the input data
		td = r4t(tl, fd.epoch, 1. / (tl * fd.deltaF))
		if tl not in self.rpl:
			self.rpl[tl] = lal.CreateReverseREAL4FFTPlan(tl, 1)
		lal.REAL4FreqTimeFFT(td, fd, self.rpl[tl])
		return td

	def __call__(self, wf):
		for (f,d) in wf:
			assert (type(f) == int and type(d) == int and not(f % 2) and not (d % 2))

		# just in time creation of the requested data in the frequency domain for the given sample rate and duration
		self.d.update({(f,d): self.fwdfft(rs2r4t(nipr4t(self.inp, d), f)) for (f,d) in wf if (f,d) not in self.d})

		# get the max rate waveform
		nfd = [(f,d) for (f,d) in wf if f == self.inrate] # pull out the nyquist waveform
		assert (len(nfd) == 1)# needs to be exactly one waveform at the max sample rate, that is all that is required
		(nf,nd) = nfd[0]

		# This is the amount we shift all SNR time series NOTE only uses h+ FIXME
		delta = wf[(nf,nd)][0].epoch + int(1./wf[(nf,nd)][0].deltaF)
		if not (delta >= 0 and delta <= 4.0): # Why is this so big, and is it really a problem???? FIXME FIXME FIXME
			raise ValueError(delta)
		# get min rate
		minrate = min([f for (f,d) in wf])
		#calculate the nearest exact sample delta
		nearestsampledelta = int(delta * minrate) / float(minrate)

		# Calculate the max rate match
		match, imatch = self.__match(wf[(nf,nd)], nearestsampledelta)

		# get the rest if they exist
		for (f,d) in wf:
			if (f,d) == (nf,nd): continue
			_match, _imatch = self.__match(wf[(f,d)], nearestsampledelta)
			match.data.data[:] += rs2r4t(_match, self.inrate).data.data[:]
			imatch.data.data[:] += rs2r4t(_imatch, self.inrate).data.data[:]

		return match, imatch, delta - nearestsampledelta

	# merge code with match
	def acorr(self, wf, ac_half_length):

		# FIXME - I don't really know why this is necessary, there is
		# still an overall phase shift in the waveforms, but, the SNRs
		# and chisqs work after these fixes
		def fix_phase(z, ix):
			return z * numpy.exp(-1.j * numpy.angle(z[ix]))

		for (f,d) in wf:
			assert (type(f) == int and type(d) == int and not(f % 2) and not (d % 2))
		assert (not ac_half_length % 2)

		# get the nyquist rate waveform match
		nfd = [(f,d) for (f,d) in wf if f == self.inrate] # pull out the nyquist waveform
		assert (len(nfd) == 1)# needs to be exactly one waveform at the max sample rate, that is all that is required
		(nf,nd) = nfd[0]
		nl = wf[(nf,nd)][0].data.length

		# Get the highest sample rate version of A
		match, imatch = self.__actseries(wf[(nf,nd)], ac_half_length*2)
		target_len = len(match)
		match = match[ac_half_length:-ac_half_length]
		imatch = imatch[ac_half_length:-ac_half_length]
		A = numpy.zeros(ac_half_length * 2 + 1, numpy.complex)
		A = fix_phase(match + 1.j * imatch, ac_half_length)

		# get the rest if they exist
		for (f,d) in wf:
			if (f,d) == (nf,nd): continue
			lr = float(wf[f,d][0].data.length) / nl
			_match, _imatch = self.__actseries(wf[(f,d)], ac_half_length * 2 * f // nf)
			A += fix_phase(resample(_match, target_len)[ac_half_length:-ac_half_length] * lr + 1.j * resample(_imatch, target_len)[ac_half_length:-ac_half_length] * lr, ac_half_length)

		return numpy.real(A), numpy.imag(A)

class SNRTimePhase(object):
	"""
	Draw from the extrinsic parameters SNR time and phase
	"""
	@initializer
	def __init__(self, splusns_data, nsamps = 8192, durations = (128,64,32,16,8,4,2,1), pad_samps = 2048, ac_half_length = 1024, target_tc = None, maximize_L = False):
		"""
		splusns_data : the time domain data containing the stationary + nonstationary part. By convention, the signal is assumed to coalesce in the last nsamps
		nsamps  : only the last nsamps will be searched for coalescence
		"""
		aip2(self.durations) # make sure durations are integer powers of 2
		assert self.pad_samps >= self.ac_half_length
		self.r4fft = r4fftclass()

		logging.info("received the following time domain data %s " % print_tseries(self.splusns_data))
		logging.info("requested searching over the last %d samples with FFT durations of %s" % (self.nsamps - 2 * self.pad_samps, self.durations))

		#FIXME something could go wrong if sample rates are changing between instances
		self.fs = int(1. / self.splusns_data.deltaT)

		_data_splusns = self.splusns_data.data.data

		assert self.splusns_data.data.length * self.splusns_data.deltaT >= max(self.durations)

		self.splusns_polymatch = R4PolyMatch(r4t(_data_splusns, self.splusns_data.epoch, self.splusns_data.deltaT), outdur = int(self.nsamps * self.splusns_data.deltaT))

	def __traditional_autochisq(self, SNR, A, ix):
		l = 300 #override the default - large half length with this. FIXME make it template dependent like we do in GstLAL
		A = A[len(A) // 2 -l: len(A) // 2 +l +1]
		phase = numpy.angle(SNR[ix])
		SNR = SNR[ix-l:ix+l+1] * numpy.exp(-1.j * phase)
		chi = SNR - numpy.real(A) * max(abs(numpy.real(SNR))) - 1.j * numpy.imag(A) * max(abs(numpy.imag(SNR)))
		norm = 2 * len(SNR) - numpy.sum(numpy.conj(A) * A)
		return numpy.real(numpy.sum(numpy.conj(chi) * chi) / norm), phase, len(A)

	def __autocorr(self, template, params):
		# compute the autocorrelation
		a, ai = self.splusns_polymatch.acorr(template, self.ac_half_length)
		a /= abs(a).max()
		ai /= abs(ai).max()
		return a + 1.j * ai, len(a)

	# FIXME this function is totally made up
	def sngl_lr(self, xi2, snr):
		"""
		Return the approximate likelihood ratio (lr) for a single detector candidate

		xi2        : is the autochisq / dof (so expectation value is 1)
		snr        : is the absolute value of the matched filter snr
		indicative of a glitch origin, but could also just mean that a glitch is
		present during a real signal. Right now this is not used.
		"""
		x = max(0, xi2 - 1.0 - 0.0005 * snr**2)
		return snr**2 * numpy.exp(-4.0 * x**2) / 2. + (-4.0 * x**2)

	def SNR_and_AC(self, template, params, calculate_ac = True):
		# get the final SNR and peak
		# compute the stationary + non-stationary matches
		if self.maximize_L and not calculate_ac:
			calculate_ac = True # can't calculate L without chisq
		splusns_match, splusns_imatch, delta = self.splusns_polymatch(template)
		splusnsSNR_r = splusns_match.data.data
		splusnsSNR_i = splusns_imatch.data.data
		SNR = splusnsSNR_r  + 1.j * (splusnsSNR_i)
		epoch, deltaT = splusns_match.epoch, splusns_match.deltaT

		# compute the templates autocorrelation
		(A, ac_len) = self.__autocorr(template, params) if calculate_ac else (None, None)
		return SNR, delta, epoch, deltaT, A

	def trigger(self, SNR, delta, epoch, deltaT, A, segment = None, calculate_chisq = False, tdc = None):
		if self.maximize_L and not calculate_chisq:
                        calculate_chisq = True # can't calculate L without chisq
		# if requested search a narrow gps time
		if segment is not None:
			ix_lo = int(round(float(segment[0] - epoch + delta) / deltaT))
			ix_hi = int(round(float(segment[1] - epoch + delta) / deltaT))
			if tdc is None:
				ix = numpy.argmax(abs(SNR)[ix_lo: ix_hi]) + ix_lo
			else:
				# find the index with the highest snr such that it forms a 'good'
				# coinc, according to the ThreeDetectorCoincidence class
				available_snrs = abs(SNR)[ix_lo: ix_hi]
				times = numpy.array(epoch + numpy.array(range(ix_lo, ix_hi)) * deltaT - delta) # times corresponsing to the snrs calculated above
				for _, time, ix in sorted(zip(available_snrs, times, range(ix_lo, ix_lo + len(times))), reverse = True):
					if tdc(time, 3):
						break

		else:
			if tdc is None:
				# Ignore the pad_samps at beginning and end
				ix = numpy.argmax(abs(SNR)[self.pad_samps:-self.pad_samps]) + self.pad_samps
			else:
				# find the index with the highest snr such that it forms a 'good'
				# coinc, according to the ThreeDetectorCoincidence class
				available_snrs = abs(SNR)[self.pad_samps:-self.pad_samps]
				times = numpy.array(epoch + numpy.array(range(self.pad_samps, len(SNR) -self.pad_samps)) * deltaT - delta) # times corresponsing to the snrs calculated above
				found_good_coinc = False
				for _, time, ix in sorted(zip(available_snrs, times, range(self.pad_samps, self.pad_samps + len(times))), reverse = True):
					if tdc(time, 3):
						break

		# Compute the autochisq and phase at the trigger peak if requested
		chisq, phase, chisqdof = self.__traditional_autochisq(SNR, A, ix) if calculate_chisq else (None, None, None)

		# get the SNR, coalescence time and LR
		rho = abs(SNR[ix])
		tc = epoch + LIGOTimeGPS(ix * deltaT) - delta

		# record the SNR time series around peak
		snr_series_six = ix - self.ac_half_length
		snr_series_eix = ix + self.ac_half_length + 1
		snr_series_epoch = epoch + LIGOTimeGPS(snr_series_six * deltaT) - delta
		snr_series = c8t(SNR[snr_series_six:snr_series_eix], snr_series_epoch, deltaT, name = "snr")

		# FIXME have this take chisqdof too and maybe do some penalty?
		lr = self.sngl_lr(chisq, rho) if self.maximize_L else None

		return SnglSample(lr, rho, tc, phase, chisq, chisqdof, 0, snr_series)

	def __call__(self, template, params):
		"""
		Take a frequency domain template and return the maximum snr
		time and phase. 
		"""

		SNR, delta, epoch, deltaT, A = self.SNR_and_AC(template, params)
		return self.trigger(SNR, delta, epoch, deltaT, A)


class MASSRectangle(object):
	"""
	A class to calculate the SNR and L at the center of a ManifoldRectangle
	"""
	@initializer
	def __init__(self, manifold_rectangle, original_rectangle, white_template, SnrTPSample, IE, horizons, min_instruments, calculate_chisq = True, instrument_override = None, seed_xmldoc = None):
		params = self.manifold_rectangle.metric.coord_func(self.manifold_rectangle.center)
		self.params = {'m1': params[M1], 'm2': params[M2], 'S1z': params[S1Z], 'S2z': params[S2Z]}
		self.wfd, self.params = self.white_template(**self.params)
		self.sample = Sample(self.params, self.wfd, self.manifold_rectangle.g, SnrTPSample, self.IE, horizon = self.horizons, min_instruments = self.min_instruments, calculate_chisq = self.calculate_chisq, instrument_override = instrument_override, seed_xmldoc = seed_xmldoc)

class RandomGaussian(object):
	"""
	A class to calculate the SNR and L centered on some mismatch gaussian
	"""
	@initializer
	def __init__(self, center, coord_func, g, white_template, SnrTPSample, IE, horizons, min_instruments):
		pass
	def __call__(self, mismatch = 0.05, at_center = False):
		if at_center:
			params = self.coord_func(self.center)
			center = self.center
		else:
			params = next(random_point_at_mismatch(self.g, self.center, self.coord_func, mismatch = mismatch, scale = mismatch)) #scale = mismatch is an arbitrary guess at a good way to do this
			center = self.coord_func(params, inv = True)
		params = {'m1': params[M1], 'm2': params[M2], 'S1z': params[S1Z], 'S2z': params[S2Z]}
		wfd, params = self.white_template(**params)
		snr_time_phase = {}
		return Sample(params, wfd, self.g, self.SnrTPSample, self.IE, horizon = self.horizons, min_instruments = self.min_instruments), params, center

class Likelihood(object):
	"""
	A class to return maximum likelihood template found in a block of data
	"""
	@initializer
	def __init__(self,
			splusns_data,
			rectangles,
			event,
                        psd,
			window_start,
			flow = 10.,
			min_mismatch = 0.0001,
			durations = (128.,64.,32.,16.,8.,4.,1.),
			search_window = 4.,
			inspiral_extrinsics = None,
			min_instruments = 1,
			algorithm = "rectangle",
			timeout = 240,
			maximize_L = False,
			original_template_only = False,
			instrument_override = None
		):
		"""
		splusns_data           : a dictionary keyed by IFO of REAL8TimeSeries containing the stationary + non-stationary whitened strain data to analyze
		rectangles             : a manifold rectangles which start the sampling process
		event                  : a dictionary of event parameters
		psd                    : a dictionary of COMPLEX16FrequencySeries containing the power spectral density
		window_start           : The gps-time of the start of the window; recorded with bgsamples for time assignment during bg time-sliding
		flow                   : The starting frequency for templates
		min_mismatch           : The minimum mismatch to consider when dividing rectangles
		durations              : The different FFT lengths to consider. NOTE if the template is longer than this its flow will be changed to fit
		search_window          : time over which to identify peaks
                search_pad             : time before the end of the data to ignore SNR. Needed to compute autocorrelation chisq
                ac_half_length         : number of samples for one side of the autocorrelation, must be even
		min_instruments        : the minimum number of instruments to consider
		algorithm              : Either "rectangle" or "random"
		timeout                : stop collecting samples after this time
		maximize_L             : Set whether to (calculate and) maximize the MASS LR instead of SNR
		original_template_only : Only process original template, don't find better templates. This is meant to be used for mimicking gstlal's behavior
		instrument_override    : The ifos you don't want to process. They will be taken directly from the seed coinc
		"""

		# I can't promise stuff would work if you change these, so they are hardcoded
		self.ac_half_length = 1024
		self.search_pad = SEARCHPAD

		# FIXME Sanity check the input
		self.snr_duration = self.search_window + 2. * self.search_pad

		# delete the psds of ifos for which we don't have data so that the psd dict
		# can be used as a source of truth for the on detectors
		for ifo in set(psd.keys()) - set(splusns_data.keys()):
			del psd[ifo]
		assert set(psd.keys()) == set(splusns_data.keys()), f"psd and data dicts must have the same ifos as keys. psd has keys {set(psd.keys())}, whereas data has keys {set(splusns_data.keys())}"

		# get horizon distances
		if self.maximize_L:
			HD = HorizonDistance(self.flow, 1024., 0.25, 1.4, 1.4)
			self.horizons = {k: HD(psd[k])[0] for k in psd}
			most_sensitive = max([(v,k) for (k,v) in self.horizons.items() if k in splusns_data])[1]
		else:
			self.horizons, most_sensitive = None, None

		# get the first template
		# FIXME only uses h+ !!
		self.white_template = WhiteTemplate(psd, durations = self.durations, flow = self.flow, fs = int(1 / list(self.splusns_data.values())[0].deltaT), approximant = lalsim.GetApproximantFromString("IMRPhenomD"), pad = self.snr_duration)
		self.SNRTimePhase = {instrument: SNRTimePhase(self.splusns_data[instrument], nsamps = int(self.snr_duration / self.splusns_data[instrument].deltaT), durations = durations, pad_samps = int(self.search_pad / self.splusns_data[instrument].deltaT), ac_half_length = self.ac_half_length, maximize_L = self.maximize_L) for instrument in self.splusns_data}

		dimensions = len(rectangles[0].center)
		tvol = cover.tmpvol(self.min_mismatch, N = dimensions)
		# keep track of index of original rectangle, i, and add that tag to all fg samples, and also for bg histogram population
		rectangles = [(r, i) for i, r in enumerate(self.rectangles)]
		samples = []
		sample_rectangles = set()

		self.num_samples = len(rectangles) // 2
		start = time.time()

		#from cProfile import Profile
		#from pstats import Stats
		#prof = Profile()
		#prof.enable()
		if self.algorithm == "rectangle":
			if not self.original_template_only:
				while True:
					these_samples = []
					for number, (r, i) in enumerate(rectangles):
						if not number % 100: print ("processing %d/%d : %s" % (number, len(rectangles), r))
						massr = MASSRectangle(r, i, self.white_template, self.SNRTimePhase, self.inspiral_extrinsics, self.horizons, self.min_instruments, calculate_chisq = self.maximize_L)
						these_samples += [(massr.sample.snr, massr.manifold_rectangle, i)]
					samples += these_samples
					samples = sorted(samples, key = lambda x: x[0])[-self.num_samples:]
					sample_rectangles = sample_rectangles & set(f for _,f,_ in samples)
					rectangles = []
					for (_, manr, i) in samples:
						if manr not in sample_rectangles:
							sample_rectangles.add(manr)
							ntmps = manr.proper_volume / tvol
							if ntmps > 1:
								new_rectangles = manr.divide(reuse_g = True)
								rectangles += [(new_rectangle, i) for new_rectangle in new_rectangles]
					if len(rectangles) == 0:
						break
					if time.time() - start > self.timeout:
						logging.info("bailing out because time limit of %d seconds has been reached" % int(self.timeout))
						break
				max_sample = max(samples, key = lambda x: x[0])
				max_manr = max_sample[1]
				max_original_rectangle = max_sample[2]
				max_massr = MASSRectangle(max_manr, max_original_rectangle, self.white_template, self.SNRTimePhase, self.inspiral_extrinsics, self.horizons, self.min_instruments, calculate_chisq = True)

			else:
				assert len(rectangles) == 1, "Cannot give more than 1 input template when --original-template-only is set"
				max_massr = MASSRectangle(rectangles[0][0], rectangles[0][1], self.white_template, self.SNRTimePhase, self.inspiral_extrinsics, self.horizons, self.min_instruments, calculate_chisq = True, instrument_override = self.instrument_override, seed_xmldoc = self.event['coinc'] if self.instrument_override else None)

			out_sample = max_massr.sample	


		elif self.algorithm == "random":
			metric = rectangles[len(rectangles) // 2][0].metric # arbitrarily pick median
			initial_params = {x:event[x] for x in (M1,M2,S1Z,S2Z)}
			initial_center = metric.coord_func(initial_params, inv = True)
			initial_g = metric(initial_center)
			randG = RandomGaussian(initial_center, metric.coord_func, initial_g, self.white_template, self.SNRTimePhase, self.inspiral_extrinsics, self.horizons, self.min_instruments)
			mismatch = 10.0
			sample, par, center = randG(mismatch = mismatch, at_center = True)
			samples = [(sample.snr, sample)]
			cnt = 0
			while cnt < 5000:
				if time.time() - start > self.timeout:
					logging.info("bailing out because time limit of %d seconds has been reached" % int(self.timeout))
					break
				if cnt > 0 and not cnt % 100:
					mismatch = max(0.0005, mismatch * 0.75)
					logging.info("Checked %d samples, still no higher SNR. Setting mismatch to %f" % (cnt, mismatch))
				sample, par, center = randG(mismatch = mismatch)
				cnt += 1
				if sample.snr > samples[-1][0]:
					if sample.snr > 1.0001 * samples[-1][0]:
						cnt = 0
					randG = RandomGaussian(center, metric.coord_func, metric(center), self.white_template, self.SNRTimePhase, self.inspiral_extrinsics, self.horizons, self.min_instruments)
					logging.info("found higher SNR %f, setting mismatch to %f and cnt to %d. Elapsed time %d" % (sample.snr, mismatch, cnt, int(time.time() - start)))
					samples.append((sample.snr, sample))
			out_sample = max(samples, key = lambda x: x[0])[1]
		else:
			raise ValueError("algorithm must be random or rectangle")
		#prof.disable()
		#prof.print_stats(sort="time")
		logging.info("Elapsed time %d" % int(time.time() - start))
		logging.info(out_sample.compact())
		self.event = out_sample

	def __call__(self, reuse_metric = True):
		"""
		Draw a sample from the likelihood function. This returns an iterator, so you can loop over it.
		"""
		raise NotImplementedError

def aip2(s):
	"""
	function to assert that the input is an integer power of 2
	"""
	try:
		for x in s:
			assert (type(x) == int)
			assert (math.log2(x).is_integer())
	except TypeError:
		assert (type(s) == int and math.log2(s).is_integer())


class WhiteTemplate(object):
	@initializer
	def __init__(self, psd, durations = (128,64,32,16,8,4,2,1), flow = 15., fs = 2048, approximant = lalsim.GetApproximantFromString("IMRPhenomD"), pad = 1.):
		"""
		psd:         is a dictionary of REAL8FreqSeries
		durations:   must be power of two integers
		flow:        minimum freq to generate a waveform at
		fs:          maximum sample rate to generate a waveform at
		approximant: lal waveform approximant
		pad        : amount of output snr a template would be expected to produce, so the waveform must be shorter than the data by pad
		"""

		aip2(self.durations) # assert durations are integer powers of 2
		aip2(self.fs)
		self.fhigh = self.fs // 2
		self.psddict = {}
		for duration in self.durations:
			for ifo in psd:
				self.psddict[ifo, duration] = signal.interpolate_psd(self.psd[ifo], 1. / duration)


	def ts(self, p, fl, fh, mindur = 4, snr_duration = 3.):
		freqs = (0.5 * 2**numpy.arange(int(numpy.ceil(numpy.log2(fl))), int(numpy.log2(fh))+1)).astype(numpy.int)
		ts = freqs[:-1]
		te = freqs[1:]
		te[-1] = fh
		ts[0] = fl
		out = [(min(2 * fh, 4 * cp2(e)), max(mindur, cp2(snr_duration + chirptime.imr_time(s, p['m1'], p['m2'], p['S1z'], p['S2z']))), s, e) for s, e in zip(ts, te)]
		# remove duplicate durations
		tmp = {}
		for o in out:
			tmp.setdefault(o[1],[]).append(o)
		out = []
		for dur, ol in tmp.items():
			rate = max([l[0] for l in ol])
			s = min([l[2] for l in ol])
			e = max([l[3] for l in ol])
			out.append((rate, dur, s, e))
		return sorted(out)
 

	def __call__(self, m1 = 10.0, m2 = 10.0, S1x = 0., S1y = 0., S1z = 0., S2x = 0., S2y = 0., S2z = 0., distance = 1.e6 * lal.PC_SI, inclination = 0., phiRef = 0., longAscNodes = 0., eccentricity = 0., meanPerAno = 0., f_ref = 0., LALparams = None):
		parameters = locals()
		del parameters['self']
		if "chirpt" in parameters: del parameters["chirpt"]
		flow = self.flow
		parameters['f_ref'] = flow
		parameters['m1'] = lal.MSUN_SI * m1
		parameters['m2'] = lal.MSUN_SI * m2

		# Solve for a new higher flow if the template doesn't fit in the max duration
		__chirpt = chirptime.imr_time(flow, parameters['m1'], parameters['m2'], parameters['S1z'], parameters['S2z'])
		__duration = int(cp2(self.pad + __chirpt))
		while __duration > max(self.durations):
			flow += 1
			__chirpt = chirptime.imr_time(flow, parameters['m1'], parameters['m2'], parameters['S1z'], parameters['S2z'])
			__duration = int(cp2(self.pad + __chirpt))

		# for now just do the one duration until it reproduces the original result
		out = {ifo:{} for ifo in self.psd}
		end_points = {}
		slices = sorted(self.ts(parameters, flow, self.fhigh, mindur = min(self.durations), snr_duration = self.pad), reverse = True)
		maxdur = max(x[1] for x in slices)
		maxlen = self.fhigh * maxdur + 1
		for cnt, (rate, dur, fmn, fmx) in enumerate(slices):
			parameters['f_min'] = float(fmn) - 1 # we truncate this later, we just need the extra bit to compute phase offset
			parameters['f_max'] = float(fmx)
			parameters['approximant'] = self.approximant
			parameters['deltaF'] = 1. / dur
			hplus, hcross = lalsim.SimInspiralFD(**parameters)
			# Record the epoch of the longest template
			if dur == maxdur: maxepoch = hplus.epoch
			start, end = int(fmn / hplus.deltaF), int(fmx / hplus.deltaF)


			# Do the phase correction here cumulatively
			phasestart, phaseend = int((fmn-1) / hplus.deltaF), int((fmx-1) / hplus.deltaF)
			if rate < self.fs:
				# correct for the phase
				this_end = hplus.data.data[phaseend]
				prev_rate = [r for r in sorted(end_points) if r > rate][0]
				prev_start = end_points[prev_rate][0]
				if numpy.vdot(this_end, prev_start) == 0:
					theta = 0
				else:
					theta = numpy.angle(numpy.vdot(this_end, prev_start) / (numpy.vdot(this_end, this_end) * numpy.vdot(prev_start, prev_start))**.5)
				hplus.data.data[:] = hplus.data.data[:] * numpy.exp(1.j * theta)
			end_points[rate] = (hplus.data.data[phasestart], hplus.data.data[phaseend])

			# truncate to actual requested range
			hplus.data.data[:start] = 0.0  + 0.j
			hplus.data.data[end:] = 0.0 + 0.j
			# If we got back a frequency series that is too short, expand it.
			if hplus.data.length < rate // 2 * dur + 1:
				hplus = expandc16f(hplus, rate // 2 * dur + 1)

			norm = (float(self.fs) / rate)**.5
			hplus.data.data[:] = hplus.data.data[:] * norm

			for ifo in self.psd:
				hp = cpc16f(hplus)
				lal.WhitenCOMPLEX16FrequencySeries(hp, self.psddict[ifo, dur])
				out[ifo][int(rate),int(dur)] = (hp, None) #FIXME include cross some day

		# normalize the waveforms # FIXME include cross some day
		for ifo in out:
			norm = 0.
			for (rate, dur) in sorted(out[ifo]): # sort by sample rate
				norm += (abs(out[ifo][rate,dur][0].data.data[:])**2).sum() * (float(rate) / self.fs)
			for (rate, dur) in sorted(out[ifo], reverse = True): # sort by sample rate
				out[ifo][rate,dur][0].data.data[:] = out[ifo][rate,dur][0].data.data[:] / norm**.5

		parameters['m1'] /= lal.MSUN_SI
		parameters['m2'] /= lal.MSUN_SI
		parameters["chirpt"] = __chirpt # store the chirp time
		return (out, parameters)

def random_point_at_mismatch(g, center, coord_func, mismatch = 0.1, scale = 0.001):
	NUM = 100
	g = scale_g(g)

	mat = numpy.linalg.inv(g)

	L = numpy.linalg.cholesky(mat)
	while True:
		#for rp in coord_func(numpy.dot(numpy.random.randn(g.shape[0] * NUM).reshape(NUM, g.shape[0]) / g.shape[0]**.5 * mismatch**.5, L.T) + center):
		for rp in coord_func(numpy.dot(numpy.random.randn(g.shape[0] * NUM).reshape(NUM, g.shape[0]) / g.shape[0]**.5 * mismatch, L.T) + center):
			# make M1 biggest by convention
			if rp[M1] < rp[M2]:
				m1, s1z = rp[M1], rp[S1Z]
				rp[M1], rp[S1Z] = rp[M2], rp[S2Z]
				rp[M2], rp[S2Z] = m1, s1z
			if rp in coord_func:
				yield rp
