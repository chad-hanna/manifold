## How to make an O4 CBC template bank and mass model

The prefered way to install this code is to use a writable container

```
$ singularity build --sandbox --fix-perms gstlal-dev docker://containers.ligo.org/lscsoft/gstlal:master
```

Then enter the container and clone manifold

```
$ singularity run --writable gstlal-dev
Singularity> cd gstlal-dev
Singularity> mkdir src
Singularity> cd src
Singularity> git clone https://git.ligo.org/chad-hanna/manifold.git
Singularity> cd manifold/
Singularity> python3 setup.py install
```

You can then exit the container and in some other directory of your choice copy the contents from the examples directory. Edit the top line of the Makefile to point to the full path of your container, e.g., change 

```
SEXEC=<path to your container>
```

to

```
SEXEC=singularity exec /home/chad.hanna/manifold_svd_binning/gstlal-dev/
```

Then you can run make -j (assuming you have a beefy machine)

```
$ make -j
```

At the end, you will be left with the following files:

```
H1L1V1-O4_MANIFOLD_BANK-0-2000000000.h5       H1L1V1-O4_MANIFOLD_BANK_MASS_MODEL-0-2000000000.h5   O4_projected_psds.xml.gz  O4-test-03.h5  O4-test-06.h5  O4-test-09.h5  O4-test-12.h5  O4-test-15.h5  O4-test.png
H1L1V1-O4_MANIFOLD_BANK-0-2000000000.xml.gz   H1L1V1-O4_MANIFOLD_BANK_MASS_MODEL-0-2000000000.png  O4-test-01.h5             O4-test-04.h5  O4-test-07.h5  O4-test-10.h5  O4-test-13.h5  O4-test-16.h5
H1L1V1-O4_MANIFOLD_BANK_INIT-0-2000000000.h5  Makefile                                             O4-test-02.h5             O4-test-05.h5  O4-test-08.h5  O4-test-11.h5  O4-test-14.h5  O4-test.h5
```

The file ```H1L1V1-O4_MANIFOLD_BANK-0-2000000000.xml.gz``` should be suitable as a template bank for e.g., an GstLAL-Inspiral analysis along with a suitable mass model ```H1L1V1-O4_MANIFOLD_BANK_MASS_MODEL-0-2000000000.h5```. You can visualize a bank sim result by looking at ```O4-test.png``` and you can visualize the mass model by looking at H1L1V1-O4_MANIFOLD_BANK_MASS_MODEL-0-2000000000.png
