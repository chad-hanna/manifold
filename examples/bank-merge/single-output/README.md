# Example: Bank Merge (Single Output)

This example demonstrates how to use the `merge` function to merge two template banks.
Merging these banks in the "single output" mode will produce a new bank object "bank_all" with the following properties:

- Union: `bank_all.rectangles` will contain the union of the all input banks' rectangles.
- No Collision: No two rectangles in `bank_all` will have the same identifier.
- Preservation: The subset of templates in `bank_all` coming from the first input bank will have the same identifiers as they did in the first bank.

To run this example (from this directory) without the config file, run the following command:

```bash
manifold_cbc_bank_merge --input-banks inp1.h5 inp2.h5 inp3.h5 --output-bank bank_all.h5 --mode single
```

The below table summarizes the template identifier behavior in this example (note, the ids are chosen here arbitrarily to illustrate the above principles, but in practice will be randomly selected from a much larger range):

| Bank File   | Template IDs     |
|-------------|------------------|
| inp1.h5     | 0, 1             |
| inp2.h5     | 0, 1             |
| inp3.h5     | 0, 1             |
| bank_all.h5 | 0, 1, 2, 3, 5, 6 |
