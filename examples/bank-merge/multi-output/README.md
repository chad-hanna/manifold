# Example: Bank Merge (Multi Output)

This example demonstrates how to use the `merge` function to merge multiple template banks into separate files.
Merging these banks in the "multi output" mode will produce one new bank object per input bank object with the following properties:

- Disjoint Union: `bank_i.rectangles` will contain the original rectangles of the i-th input.
- No Collision: No two rectangles in any output banks will have the same identifier.
- Preservation: The templates in the first output bank will have the same identifiers as they did in the first bank.

To run this example (from this directory) without the config file, run the following command:

```bash
manifold_cbc_bank_merge --input-banks inp1.h5 inp2.h5 inp3.h5 --output-bank out1.h5 out2.h5 out3.h5 --mode multi
```

The below table summarizes the template identifier behavior in this example (note, the ids are chosen here arbitrarily to illustrate the above principles, but in practice will be randomly selected from a much larger range):

| Bank File | Template IDs |
|-----------|--------------|
| inp1.h5   | 0, 1         |
| inp2.h5   | 0, 1         |
| inp3.h5   | 0, 1         |
| out1.h5   | 0, 1         |
| out2.h5   | 2, 3         |
| out3.h5   | 4, 5         |
