import setuptools

with open("README.md", "r") as fh:
	long_description = fh.read()

setuptools.setup(
	name="manifold",
	version="0.0.1",
	author="Chad Hanna",
	author_email="crh184@psu.edu",
	description="Geometric tools for gravitational wave data analysis",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://git.ligo.org/chad-hanna/manifold",
	packages=setuptools.find_packages(),
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: GPLv2",
		"Operating System :: OS Independent",
	],
	scripts = ["bin/manifold_cbc_bank", "bin/manifold_cbc_bank_dag", "bin/manifold_cbc_bank_test_dag",
			   "bin/manifold_cbc_bank_add", "bin/manifold_cbc_bank_constrain", "bin/manifold_cbc_bank_test",
			   "bin/manifold_cbc_bank_plot", "bin/manifold_cbc_bank_test_add", "bin/manifold_cbc_bank_test_plot",
			   "bin/manifold_cbc_bank_group", "bin/manifold_cbc_bank_to_xml", "bin/manifold_cbc_bank_mass_model",
			   "bin/manifold_cbc_bank_plot_mass_model", "bin/manifold_cbc_bank_mass_model_add",
			   "bin/manifold_cbc_bank_mass_model_dag", "bin/manifold_cbc_bank_snr_optimizer_online",
			   "bin/manifold_cbc_bank_split", "bin/manifold_cbc_bank_merge", "bin/manifold_cbc_bank_profile",
			   "bin/manifold_cbc_bank_boundary", "bin/manifold_cbc_bank_boundary_grid", "bin/manifold_cbc_bank_regime"],
	python_requires='>=3.6',
)
