#!/usr/bin/env python3
import sys
import os
from optparse import OptionParser
import getpass
import logging
logging.basicConfig(level = logging.INFO)
import yaml

from manifold.bin.cbc import bank, group

from gstlal.dags import Argument, Option, DAG
from gstlal.dags.layers import Layer, Node

def mkdir(d):
    try:
        os.mkdir(d)
    except OSError:
        pass

p = OptionParser(usage="Usage: %prog [options] YAML")
p.add_option("", "--singularity-image", help = "Singularity image to use")
(opt, arg) = p.parse_args(sys.argv[1:])
assert (len(arg) == 1)


dag = DAG("manifold_cbc_bank_test")
dag.create_log_dir()


requirements = {
    "want_graceful_removal": "True",
    "kill_sig": "15",
    "accounting_group_user": getpass.getuser(),
    "accounting_group": "ligo.prod.o3.cbc.uber.gstlaloffline",
}

if opt.singularity_image:
	requirements['+SingularityImage'] = '"{}"'.format(opt.singularity_image)
	requirements['transfer_executable'] = False
	requirements['get_env'] = False
	requirements['requirements'] = "(HAS_SINGULARITY=?=True)"

with open(arg[0]) as f:
    config = yaml.safe_load(f)

test_layer = Layer("manifold_cbc_bank_test", requirements={"request_cpus": 1, "request_memory": 2000, **requirements},)
test_add_layer = Layer("manifold_cbc_bank_test_add", requirements={"request_cpus": 1, "request_memory": 2000, **requirements},)
test_plot_layer = Layer("manifold_cbc_bank_test_plot", requirements={"request_cpus": 1, "request_memory": 2000, **requirements},)

# setup directories
mkdir(config['test']['out-dir'])
mkdir(config['test-plot']['out-dir'])

test_files = []
for n in range(config['test']['num-jobs']):
    fn = os.path.join(config['test']['out-dir'], "%05d-test.h5" % n)
    test_files += [fn]
    test_layer += Node(arguments = Option("key", "test"), inputs = [Option("yaml", arg), Option("bank", config['constrain']['output-h5'], remap = True)], outputs = Option("output-h5", fn, remap = True))

test_add_layer += Node(arguments = Option("key", "test-add"), inputs = [Argument("tests", test_files, remap = True), Option("yaml", arg, remap = True)], outputs = Option("output-h5", config['test-add']['output-h5'], remap = True))
test_plot_layer += Node(arguments = Option("key", "test-plot"), inputs = [Argument("test", config['test-add']['output-h5'], remap = True), Option("yaml", arg, remap = True)])

dag.attach(test_layer)
dag.attach(test_add_layer)
dag.attach(test_plot_layer)

dag.write_dag("manifold_cbc_bank_test.dag")
dag.write_script("manifold_cbc_bank_test.sh")
