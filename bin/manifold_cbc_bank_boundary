#!/usr/bin/env python3
"""Script for computing bank constraint boundary quantities
"""

import argparse
import datetime

import matplotlib.pyplot as plt
import numpy
import yaml

from manifold import io
from manifold.sources import cbc


def parse_args() -> dict:
	"""Parse command line arguments"""
	parser = argparse.ArgumentParser(description="Profile a bank")
	parser.add_argument("-c", "--config", help="Specify the YAML file for configuration")
	parser.add_argument("-b", "--bank", help="Specify the bank file")
	parser.add_argument("-f", "--file", help="File to read for saved EBR results, will short-circuit computation if present")
	parser.add_argument("-o", "--output", help="Specify the output h5 file for ebr results")
	parser.add_argument("-v", "--verbose", action="store_true", help="Be verbose")
	parser.add_argument("-t", "--target-mismatch", type=float, help="Target mismatch for EBR computation")
	parser.add_argument("-n", "--n-points", type=int, help="Number of points for EBR computation")
	parser.add_argument("-r", "--truncate", type=int, help="If specified, truncate the bank to this many rectangles, used for testing")
	parser.add_argument("-m", "--trim", type=bool, help="If specified, trim the bank to this many rectangles, used for testing")
	parser.add_argument("-s", "--symmetric", type=bool, help="If specified use an object-symmetric constraint")
	args = parser.parse_args()

	#  Update config with command line arguments
	if args.config is not None:
		with open(args.config) as f:
			config = yaml.safe_load(f)
	else:
		config = {}

	if args.bank is not None:
		config['bank'] = args.bank

	if args.file is not None:
		config['file'] = args.file

	if args.output is not None:
		config['output'] = args.output

	if args.verbose:
		config['verbose'] = args.verbose

	if args.target_mismatch is not None:
		config['target-mismatch'] = args.target_mismatch

	if args.n_points is not None:
		config['n-points'] = args.n_points

	if args.truncate is not None:
		config['truncate'] = args.truncate

	if args.trim is not None:
		config['trim'] = args.trim

	return config


def compute_bank_ebr(bank: cbc.Bank, target_mismatch: float = 0.03, n_points: int = 100, verbose: bool = False, truncate: int = None, object_symmetric: bool = False) -> dict:
	"""Compute the effective bandwidth ratio for a bank

	Args:
		bank:
		target_mismatch:
		n_points:
		verbose:

	Returns:
		Dict, the results of the computation with keys: ebr, target_mismatch, n_points, elapsed
	"""
	print(f"Computing EBR for bank {bank} with target mismatch {target_mismatch} and {n_points} points and truncation {truncate}")
	start = datetime.datetime.now()

	if truncate is None:
		N = len(bank.rectangles)
		bank_ids = bank.ids
		bank_rectangles = bank.rectangles
	else:
		N = truncate
		bank_ids = numpy.random.choice(a=bank.ids, size=N, replace=False)
		bank_rectangles = [bank.rectangles[bank.ids_map[rid]] for rid in bank_ids]

	data = numpy.empty(shape=(N,), dtype=numpy.float64)

	for n, (rid, r) in enumerate(zip(bank_ids, bank_rectangles)):
		if verbose and (n + 1) % (N // 100) == 0:
			elapsed = (datetime.datetime.now() - start).total_seconds()
			total_time = elapsed / (n + 1) * N
			estimated = start + datetime.timedelta(seconds=total_time)
			print(f"Computing EBR for rectangle {n + 1} of {N} ({(n + 1) / N * 100:.2f}%), ETA: {estimated}")

		ebr = bank.ellipse_boundary_ratio(r, target_mismatch=target_mismatch, n_points=n_points, object_symmetric=object_symmetric)
		data[n] = ebr

	results = {
		"ids": bank_ids,
		"ebr": data,
		"target_mismatch": target_mismatch,
		"n_points": n_points,
		"elapsed": (datetime.datetime.now() - start).total_seconds()
	}

	return results


def save_ebr_results(results: dict, output_file: str):
	"""Save the results of a EBR computation to a file

	Args:
		results:
		output_file:
	"""
	io.h5write(fname=output_file, obj=results)


def load_ebr_results(input_file: str) -> dict:
	"""Load the results of a EBR computation from a file

	Args:
		input_file:

	Returns:
		Dict, the results of the computation with keys: ebr, target_mismatch, n_points, elapsed
	"""
	return io.h5read(fname=input_file)


def plot_ebr_m1_m2(bank: cbc.Bank, ebr: numpy.ndarray, ebr_ids: numpy.ndarray, target_mismatch: float, n_points: int, output_file: str):
	"""Plot the results of a EBR computation vs m1, m2 coordinates with color indicating EBR

	Args:
		bank:
			Bank, the bank used to compute the EBR
		ebr:
			Array[N], the EBR values
	"""
	# Extract m1, m2 coordinates in same order as EBR
	ms = [(bank.cfunc(bank.rectangles[bank.ids_map[rid]].center)[cbc.M1],
		   bank.cfunc(bank.rectangles[bank.ids_map[rid]].center)[cbc.M2]) for rid in ebr_ids]
	m1s, m2s = zip(*ms)

	# Convert to arrays
	m1s = numpy.array(m1s)
	m2s = numpy.array(m2s)

	fig, ax = plt.subplots()
	sc = ax.scatter(m1s, m2s, c=ebr, cmap='Blues', alpha=0.2)
	fig.colorbar(sc, ax=ax)

	ax.set_xlabel('M1')
	ax.set_ylabel('M2')
	ax.set_title(f'Ellipse Boundary Ratio | Target Mismatch: {target_mismatch} | N Points: {n_points}')

	# fig.tight_layout()
	plt.savefig(sanitize_plot_file_name(output_file))


def plot_ebr_log10m1_chi(bank: cbc.Bank, ebr: numpy.ndarray, ebr_ids: numpy.ndarray, target_mismatch: float, n_points: int, output_file: str):
	"""Plot the results of a EBR computation vs m1, m2 coordinates with color indicating EBR

	Args:
		bank:
			Bank, the bank used to compute the EBR
		ebr:
			Array[N], the EBR values
	"""
	# Extract m1, m2 coordinates in same order as EBR
	ms = [(bank.cfunc(bank.rectangles[bank.ids_map[rid]].center)[cbc.M1],
		   bank.cfunc(bank.rectangles[bank.ids_map[rid]].center)[cbc.S1Z]) for rid in ebr_ids]
	m1s, chis = zip(*ms)

	# Convert to arrays
	m1s = numpy.array(m1s)
	chis = numpy.array(chis)

	fig, ax = plt.subplots()
	sc = ax.scatter(m1s, chis, c=ebr, cmap='Blues', alpha=0.2)
	fig.colorbar(sc, ax=ax)

	# Set log scale on x axis
	ax.set_xscale('log', basex=10)

	ax.set_xlabel('M1')
	ax.set_ylabel('Chi')
	ax.set_title(f'Ellipse Boundary Ratio | Target Mismatch: {target_mismatch} | N Points: {n_points}')

	# fig.tight_layout()
	plt.savefig(sanitize_plot_file_name(output_file))


def plot_ebr_logm1_logm2(bank: cbc.Bank, ebr: numpy.ndarray, ebr_ids: numpy.ndarray, target_mismatch: float, n_points: int, output_file: str):
	"""Plot the results of a EBR computation vs m1, m2 coordinates with color indicating EBR

	Args:
		bank:
			Bank, the bank used to compute the EBR
		ebr:
			Array[N], the EBR values
	"""
	# Extract m1, m2 coordinates in same order as EBR
	ms = [(bank.cfunc(bank.rectangles[bank.ids_map[rid]].center)[cbc.M1],
		   bank.cfunc(bank.rectangles[bank.ids_map[rid]].center)[cbc.M2]) for rid in ebr_ids]
	m1s, m2s = zip(*ms)

	# Convert to arrays
	m1s = numpy.array(m1s)
	m2s = numpy.array(m2s)

	fig, ax = plt.subplots()
	sc = ax.scatter(m1s, m2s, c=ebr, cmap='Blues', alpha=0.2)
	fig.colorbar(sc, ax=ax)

	# Set log scale on axes
	ax.set_xscale('log', basex=10)
	ax.set_yscale('log', basey=10)

	ax.set_xlabel('M1')
	ax.set_ylabel('M2')
	ax.set_title(f'Ellipse Boundary Ratio | Target Mismatch: {target_mismatch} | N Points: {n_points}')

	# fig.tight_layout()
	plt.savefig(sanitize_plot_file_name(output_file))


def plot_ebr_histogram(ebr: numpy.ndarray, target_mismatch: float, n_points: int, output_file: str):
	"""Plot the results of a EBR computation as a histogram

	Args:
		ebr:
			Array[N], the EBR values
	"""
	# Compute histogram bins
	n_bins = max(ebr.size // 2000, 100)  # Max of 2000 per bin on average, min of 100 bins
	counts, bins = numpy.histogram(ebr, bins=n_bins)
	bin_centers = (bins[:-1] + bins[1:]) / 2

	fig, ax = plt.subplots()
	ax.hist(bins[:-1], bins, weights=counts)
	ax.set_title(f'Ellipse Boundary Ratio Histogram | Target Mismatch: {target_mismatch} | N Points: {n_points}')

	# fig.tight_layout()
	plt.savefig(sanitize_plot_file_name(output_file))


def sanitize_plot_file_name(name: str) -> str:
	"""Remove characters that are not allowed in file names

	Args:
		name:

	Returns:

	"""
	suffix = None
	if name.endswith('.png'):
		name = name[:-4]
		suffix = '.png'
	new = name.replace(' ', '_').replace('.', '_').replace(':', '_').replace('|', '_')

	if suffix is not None:
		new += suffix
	return new


def main():
	"""Main function"""
	config = parse_args()

	print(f"Loading bank from {config['bank']}")
	bank = cbc.Bank.load(config['bank'])

	if config.get('trim', False):
		print(f"Trimming bank")
		bank.trim(verbose=True)

	if 'file' in config:
		print(f"Loading EBR results from {config['file']}")
		results = load_ebr_results(config['file'])
	else:
		print(f"Computing EBR for bank {config['bank']}")
		results = compute_bank_ebr(bank=bank,
								   target_mismatch=config.get('target-mismatch', 0.03),
								   n_points=config.get('n-points', 100),
								   verbose=True,
								   truncate=config.get('truncate', None),
								   object_symmetric=config.get('symmetric', False))

		print(f"Saving EBR results to {config['output']}")
		save_ebr_results(results, config['output'])

	print(f"Plotting EBR results: m1 v m2")
	plot_ebr_m1_m2(bank=bank,
				   ebr=results['ebr'],
				   ebr_ids=results['ids'],
				   target_mismatch=results['target_mismatch'],
				   n_points=results['n_points'],
				   output_file=f'ebr_m1_m2_x_t{results["target_mismatch"]}_n{results["n_points"]}.png')

	print(f"Plotting EBR results: log10m1 v log10m2")
	plot_ebr_logm1_logm2(bank=bank,
						 ebr=results['ebr'],
						 ebr_ids=results['ids'],
						 target_mismatch=results['target_mismatch'],
						 n_points=results['n_points'],
						 output_file=f'ebr_log10m1_log10m2_x_t{results["target_mismatch"]}_n{results["n_points"]}.png')

	print(f"Plotting EBR results: log10m1 v m2")
	plot_ebr_log10m1_chi(bank=bank,
						 ebr=results['ebr'],
						 ebr_ids=results['ids'],
						 target_mismatch=results['target_mismatch'],
						 n_points=results['n_points'],
						 output_file=f'ebr_log10m1_chi_x_t{results["target_mismatch"]}_n{results["n_points"]}.png')

	print(f"Plotting EBR results: histogram")
	plot_ebr_histogram(ebr=results['ebr'],
					   target_mismatch=results['target_mismatch'],
					   n_points=results['n_points'],
					   output_file=f'ebr_hist_x_t{results["target_mismatch"]}_n{results["n_points"]}.png')


if __name__ == "__main__":
	main()
